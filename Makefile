ROOTCFLAGS    = $(shell $(ROOTSYS)/bin/root-config --cflags)
ROOTGLIBS     = $(shell $(ROOTSYS)/bin/root-config --glibs)

CXX            = g++

CXXFLAGS       = -fPIC -Wall -O3 -g
CXXFLAGS       += $(filter-out -stdlib=libc++ -pthread , $(ROOTCFLAGS))
#CXXFLAGS       += $(filter-out -stdlib=libc++ -pthread , $(RFCFLAGS))

GLIBS          = $(filter-out -stdlib=libc++ -pthread , $(ROOTGLIBS))
#GLIBS         += $(filter-out -stdlib=libc++ -pthread , $(RFGLIBS))

INCLUDEDIR       = ./include/
SRCDIR           = ./src/
CXX              += -I$(INCLUDEDIR) -I.
OUTOBJ           = ./obj/

CC_FILES := $(wildcard src/*.cxx)
HH_FILES := $(wildcard include/*.h)
OBJ_FILES := $(addprefix $(OUTOBJ),$(notdir $(CC_FILES:.cxx=.o)))

all: runLoop.x

runLoop.x:  $(SRCDIR)runLoop.C $(OBJ_FILES) $(HH_FILES)
	$(CXX) $(CXXFLAGS) -o runLoop.x $(OBJ_FILES) $(GLIBS) $ $<
	touch runLoop.x

$(OUTOBJ)%.o: src/%.cxx include/%.h
	mkdir -p obj/
	$(CXX) $(CXXFLAGS) -c $< -o $@

clean:
	rm -f $(OUTOBJ)*.o
	rm -f *.x
	rm -rf *.dSYM
