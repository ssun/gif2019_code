#ifndef chan_properties_H
#define chan_properties_H

#include<iostream>
#include<string>
#include<sstream>

#include<TString.h>
#include<TGraph.h>
#include<TPostScript.h>
#include<TCanvas.h>
#include<TH1F.h>
#include<TF1.h>
#include<TLatex.h>

#include<vector>
#include<utility>

class chan_properties {

 public :
  bool verbose;

  chan_properties();
  ~chan_properties();

  double getPadArea( int layer_number, int ivmm,           int ch_number, int detector_type );
  double getPadArea( int layer_number, TString vmm_letter, int ch_number, int detector_type );

  double getStripArea( int layer_number, int ivmm,         int ch_number );
  
  int getPad_PiCap( int layer_number, int ivmm,           int ch_number );
  int getPad_PiCap( int layer_number, TString vmm_letter, int ch_number );

  int getStrip_Resistance( int layer_number, int ivmm,    int ch_number );

  std::pair<int,int> getComparablePadChan( int layer_number, TString vmm_letter, int ch_number );
  std::pair<int,int> getComparablePadChan( int layer_number, int ivmm, int ch_number );

  std::pair<int,int> getComparableStripChan( int layer_number, int ivmm, int ch_number );

  bool isGood( int ilayer, int ivmm, int ich, bool ispad, int data_type, int detector_type );
};

#endif

