
//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Oct 30 15:15:12 2018 by ROOT version 6.10/02
// from TTree decoded_event_tree/decoded_event_tree
// found on file: run_181026_mu_2900V_640At_decoded.root
//////////////////////////////////////////////////////////

#ifndef decoded_event_tree_h
#define decoded_event_tree_h

#include <chan_plots.h>
#include <chan_compare_plots.h>
#include <chan_properties.h>

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TRandom3.h>

#include <TString.h>
#include <TGraph.h>
#include <TArrow.h>
#include <TF1.h>
#include <TH1F.h>
#include <TPostScript.h>
#include <TCanvas.h>
#include <TPad.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TSystem.h>

#include <iostream>
#include <sstream>
#include <fstream>

// Header file for the classes stored in the TTree if any.
#include "vector"

class decoded_event_tree {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Fixed size dimensions of array or collections stored in the TTree if any.
   //------------------------------------//

   int max_kek_rate;

   int nhists;

   chan_properties * chan_prop;

   //------------------------------------//
   // Declaration of leaf types
   UInt_t          m_l1_id;
   Int_t           m_nhits;
   Int_t           m_nNullPackets;

   int n_pad_hit;
   int n_wire_hit;
   int n_strip_hit;

   int in_beam_pad_elink;
   int in_beam_pad_vmm;
   int in_beam_pad_ch;

   int in_beam_wire_elink;
   int in_beam_wire_vmm;
   int in_beam_wire_ch;

   int in_beam_strip_elink;
   int in_beam_strip_vmm;
   int in_beam_strip_ch;

   std::vector<int> min_tdo_pad_ch;
   std::vector<int> max_tdo_pad_ch;
   std::vector<int> min_tdo_wire_ch;
   std::vector<int> max_tdo_wire_ch;
   std::vector<int> min_tdo_strip_ch;
   std::vector<int> max_tdo_strip_ch;

   double pad_in_time_bcid;
   double wire_in_time_bcid;
   double strip_in_time_bcid;

   std::stringstream m_sx;

   std::vector<unsigned int> *m_tdo;
   std::vector<unsigned int> *m_pdo;
   std::vector<unsigned int> *m_chan;
   std::vector<unsigned int> *m_vmm_id;
   std::vector<unsigned int> *m_elink_id;
   std::vector<unsigned int> *m_bcid_rel;
   std::vector<unsigned int> *m_bcid;
   std::vector<unsigned int> *m_neighbour;
   std::vector<unsigned int> *m_parity;
   std::vector<unsigned int> *m_orbit;
   std::vector<unsigned int> *m_l0_id;
   std::vector<unsigned int> *m_null_bcid;
   std::vector<unsigned int> *m_null_elink_id;
   std::vector<unsigned int> *m_null_orbit;
   std::vector<unsigned int> *m_null_l1_id;
   std::vector<unsigned int> *m_null_l0_id;

   // List of branches
   TBranch        *b_m_l1_id;   //!
   TBranch        *b_m_nhits;   //!
   TBranch        *b_m_nNullPackets;   //!
   TBranch        *b_m_tdo;   //!
   TBranch        *b_m_pdo;   //!
   TBranch        *b_m_chan;   //!
   TBranch        *b_m_vmm_id;   //!
   TBranch        *b_m_elink_id;   //!
   TBranch        *b_m_bcid_rel;   //!
   TBranch        *b_m_bcid;   //!
   TBranch        *b_m_neighbour;   //!
   TBranch        *b_m_parity;   //!
   TBranch        *b_m_orbit;   //!
   TBranch        *b_m_l0_id;   //!
   TBranch        *b_m_null_bcid;   //!
   TBranch        *b_m_null_elink_id;   //!
   TBranch        *b_m_null_orbit;   //!
   TBranch        *b_m_null_l1_id;   //!
   TBranch        *b_m_null_l0_id;   //!

   //--------------------------------------//

   TCanvas * c1;
   
   TPostScript *ps;

   TLegend *leg_top;
   TLegend *leg_top2;
   TLegend *leg_bot;
   
   TLatex *Tl;

   //-------------------------------------//

   std::vector<chan_plots*> *layer3_pad_vmmB_chan_plots_2800;
   std::vector<chan_plots*> *layer3_pad_vmmC_chan_plots_2800;
   std::vector<chan_plots*> *layer4_pad_vmmB_chan_plots_2800;
   std::vector<chan_plots*> *layer4_pad_vmmC_chan_plots_2800;

   std::vector<chan_plots*> *layer3_strip_vmm4_chan_plots_2800;
   std::vector<chan_plots*> *layer4_strip_vmm3_chan_plots_2800;

   //For QS2
   std::vector<chan_plots*> *layer3_strip_vmm3_chan_plots_2800;
   std::vector<chan_plots*> *layer4_strip_vmm4_chan_plots_2800;
   
   std::vector<chan_plots*> *layer3_pad_vmmB_chan_plots_2900;
   std::vector<chan_plots*> *layer3_pad_vmmC_chan_plots_2900;
   std::vector<chan_plots*> *layer4_pad_vmmB_chan_plots_2900;
   std::vector<chan_plots*> *layer4_pad_vmmC_chan_plots_2900;

   std::vector<chan_plots*> *layer3_strip_vmm4_chan_plots_2900;
   std::vector<chan_plots*> *layer4_strip_vmm3_chan_plots_2900;

   //For QS2
   std::vector<chan_plots*> *layer3_strip_vmm3_chan_plots_2900;
   std::vector<chan_plots*> *layer4_strip_vmm4_chan_plots_2900;

   //-------------------------------------//

   std::vector<chan_plots*> *layer3_pad_vmmB_chan_plots_6bitToT_2800;
   std::vector<chan_plots*> *layer3_pad_vmmC_chan_plots_6bitToT_2800;
   std::vector<chan_plots*> *layer4_pad_vmmB_chan_plots_6bitToT_2800;
   std::vector<chan_plots*> *layer4_pad_vmmC_chan_plots_6bitToT_2800;

   std::vector<chan_plots*> *layer3_strip_vmm4_chan_plots_6bitToT_2800;
   std::vector<chan_plots*> *layer4_strip_vmm3_chan_plots_6bitToT_2800;

   //For QS2
   std::vector<chan_plots*> *layer3_strip_vmm3_chan_plots_6bitToT_2800;
   std::vector<chan_plots*> *layer4_strip_vmm4_chan_plots_6bitToT_2800;

   std::vector<chan_plots*> *layer3_pad_vmmB_chan_plots_6bitToT_2900;
   std::vector<chan_plots*> *layer3_pad_vmmC_chan_plots_6bitToT_2900;
   std::vector<chan_plots*> *layer4_pad_vmmB_chan_plots_6bitToT_2900;
   std::vector<chan_plots*> *layer4_pad_vmmC_chan_plots_6bitToT_2900;

   std::vector<chan_plots*> *layer3_strip_vmm4_chan_plots_6bitToT_2900;
   std::vector<chan_plots*> *layer4_strip_vmm3_chan_plots_6bitToT_2900;

   //For QS2
   std::vector<chan_plots*> *layer3_strip_vmm3_chan_plots_6bitToT_2900;
   std::vector<chan_plots*> *layer4_strip_vmm4_chan_plots_6bitToT_2900;

   //-------------------------------------//

   std::vector<chan_plots*> *layer3_pad_vmmB_chan_plots_2800_ana;
   std::vector<chan_plots*> *layer3_pad_vmmC_chan_plots_2800_ana;
   std::vector<chan_plots*> *layer4_pad_vmmB_chan_plots_2800_ana;
   std::vector<chan_plots*> *layer4_pad_vmmC_chan_plots_2800_ana;

   std::vector<chan_plots*> *layer3_strip_vmm4_chan_plots_2800_ana;
   std::vector<chan_plots*> *layer4_strip_vmm3_chan_plots_2800_ana;

   //-------------------------------------//

   std::vector<chan_compare_plots*> *layer3_pad_vmmB_compare_plots_2800;
   std::vector<chan_compare_plots*> *layer3_pad_vmmC_compare_plots_2800;

   std::vector<chan_compare_plots*> *layer3_pad_vmmB_compare_plots_6bitToT_2800;
   std::vector<chan_compare_plots*> *layer3_pad_vmmC_compare_plots_6bitToT_2800;

   std::vector<chan_compare_plots*> *layer3_strip_vmm4_compare_plots_2800;

   std::vector<chan_compare_plots*> *layer3_strip_vmm4_compare_plots_6bitToT_2800;

   std::vector<chan_compare_plots*> *layer3_pad_vmmB_compare_plots_2900;
   std::vector<chan_compare_plots*> *layer3_pad_vmmC_compare_plots_2900;

   std::vector<chan_compare_plots*> *layer3_pad_vmmB_compare_plots_6bitToT_2900;
   std::vector<chan_compare_plots*> *layer3_pad_vmmC_compare_plots_6bitToT_2900;

   std::vector<chan_compare_plots*> *layer3_strip_vmm4_compare_plots_2900;

   std::vector<chan_compare_plots*> *layer3_strip_vmm4_compare_plots_6bitToT_2900;

   //------------------------------------//

   std::vector<int> layer_vec;
   std::vector<int> elink_vec;
   std::vector<int> vmm_vec;
   std::vector<int> SCA_ID_vec;

   std::vector<int> HV_vec;

   std::vector<int> layer_vec_ana;
   std::vector<int> elink_vec_ana;
   std::vector<int> vmm_vec_ana;
   std::vector<int> SCA_ID_vec_ana;

   std::vector<int> HV_vec_ana;

   //-------------------------------------//

   TFile * file;

   decoded_event_tree(TTree *tree=0);
   virtual ~decoded_event_tree();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   //   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);

   //-----------------------------------------------------------------------------------//

   int binarySearch(std::vector<double> sorted_vec, int l, int r, double x) ;
   std::vector<double> sort_vector( std::vector<double> vec );
   double GetMedian( TString filename );

   void GetNoise(TString filename);
   void plotNoise( TH1F * h_noise_ivmm, int ntriggers, TString filename );

   float GetChanEfficiency( TH1F * h_rel_bcid, int n_triggers, float photon_rate );

   std::vector<chan_plots*>         * GetChannelPlots(        int ilayer, bool pads, int ivmm, int voltage, int data_type );
   std::vector<chan_plots*>         * GetAnalogChannelPlots(  int ilayer, bool pads, int ivmm, int voltage );
   std::vector<chan_compare_plots*> * GetChannelComparePlots( int ilayer, bool pads, int ivmm, int voltage, int data_type );

   void initChannelPlots(int data_type, int detector_type);
   void initAnalogChannelPlots(int detector_type);
   void plotChannelPlots(bool pads, bool isAnalog, int data_type, int detector_type);

   void initChannelComparePlots(int data_type);
   void plotChannelComparePlots(int data_type);

   void plotPadHits(   TH1F * h_pad_vmmB, TH1F *h_pad_vmmC, 
		       int layer, 
		       int n_triggers, float KEK_photon_rate, float attenuation, int voltage,
		       TString filename, int data_type );
   void plotPadHits(   TH1F * h_pad_vmmB, TH1F *h_pad_vmmC, 
		       std::vector<TH1F*> * h_pad_vmmB_eff_vec,
		       std::vector<TH1F*> * h_pad_vmmC_eff_vec,
		       int layer, int n_triggers, int n_triggers_TP, 
		       float KEK_photon_rate, float attenuation, int voltage,
		       TString filename, bool isTP, int data_type );

   void plotStripHits( TH1F * h_strip_vmm, 
		       int layer, int vmm, int n_triggers, 
		       float kek_photon_rate, float attenuation, int voltage,
		       TString filename, int data_type );
   void plotStripHits( TH1F * h_strip_vmm,
		       std::vector<TH1F*> * h_strip_eff,
                       int     layer,           int   vmm, 
		       int     n_triggers,      int   n_triggers_TP,
                       float   kek_photon_rate, float attenuation, int voltage,
                       TString filename,        bool  isTP,          int data_type );


   void Loop(TString filename, 
	     double attenuation, double KEK_rate, 
	     int voltage, bool is25ns, bool loopEvt, int data_type, int detector_type);
   void Loop(TString filename, TString filename_TP, 
	     double attenuation, double KEK_rate, 
	     int voltage, bool is25ns, bool loopEvt, int data_type, bool isTP, int detector_type);

   void AnalyzeAnalog(TString filename, double attenuation, double KEK_photon_rate, int voltage, bool is25ns,
		      int ilayer, int SCA_ID, int ivmm, int ich );

   int GetNPulse( TGraph * h_waveform, double threshold,
		  double attenuation, double KEK_photon_rate, int voltage, bool is25ns,
		  int ilayer, int SCA_ID, int ivmm, int ich);

   void find_min_max_tdo( TH1F * hist_tdo, double &min_tdo, double &max_tdo );
   double calibrated_tdo( double hit_rel_bcid, double hit_raw_tdo,
			  double in_time_bcid, double min_tdo, double max_tdo );

   std::vector<double> FindHits(TH1F * h_hits, std::vector<int> channels);

   void initVal();
   void initHistos(int detector_type);
   void PlotHistos(int data_type, int detector_type);
   void plotSummary(bool pads, int voltage, int data_type, int detector_type );
   void plotSummaryL3(bool pads, int voltage, int data_type);
   void plotSummaryL4(bool pads, int voltage, int data_type);

 private:

   double convert_to_kHz;

   //-----------------------------------------------------------------------------------//
};

#endif

#ifdef decoded_event_tree_cxx
decoded_event_tree::decoded_event_tree(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
  /*
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("run_181026_mu_2900V_640At_decoded.root");
      if (!f || !f->IsOpen()) {
	//         f = new TFile("run_181026_mu_2900V_640At_decoded.root");
	f = new TFile("run_181029_mu_2900_noSource_pad17_25ns_decoded.root");
      }
      f->GetObject("decoded_event_tree",tree);

   }
   Init(tree);
  */
}

decoded_event_tree::~decoded_event_tree()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t decoded_event_tree::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t decoded_event_tree::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void decoded_event_tree::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   m_tdo = 0;
   m_pdo = 0;
   m_chan = 0;
   m_vmm_id = 0;
   m_elink_id = 0;
   m_bcid_rel = 0;
   m_bcid = 0;
   m_neighbour = 0;
   m_parity = 0;
   m_orbit = 0;
   m_l0_id = 0;
   m_null_bcid = 0;
   m_null_elink_id = 0;
   m_null_orbit = 0;
   m_null_l1_id = 0;
   m_null_l0_id = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("m_l1_id", &m_l1_id, &b_m_l1_id);
   fChain->SetBranchAddress("m_nhits", &m_nhits, &b_m_nhits);
   fChain->SetBranchAddress("m_nNullPackets", &m_nNullPackets, &b_m_nNullPackets);
   fChain->SetBranchAddress("m_tdo", &m_tdo, &b_m_tdo);
   fChain->SetBranchAddress("m_pdo", &m_pdo, &b_m_pdo);
   fChain->SetBranchAddress("m_chan", &m_chan, &b_m_chan);
   fChain->SetBranchAddress("m_vmm_id", &m_vmm_id, &b_m_vmm_id);
   fChain->SetBranchAddress("m_elink_id", &m_elink_id, &b_m_elink_id);
   fChain->SetBranchAddress("m_bcid_rel", &m_bcid_rel, &b_m_bcid_rel);
   fChain->SetBranchAddress("m_bcid", &m_bcid, &b_m_bcid);
   fChain->SetBranchAddress("m_neighbour", &m_neighbour, &b_m_neighbour);
   fChain->SetBranchAddress("m_parity", &m_parity, &b_m_parity);
   fChain->SetBranchAddress("m_orbit", &m_orbit, &b_m_orbit);
   fChain->SetBranchAddress("m_l0_id", &m_l0_id, &b_m_l0_id);
   fChain->SetBranchAddress("m_null_bcid", &m_null_bcid, &b_m_null_bcid);
   fChain->SetBranchAddress("m_null_elink_id", &m_null_elink_id, &b_m_null_elink_id);
   fChain->SetBranchAddress("m_null_orbit", &m_null_orbit, &b_m_null_orbit);
   fChain->SetBranchAddress("m_null_l1_id", &m_null_l1_id, &b_m_null_l1_id);
   fChain->SetBranchAddress("m_null_l0_id", &m_null_l0_id, &b_m_null_l0_id);
   Notify();
}

Bool_t decoded_event_tree::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void decoded_event_tree::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t decoded_event_tree::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef decoded_event_tree_cxx
