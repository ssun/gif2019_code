#ifndef chan_plots_H
#define chan_plots_H

#include<iostream>
#include<string>
#include<sstream>

#include<TString.h>
#include<TGraph.h>
#include<TPostScript.h>
#include<TCanvas.h>
#include<TH1F.h>
#include<TF1.h>
#include<TLatex.h>

#include<vector>
//#include<pair>

class chan_plots {

 public :
  chan_plots();
  ~chan_plots();
  
  int layer;
  int SCA_ID;

  int elink;
  int vmm;
  int chan;

  bool is_pad;
  bool is_good;

  int npoints;
  int npoints_eff;
  int npoints_baseline;

  float area;
  int capacitance;
  int resistance;

  float max_tot_rate;

  std::stringstream m_sx;

  std::vector<std::pair<float,float>> hits_vs_KEK_rate_vec;

  std::vector<std::pair<float,float>> hits_eff_vs_KEK_rate_vec;
  std::vector<std::pair<float,float>> hits_eff_vs_tot_rate_vec;

  TF1 * f_linear     ;
  TF1 * f_expo       ;
  TF1 * f_xexpo      ;
  TF1 * f_x1overX    ;
  TF1 * f_linear_tot ;
  TF1 * f_expo_tot   ;
  TF1 * f_xexpo_tot  ;
  TF1 * f_x1overX_tot;
  TF1 * f_1overX_tot ;

  TF1 * f_linear_eff_kek    ;
  TF1 * f_expo_eff_kek      ;
  TF1 * f_linear_eff_kek_tot;
  TF1 * f_expo_eff_kek_tot  ;

  TF1 * f_expo_eff_tot      ;

  TLatex * Tl;

  TGraph hits_vs_KEK_rate;
  TGraph hits_vs_KEK_rate_tot;

  TGraph hits_over_linear_vs_KEK_rate;
  TGraph hits_over_linear_vs_tot_rate;

  TGraph hits_over_eff_vs_KEK_rate;
  TGraph hits_over_eff_vs_KEK_rate_tot;

  TGraph eff_vs_KEK_rate;
  TGraph eff_vs_KEK_rate_tot;

  TGraph eff_vs_photon_rate_tot;

  //------------------------------------------//
  
  TGraph baseline_vs_KEK_rate;
  TGraph baseline_vs_KEK_rate_tot;

  void SetParams( int ilayer, int ielink, int ivmm, int ich, int iSCA_ID, float iarea, int icap, bool ispad );

  void AddNoisePoint( float noise_rate );
  void AddBaselinePoint(float kek_photon_rate, float baseline);

  void AddPoint( float kek_photon_rate, float hit_rate );
  void AddPoint( float kek_photon_rate, float photon_hit_rate, float efficiency );
  void FitAll( float max_kek_rate );

  void DrawAll( float max_kek_rate, TPostScript *ps, TCanvas *c_ch, TH1F *hist );

  void reset();

};

#endif

