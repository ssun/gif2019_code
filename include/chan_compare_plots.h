#ifndef chan_compare_plots_H
#define chan_compare_plots_H

#include <chan_plots.h>

#include<iostream>
#include<string>
#include<sstream>

#include<TString.h>
#include<TGraph.h>
#include<TPostScript.h>
#include<TCanvas.h>
#include<TH1F.h>
#include<TF1.h>
#include<TLatex.h>

#include<vector>
//#include<pair>

class chan_compare_plots {

 public :
  chan_compare_plots();
  ~chan_compare_plots();
  
  //----------------------------//

  int layer;
  int SCA_ID;

  int elink;
  int vmm;
  int chan;

  float area;
  int capacitance;
  int resistance;

  bool is_pad;

  int npoints;

  float max_rate_ch1     = 0;
  float max_rate_ch2     = 0;
  float max_tot_rate_ch1 = 0;
  float max_tot_rate_ch2 = 0;

  std::stringstream m_sx;
  
  chan_plots * ch1;
  chan_plots * ch2;

  //----------------------------//

  TF1 * f_compare;

  TLatex * Tl;

  TGraph hits_ch1_vs_hits_ch2;

  bool SetParams( chan_plots * ich1, chan_plots * ich2 );
  bool MakePlots();

  void FitAll( float max_kek_rate );

  void DrawAll( float max_kek_rate, TPostScript *ps, TCanvas *c_ch, TH1F *hist );

  void reset();

  //--------------------------//

};

#endif

