#define decoded_event_tree_cxx
#include "decoded_event_tree.h"
#include "data_properties.h"

#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>



double decoded_event_tree::calibrated_tdo( double hit_rel_bcid, double hit_raw_tdo,
					   double in_time_bcid, double min_tdo, double max_tdo ) {

   // returns timing in terms of ns.

   double calibrated_tdo = ( (hit_rel_bcid - in_time_bcid) -
			     (hit_raw_tdo - (min_tdo+max_tdo)/2.0)/(max_tdo - min_tdo) ) * 25.0;

   return calibrated_tdo;
 }

//----------------------------------------------------------------------------------------------//

 void decoded_event_tree::find_min_max_tdo( TH1F * hist_tdo, double &min_tdo, double &max_tdo ) {

   bool found_first_bin = false;
   bool found_last_bin  = false;

   for ( int i=0; i< hist_tdo->GetNbinsX(); i++ ) {
     if ( !found_first_bin && hist_tdo->GetBinContent( i ) > 0 ) {
       found_first_bin = true;
       min_tdo = hist_tdo->GetBinCenter(i);
       break;
     }
   }

   for ( int i=hist_tdo->GetNbinsX()+1; i>0; i-- ) {
     if ( !found_last_bin && hist_tdo->GetBinContent( i ) > 0 ) {
       found_last_bin = true;
     }
     else if ( found_last_bin && hist_tdo->GetBinContent( i ) > 3 ) {
       max_tdo = hist_tdo->GetBinCenter(i+1);
       break;
     }
   }

   if ( found_first_bin && found_last_bin ) return;
   else {
     std::cout << "Error: Could not find min and max tdo for histogram "
	       << hist_tdo->GetName() << std::endl;
     return;
   }

 }

//---------------------------------------------------------------------------------//

void decoded_event_tree::initVal() {

  max_kek_rate=0.0;

  convert_to_kHz = 1.0/200.0E-9/1000.0;

  chan_prop = new chan_properties();

  layer3_pad_vmmB_chan_plots_2800 = new std::vector<chan_plots*>;
  layer3_pad_vmmC_chan_plots_2800 = new std::vector<chan_plots*>;
  layer4_pad_vmmB_chan_plots_2800 = new std::vector<chan_plots*>;
  layer4_pad_vmmC_chan_plots_2800 = new std::vector<chan_plots*>;

  //New chan_plot vectors for strips
  layer3_strip_vmm4_chan_plots_2800 = new std::vector<chan_plots*>;
  layer4_strip_vmm3_chan_plots_2800 = new std::vector<chan_plots*>;

  //New chan_plot vectors for QS2 strips
  layer3_strip_vmm3_chan_plots_2800 = new std::vector<chan_plots*>;
  layer4_strip_vmm4_chan_plots_2800 = new std::vector<chan_plots*>;
  
  layer3_pad_vmmB_compare_plots_2800 = new std::vector<chan_compare_plots*>;
  layer3_pad_vmmC_compare_plots_2800 = new std::vector<chan_compare_plots*>;
  //New compare_plot vector for strips
  layer3_strip_vmm4_compare_plots_2800 = new std::vector<chan_compare_plots*>;

  layer3_pad_vmmB_chan_plots_2900 = new std::vector<chan_plots*>;
  layer3_pad_vmmC_chan_plots_2900 = new std::vector<chan_plots*>;
  layer4_pad_vmmB_chan_plots_2900 = new std::vector<chan_plots*>;
  layer4_pad_vmmC_chan_plots_2900 = new std::vector<chan_plots*>;

  //New chan_plot vectors for strips   
  layer3_strip_vmm4_chan_plots_2900 = new std::vector<chan_plots*>;
  layer4_strip_vmm3_chan_plots_2900 = new std::vector<chan_plots*>;

  //New chan_plot vectors for QS2 strips
  layer3_strip_vmm3_chan_plots_2900 = new std::vector<chan_plots*>;
  layer4_strip_vmm4_chan_plots_2900 = new std::vector<chan_plots*>;

  layer3_pad_vmmB_compare_plots_2900 = new std::vector<chan_compare_plots*>;
  layer3_pad_vmmC_compare_plots_2900 = new std::vector<chan_compare_plots*>;
  //New compare_plot vector for strips 
  layer3_strip_vmm4_compare_plots_2900 = new std::vector<chan_compare_plots*>;

  //-------------------------------------------//
  //          Data With 6 bit ADC
  //-------------------------------------------//

  layer3_pad_vmmB_chan_plots_6bitToT_2800 = new std::vector<chan_plots*>;
  layer3_pad_vmmC_chan_plots_6bitToT_2800 = new std::vector<chan_plots*>;
  layer4_pad_vmmB_chan_plots_6bitToT_2800 = new std::vector<chan_plots*>;
  layer4_pad_vmmC_chan_plots_6bitToT_2800 = new std::vector<chan_plots*>;

  //New chan_plot vectors for strips
  layer3_strip_vmm4_chan_plots_6bitToT_2800 = new std::vector<chan_plots*>;
  layer4_strip_vmm3_chan_plots_6bitToT_2800 = new std::vector<chan_plots*>;

  //New chan_plot vectors for QS2 strips
  layer3_strip_vmm3_chan_plots_6bitToT_2800 = new std::vector<chan_plots*>;
  layer4_strip_vmm4_chan_plots_6bitToT_2800 = new std::vector<chan_plots*>;

  layer3_pad_vmmB_compare_plots_6bitToT_2800 = new std::vector<chan_compare_plots*>;
  layer3_pad_vmmC_compare_plots_6bitToT_2800 = new std::vector<chan_compare_plots*>;
  //New compare_plot vector for strips
  layer3_strip_vmm4_compare_plots_6bitToT_2800 = new std::vector<chan_compare_plots*>;

  //-----------------------------------------//

  layer3_pad_vmmB_chan_plots_6bitToT_2900 = new std::vector<chan_plots*>;
  layer3_pad_vmmC_chan_plots_6bitToT_2900 = new std::vector<chan_plots*>;
  layer4_pad_vmmB_chan_plots_6bitToT_2900 = new std::vector<chan_plots*>;
  layer4_pad_vmmC_chan_plots_6bitToT_2900 = new std::vector<chan_plots*>;

  //New chan_plot vectors for strips  
  layer3_strip_vmm4_chan_plots_6bitToT_2900 = new std::vector<chan_plots*>;
  layer4_strip_vmm3_chan_plots_6bitToT_2900 = new std::vector<chan_plots*>;

  //New chan_plot vectors for QS2 strips
  layer3_strip_vmm3_chan_plots_6bitToT_2900 = new std::vector<chan_plots*>;
  layer4_strip_vmm4_chan_plots_6bitToT_2900 = new std::vector<chan_plots*>;
  
  layer3_pad_vmmB_compare_plots_6bitToT_2900 = new std::vector<chan_compare_plots*>;
  layer3_pad_vmmC_compare_plots_6bitToT_2900 = new std::vector<chan_compare_plots*>;
  //New compare_plot vector for strips
  layer3_strip_vmm4_compare_plots_6bitToT_2900 = new std::vector<chan_compare_plots*>;

  //-------------------------------------------//
  //              Analog Data
  //-------------------------------------------//

  layer3_pad_vmmB_chan_plots_2800_ana = new std::vector<chan_plots*>;
  layer3_pad_vmmC_chan_plots_2800_ana = new std::vector<chan_plots*>;
  layer4_pad_vmmB_chan_plots_2800_ana = new std::vector<chan_plots*>;
  layer4_pad_vmmC_chan_plots_2800_ana = new std::vector<chan_plots*>;

  layer3_strip_vmm4_chan_plots_2800_ana = new std::vector<chan_plots*>;
  layer4_strip_vmm3_chan_plots_2800_ana = new std::vector<chan_plots*>;

  //-------------------------------------------//


}

void decoded_event_tree::initHistos(int detector_type) {

  //------------------------------------------------------//
  //            Initialize Plotting Tools
  //------------------------------------------------------//

  //  c1 = new TCanvas("c1","c1");
  c1 = new TCanvas("c1");
  if ( detector_type == 1 ) {
    ps = new TPostScript("plotsQL1/plots.eps",112);
  }
  if ( detector_type == 2 ) {
    ps = new TPostScript("plotsQS2/plots.eps",112);
  }
  
  leg_top  = new TLegend(0.7,0.7,1.0,0.9);
  leg_top2 = new TLegend( 0.13, 0.70, 0.43, 0.87 );
  leg_bot  = new TLegend(0.7,0.15,1.0,0.3);
  
  Tl = new TLatex();
  Tl->SetTextSize(0.02);

  gStyle->SetOptStat(0);

}

void decoded_event_tree::GetNoise(TString filename) {
  TFile * noise_file = new TFile(filename.Data());

  if  ( !noise_file ) {
    std::cout << "Error: no noise file, aborting \n";
    return;
  }

  TH1F * h_nhits = (TH1F*) noise_file->Get("h_nhits");
  int n_triggers = h_nhits->GetEntries();

  TH1F * h_L3_pad_vmm_B_noise = (TH1F*) noise_file->Get("elink_23_vmm_1_channel_hits");
  if ( !h_L3_pad_vmm_B_noise ) {
    std::cout << "Error: no noise histo, aborting \n";
    return;
  }

  TH1F * h_L3_strip_vmm_4_noise = (TH1F*) noise_file->Get("elink_9_vmm_4_channel_hits");
  if ( !h_L3_strip_vmm_4_noise ) {
    std::cout << "Error: no noise histo, aborting \n";
    return;
  }


  h_L3_pad_vmm_B_noise->SetTitle("Noise Rate in Layer 3 Pad VMM B");
  plotNoise( h_L3_pad_vmm_B_noise, n_triggers, filename );

  h_L3_strip_vmm_4_noise->SetTitle("Noise Rate in Layer 3 Strip VMM 4");
  plotNoise( h_L3_strip_vmm_4_noise, n_triggers, filename );

}

void decoded_event_tree::plotNoise( TH1F * h_noise_ivmm, int n_triggers, TString filename ) {

  c1->cd();

  c1->SetLogy(1);
  c1->SetGridy(1);

  gStyle->SetOptStat(0);

  h_noise_ivmm->Scale(1.0/n_triggers/(200e-6)); // convert to kHz

  h_noise_ivmm->SetXTitle("VMM Channel Number");
  h_noise_ivmm->SetYTitle("Noise Rate (kHz)");

  h_noise_ivmm->Draw("hist");

  Tl->DrawLatex(0.0, 0.0, filename);
  
  c1->Update();
  ps->NewPage();

  c1->SetLogy(0);
  c1->SetGridy(0);
  

}

void decoded_event_tree::plotStripHits( TH1F * h_strip_vmm,
                                        int layer, int vmm, 
					int n_triggers, 
					float kek_photon_rate, float attenuation, int voltage,
                                        TString filename, int data_type ) {

  std::vector<TH1F*> * null_vmm = new std::vector<TH1F*>;
  null_vmm->resize(0);

  plotStripHits( h_strip_vmm,     null_vmm,
		 layer,           vmm, 
		 n_triggers,      0, 
		 kek_photon_rate, attenuation, voltage,
		 filename,        false,       data_type );

}

void decoded_event_tree::plotStripHits( TH1F * h_strip_vmm,    std::vector<TH1F*> * h_strip_eff,
					int layer,             int vmm, 
					int n_triggers,        int n_triggers_TP, 
					float kek_photon_rate, float attenuation, int voltage,
					TString filename,      bool isTP,         int data_type ) {

  gStyle->SetPalette(55);

  //  c1->cd();

  c1->SetLogy(0);
  c1->SetGridy(1);

  gStyle->SetOptStat(0);

  //--------------------------------------------------------------//                                                                        
  //                   Check Histogram Vector Size                                                                                          
  //--------------------------------------------------------------//                                                                        

  if ( isTP ) {
    if ( h_strip_eff->size() < 64 ) {
      std::cout << "Error:  efficiency histogram vector size is less than 64: Aborting Efficiency plotting.  Only plotting photon rate" << std::endl;

      plotStripHits( h_strip_vmm,     h_strip_eff,
		     layer,           vmm, 
		     n_triggers,      0,
		     kek_photon_rate, attenuation, voltage,
		     filename,        false,       data_type );
    }
  }


  //--------------------------------------------------------------//

  if ( h_strip_vmm ) {
    h_strip_vmm->Scale(1.0/n_triggers/(200e-6)); // convert to kHz

    h_strip_vmm->SetXTitle("VMM Channel Number");
    h_strip_vmm->SetYTitle("Hit Rate (kHz)");

    h_strip_vmm->SetMinimum(0.0);

    h_strip_vmm->Draw("hist");

    Tl->DrawLatex(0.0, h_strip_vmm->GetMaximum()*0.1, filename);
    
    c1->Update();
    ps->NewPage();

    for ( int ich=0; ich<64; ich++ ) {
      //std::cout << "I am working" << ich << std::endl;
      float hit_rate = h_strip_vmm->GetBinContent( ich+1 );
      
      float efficiency = -1.0;
      if ( isTP ) efficiency = GetChanEfficiency( h_strip_eff->at(ich), n_triggers_TP, hit_rate );
      
      if ( attenuation >= 100000 ) GetChannelPlots( layer, false, vmm, voltage, data_type )->at(ich)->AddNoisePoint( hit_rate );
      else {

	if ( isTP ) {
	  GetChannelPlots( layer, false, vmm, voltage, data_type )->at(ich)->AddPoint( kek_photon_rate, hit_rate, efficiency );
	}
	else {
	  GetChannelPlots( layer, false, vmm, voltage, data_type )->at(ich)->AddPoint( kek_photon_rate, hit_rate );
	}

      }

    }

  }

  //--------------------------------------------------------------//

  c1->SetLogy(0);
  c1->SetGridy(0);

}

std::vector<chan_plots*> * decoded_event_tree::GetChannelPlots( int ilayer, bool pads, int ivmm, int voltage, int data_type ) {

  std::vector<chan_plots*> *null = new std::vector<chan_plots*>;
  null->resize(0);

  if ( data_type == type_10bit ) {
    
    if( ilayer == 3 && pads && ivmm == 1 && voltage == 2800) return layer3_pad_vmmB_chan_plots_2800;
    if( ilayer == 3 && pads && ivmm == 2 && voltage == 2800) return layer3_pad_vmmC_chan_plots_2800;
    if( ilayer == 4 && pads && ivmm == 1 && voltage == 2800) return layer4_pad_vmmB_chan_plots_2800;
    if( ilayer == 4 && pads && ivmm == 2 && voltage == 2800) return layer4_pad_vmmC_chan_plots_2800;
    
    if( ilayer == 3 && !pads && ivmm == 4 && voltage == 2800) return layer3_strip_vmm4_chan_plots_2800;
    if( ilayer == 4 && !pads && ivmm == 3 && voltage == 2800) return layer4_strip_vmm3_chan_plots_2800;

    if( ilayer == 3 && pads && ivmm == 1 && voltage == 2900) return layer3_pad_vmmB_chan_plots_2900;
    if( ilayer == 3 && pads && ivmm == 2 && voltage == 2900) return layer3_pad_vmmC_chan_plots_2900;
    if( ilayer == 4 && pads && ivmm == 1 && voltage == 2900) return layer4_pad_vmmB_chan_plots_2900;
    if( ilayer == 4 && pads && ivmm == 2 && voltage == 2900) return layer4_pad_vmmC_chan_plots_2900;

    if( ilayer == 3 && !pads && ivmm == 4 && voltage == 2900) return layer3_strip_vmm4_chan_plots_2900;
    if( ilayer == 4 && !pads && ivmm == 3 && voltage == 2900) return layer4_strip_vmm3_chan_plots_2900;

    // For QS2 strips
    if( ilayer == 3 && !pads && ivmm == 3 && voltage == 2800) return layer3_strip_vmm3_chan_plots_2800;
    if( ilayer == 4 && !pads && ivmm == 4 && voltage == 2800) return layer4_strip_vmm4_chan_plots_2800;

    if( ilayer == 3 && !pads && ivmm == 3 && voltage == 2900) return layer3_strip_vmm3_chan_plots_2900;
    if( ilayer == 4 && !pads && ivmm == 4 && voltage == 2900) return layer4_strip_vmm4_chan_plots_2900;

  }
  else if ( data_type == type_6bit ) {
    
    if( ilayer == 3 && pads && ivmm == 1 && voltage == 2800) return layer3_pad_vmmB_chan_plots_6bitToT_2800;
    if( ilayer == 3 && pads && ivmm == 2 && voltage == 2800) return layer3_pad_vmmC_chan_plots_6bitToT_2800;
    if( ilayer == 4 && pads && ivmm == 1 && voltage == 2800) return layer4_pad_vmmB_chan_plots_6bitToT_2800;
    if( ilayer == 4 && pads && ivmm == 2 && voltage == 2800) return layer4_pad_vmmC_chan_plots_6bitToT_2800;

    if( ilayer == 3 && !pads && ivmm == 4 && voltage == 2800) return layer3_strip_vmm4_chan_plots_6bitToT_2800;
    if( ilayer == 4 && !pads && ivmm == 3 && voltage == 2800) return layer4_strip_vmm3_chan_plots_6bitToT_2800;

    if( ilayer == 3 && pads && ivmm == 1 && voltage == 2900) return layer3_pad_vmmB_chan_plots_6bitToT_2900;
    if( ilayer == 3 && pads && ivmm == 2 && voltage == 2900) return layer3_pad_vmmC_chan_plots_6bitToT_2900;
    if( ilayer == 4 && pads && ivmm == 1 && voltage == 2900) return layer4_pad_vmmB_chan_plots_6bitToT_2900;
    if( ilayer == 4 && pads && ivmm == 2 && voltage == 2900) return layer4_pad_vmmC_chan_plots_6bitToT_2900;

    if( ilayer == 3 && !pads && ivmm == 4 && voltage == 2900) return layer3_strip_vmm4_chan_plots_6bitToT_2900;
    if( ilayer == 4 && !pads && ivmm == 3 && voltage == 2900) return layer4_strip_vmm3_chan_plots_6bitToT_2900;

    // For QS2 strips
    if( ilayer == 3 && !pads && ivmm == 3 && voltage == 2800) return layer3_strip_vmm3_chan_plots_6bitToT_2800;
    if( ilayer == 4 && !pads && ivmm == 4 && voltage == 2800) return layer4_strip_vmm4_chan_plots_6bitToT_2800;

    if( ilayer == 3 && !pads && ivmm == 3 && voltage == 2900) return layer3_strip_vmm3_chan_plots_6bitToT_2900;
    if( ilayer == 4 && !pads && ivmm == 4 && voltage == 2900) return layer4_strip_vmm4_chan_plots_6bitToT_2900;

  }

  std::cout << "FATAL: cannot find channel plot l" << ilayer << " vmm " << ivmm << " HV " << voltage << " is pad " << pads << " data type " << data_type << std::endl;

  return null;

}

std::vector<chan_plots*> * decoded_event_tree::GetAnalogChannelPlots( int ilayer, bool pads, int ivmm, int voltage ) {

  std::vector<chan_plots*> *null = new std::vector<chan_plots*>;
  null->resize(0);

  if( ilayer == 3 && pads && ivmm == 1 && voltage == 2800) return layer3_pad_vmmB_chan_plots_2800_ana;
  if( ilayer == 3 && pads && ivmm == 2 && voltage == 2800) return layer3_pad_vmmC_chan_plots_2800_ana;
  if( ilayer == 4 && pads && ivmm == 1 && voltage == 2800) return layer4_pad_vmmB_chan_plots_2800_ana;
  if( ilayer == 4 && pads && ivmm == 2 && voltage == 2800) return layer4_pad_vmmC_chan_plots_2800_ana;

  if( ilayer == 3 && !pads && ivmm == 4 && voltage == 2800) return layer3_strip_vmm4_chan_plots_2800_ana;
  if( ilayer == 4 && !pads && ivmm == 3 && voltage == 2800) return layer4_strip_vmm3_chan_plots_2800_ana;

  std::cout << "FATAL: cannot find channel plot" << std::endl;

  return null;

}

std::vector<chan_compare_plots*> * decoded_event_tree::GetChannelComparePlots( int ilayer, bool pads, int ivmm, int voltage, int data_type ) {

  std::vector<chan_compare_plots*> *null = new std::vector<chan_compare_plots*>;
  null->resize(0);

  if ( data_type == type_10bit ) {
    if( ilayer == 3 && pads && ivmm == 1 && voltage == 2800) return layer3_pad_vmmB_compare_plots_2800;
    if( ilayer == 3 && pads && ivmm == 2 && voltage == 2800) return layer3_pad_vmmC_compare_plots_2800;

    if( ilayer == 3 && pads && ivmm == 1 && voltage == 2900) return layer3_pad_vmmB_compare_plots_2900;
    if( ilayer == 3 && pads && ivmm == 2 && voltage == 2900) return layer3_pad_vmmC_compare_plots_2900;

    //if( ilayer == 3 && !pads && ivmm == 4 && voltage == 2800) return layer3_strip_vmm4_compare_plots_2800;

  }
  else if ( data_type == type_6bit ) {
    if( ilayer == 3 && pads && ivmm == 1 && voltage == 2800) return layer3_pad_vmmB_compare_plots_6bitToT_2800;
    if( ilayer == 3 && pads && ivmm == 2 && voltage == 2800) return layer3_pad_vmmC_compare_plots_6bitToT_2800;
    
    if( ilayer == 3 && pads && ivmm == 1 && voltage == 2900) return layer3_pad_vmmB_compare_plots_6bitToT_2900;
    if( ilayer == 3 && pads && ivmm == 2 && voltage == 2900) return layer3_pad_vmmC_compare_plots_6bitToT_2900;

    //if( ilayer == 3 && !pads && ivmm == 4 && voltage == 2800) return layer3_strip_vmm4_compare_plots_6bitToT_2800;

  }

  std::cout << "FATAL: cannot find channel compare plot" << std::endl;

  return null;

}


void decoded_event_tree::initChannelPlots(int data_type, int detector_type) {

  //---------------------------------------------//
  //  init plots for all channels
  //---------------------------------------------//

  //QL1 values
  if ( detector_type == 1 ) {
    
    layer_vec  = {3,    3,    4,    4,    3,    4  };
    elink_vec  = {23,   20,   19,   16,   9,    24 };
    vmm_vec    = {1,    2,    1,    2,    4,    3  };
    SCA_ID_vec = {1040, 1040, 993,  993,  1001, 1026};
  }
  
  //Swapped QS2 Values
  else if ( detector_type == 2 ) {
    layer_vec  = {3,    3,    4,    4,    3,    4  };
    elink_vec  = {19,   16,   23,   20,   24,   9 };
    vmm_vec    = {1,    2,    1,    2,    3,    4  };
    SCA_ID_vec = {993, 993, 1040,  1040,  1026, 1001};
  }
  
  HV_vec = {2800, 2900};

  //--------------------------------------------//

  for ( uint ith=0; ith<vmm_vec.size(); ith++ ) {
    
    for ( uint ivoltage = 0; ivoltage<HV_vec.size(); ivoltage++ ) {

      int ilayer  = layer_vec.at(ith);
      int ielink  = elink_vec.at(ith);
      int ivmm    = vmm_vec.at(ith);
      int iSCA_ID = SCA_ID_vec.at(ith);
      int idetect = detector_type;

      int ivolt   = HV_vec.at(ivoltage);

      std::cout << " layer " << ilayer << " ivmm " << ivmm << " voltage " << ivolt << "\n";

      // pFEBs
      if ( iSCA_ID == 1040 ||
	   iSCA_ID == 993 ) {

	GetChannelPlots( ilayer, true, ivmm, ivolt, data_type )->resize(0);

	for ( int ich=0; ich<64; ich++ ) {
	  chan_plots * tmp_chan_plots = new chan_plots();
	  tmp_chan_plots->SetParams( ilayer, ielink, ivmm, ich, iSCA_ID, 
				     chan_prop->getPadArea(   ilayer, ivmm, ich, idetect ),
				     chan_prop->getPad_PiCap( ilayer, ivmm, ich ),
				     true );
	  tmp_chan_plots->is_good = chan_prop->isGood( ilayer, ivmm, ich, true, data_type, detector_type );
	  GetChannelPlots( ilayer, true, ivmm, ivolt, data_type )->push_back( tmp_chan_plots );
	}
	
      }

      // sFEBs
      if ( iSCA_ID == 1001 ||
	   iSCA_ID == 1026 ) {

	GetChannelPlots( ilayer, false, ivmm, ivolt, data_type )->resize(0);

	for ( int ich=0; ich<64; ich++ ) {
	  chan_plots * tmp_chan_plots = new chan_plots();
	  tmp_chan_plots->SetParams( ilayer, ielink, ivmm, ich, iSCA_ID, 
				     chan_prop->getStripArea(        ilayer, ivmm, ich ),
				     chan_prop->getStrip_Resistance( ilayer, ivmm, ich ),
				     false );
          tmp_chan_plots->is_good = chan_prop->isGood( ilayer, ivmm, ich, false, data_type, detector_type );
	  GetChannelPlots( ilayer, false, ivmm, ivolt, data_type )->push_back( tmp_chan_plots );
	}
      }
      
    }
  }

}

void decoded_event_tree::initAnalogChannelPlots( int detector_type ) {

  //---------------------------------------------//
  //  init plots for all channels                  
  //---------------------------------------------//

  layer_vec_ana  = {3,    3,    4,    4,    3,    4  };
  elink_vec_ana  = {23,   20,   19,   16,   9,    24 };
  vmm_vec_ana    = {1,    2,    1,    2,    4,    3  };
  SCA_ID_vec_ana = {1040, 1040, 993,  993,  1001, 1026};


  HV_vec_ana = {2800};

  //--------------------------------------------//                                                                                                                                                                                                                            

  for ( uint ith=0; ith<vmm_vec_ana.size(); ith++ ) {

    for ( uint ivoltage = 0; ivoltage<HV_vec_ana.size(); ivoltage++ ) {

      int ilayer  = layer_vec_ana.at(ith);
      int ielink  = elink_vec_ana.at(ith);
      int ivmm    = vmm_vec_ana.at(ith);
      int iSCA_ID = SCA_ID_vec_ana.at(ith);
      
      int ivolt   = HV_vec_ana.at(ivoltage);

      std::cout << " layer " << ilayer << " ivmm " << ivmm << " voltage " << ivolt << "\n";

      // pFEBs                                                                                                                                                                                                                                                                
      if ( iSCA_ID == 1040 ||
           iSCA_ID == 993 ) {

        GetAnalogChannelPlots( ilayer, true, ivmm, ivolt )->resize(0);

        for ( int ich=0; ich<64; ich++ ) {
          chan_plots * tmp_chan_plots = new chan_plots();
          tmp_chan_plots->SetParams( ilayer, ielink, ivmm, ich, iSCA_ID,
                                     chan_prop->getPadArea(   ilayer, ivmm, ich, detector_type ),
                                     chan_prop->getPad_PiCap( ilayer, ivmm, ich ),
                                     true );
          GetAnalogChannelPlots( ilayer, true, ivmm, ivolt )->push_back( tmp_chan_plots );
        }

      }

      // sFEBs                                                                                                                                                                                                                                                                
      if ( iSCA_ID == 1001 ||
           iSCA_ID == 1026 ) {

        GetAnalogChannelPlots( ilayer, false, ivmm, ivolt )->resize(0);

        for ( int ich=0; ich<64; ich++ ) {
          chan_plots * tmp_chan_plots = new chan_plots();
          tmp_chan_plots->SetParams( ilayer, ielink, ivmm, ich, iSCA_ID,
                                     chan_prop->getStripArea(        ilayer, ivmm, ich ),
                                     chan_prop->getStrip_Resistance( ilayer, ivmm, ich ),
                                     false );
          GetAnalogChannelPlots( ilayer, false, ivmm, ivolt )->push_back( tmp_chan_plots );
        }
      }

    }
  }

}


void decoded_event_tree::initChannelComparePlots(int data_type) {


  for ( uint ith=0; ith<vmm_vec.size(); ith++ ) {

    for ( uint ivoltage = 0; ivoltage<HV_vec.size(); ivoltage++ ) {

      int ilayer  = layer_vec.at(ith);
      int ielink  = elink_vec.at(ith);
      int ivmm    = vmm_vec.at(ith);
      int iSCA_ID = SCA_ID_vec.at(ith);
      
      int ivolt   = HV_vec.at(ivoltage);
      
      if ( ilayer == 3 ) {

	if ( ivmm <= 2 ) {
	  
	  GetChannelComparePlots( ilayer, true,
				  ivmm,   ivolt, data_type )->resize(0);

	  std::pair<int, int> tmp_comp_vmm_ch = chan_prop->getComparablePadChan( ilayer, ivmm, 32 );
	  
	  int ivmm_compare = tmp_comp_vmm_ch.first;
	  int ilayer_compare = 4;
	  
	  std::cout << "layer " << ilayer << " vmm " << ivmm << " compared with " 
		    << "layer " << ilayer_compare << " vmm " << ivmm_compare << std::endl;
	  
	  std::vector<chan_plots*> * vmm1_ch_plot_vec;                                                     
	  std::vector<chan_plots*> * vmm2_ch_plot_vec;

	  vmm1_ch_plot_vec = GetChannelPlots( ilayer,         true, 
					      ivmm,           ivolt, data_type );
	  vmm2_ch_plot_vec = GetChannelPlots( ilayer_compare, true, 
					      ivmm_compare,   ivolt, data_type );

	  for ( int ich=0; ich<vmm1_ch_plot_vec->size(); ich++ ) {
	    
	    std::pair<int, int> tmp_comp_vmm_ch = chan_prop->getComparablePadChan( ilayer, ivmm, 
										   vmm1_ch_plot_vec->at(ich)->chan );
	    int ich_compare  = tmp_comp_vmm_ch.second;
	    
	    if ( ich_compare >= 0 ) {
	      
	      chan_plots * ch1 = vmm1_ch_plot_vec->at( ich );
	      chan_plots * ch2 = vmm2_ch_plot_vec->at( ich_compare );
	      
	      chan_compare_plots * tmp_ch_compare_plots = new chan_compare_plots();
	      tmp_ch_compare_plots->SetParams( ch1, ch2 );
	      
	      GetChannelComparePlots( ilayer, true,
				      ivmm,   ivolt, data_type )->push_back( tmp_ch_compare_plots );
	    }
	    
	  }
	}

      }

    } // loop over voltages

  } // loop over vmms 

}

double decoded_event_tree::GetMedian( TString filename ) {

  TFile * file_ana = new TFile(filename.Data(), "READ");

  TRandom3 * Tr = new TRandom3();
  TGraph * h_waveform = (TGraph*) file_ana->Get("Graph");
  std::vector<double> sample_points;
  std::vector<double> sorted_sample_points;
  double curr_point;
  double median;
  int npoint = h_waveform->GetN();
  int random_loc;
  int median_loc;

  Double_t * y_values = h_waveform->GetY();

  for ( int i = 1; i < 3000; i++ ) {
    random_loc = Tr->Rndm()*npoint;
    curr_point = y_values[random_loc];
    sample_points.push_back(curr_point);
  }

  sorted_sample_points = sort_vector(sample_points);
  median_loc = (sample_points.size() - 1) / 2;
  median = sorted_sample_points.at(median_loc);

  std::cout << "median = " << median <<std::endl;
  return median;
}

void decoded_event_tree::plotSummary(bool pads, int voltage, int data_type, int detector_type) {

  gStyle->SetOptStat(0);

  m_sx.str("");
  m_sx << "plot_summary_" ;
  if ( pads ) m_sx << "_pads";
  else        m_sx << "_strips";
  if ( data_type == type_10bit ) m_sx << "_10bit";
  else if ( data_type == type_6bit ) m_sx << "_6bit";
  m_sx << "_" << voltage;

  TString ps_name = m_sx.str();

  TPostScript * ps3;

  if ( detector_type == 1 ) {
    ps3 = new TPostScript(("plotsQL1/"+ps_name+".eps").Data(),112);
  }

  if ( detector_type == 2 ) {
    ps3 = new TPostScript(("plotsQS2/"+ps_name+".eps").Data(),112);
  }
    
  TCanvas * c_ch = new TCanvas(("c_ch"+ps_name).Data(), "c_ch");

  TH1F* hist_100pF_deadTime;
  TH1F* hist_100pF_deadTime_small;
  TH1F* hist_100pF_deadTime_large;

  TH1F* hist_200Ohm_deadTime;

  if ( pads ) {

  m_sx.str("");
  m_sx << "Pad ";
  if ( data_type == type_10bit ) m_sx << "10 bit ADC ";
  else if ( data_type == type_6bit ) m_sx << "ToT ";
  m_sx << "Fitted Dead Time with Different Pi-Networks at HV=";
  m_sx << voltage << " volts";

  hist_100pF_deadTime = new TH1F(("hist_100pF_deadtime"+ps_name).Data(), m_sx.str().c_str(), 50, 200, 400);
  hist_100pF_deadTime->SetXTitle("Channel Fitted Dead Time (ns)");
  hist_100pF_deadTime->SetYTitle("N Channels");
  hist_100pF_deadTime->SetLineColor(kBlack);
  hist_100pF_deadTime->SetFillColor(kTeal);
  hist_100pF_deadTime->SetFillStyle(3003);

  m_sx.str("");
  //For QL1
  if ( detector_type == 1 ) { 
    m_sx << "Pad with Size <= 55 cm^{2} ";
  }
  //For QS2
  if ( detector_type == 2 ) {
    m_sx << "Pad with Size <= 250 cm^{2} ";
  }
  
  if ( data_type == type_10bit ) m_sx << "10 Bit ADC ";
  else if ( data_type == type_6bit ) m_sx << "ToT ";
  m_sx << "Fitted Dead Time with Different Pi-Networks at HV=";
  m_sx << voltage << " volts";

  hist_100pF_deadTime_small = new TH1F(("hist_100pF_deadtime_small_"+ps_name).Data(), m_sx.str().c_str(), 50, 200, 400);
  hist_100pF_deadTime_small->SetXTitle("Channel Fitted Dead Time (ns)");
  hist_100pF_deadTime_small->SetYTitle("N Channels");
  hist_100pF_deadTime_small->SetLineColor(kBlack);
  hist_100pF_deadTime_small->SetFillColor(kTeal);
  hist_100pF_deadTime_small->SetFillStyle(3003);

  m_sx.str("");
  //For QL1
  if ( detector_type == 1 ) {
    m_sx << "Pad with Size > 55 cm^{2} ";
  }
  //For QS2
  if ( detector_type == 2 ) {
    m_sx << "Pad with Size > 250 cm^{2} ";
  }
  
  if ( data_type == type_10bit ) m_sx << "10 Bit ADC ";
  else if ( data_type == type_6bit ) m_sx << "ToT ";
  m_sx << "Fitted Dead Time with Different Pi-Networks at HV=";
  m_sx << voltage << " volts";

  hist_100pF_deadTime_large = new TH1F(("hist_100pF_deadtime_small_"+ps_name).Data(), m_sx.str().c_str(), 50, 200, 400);
  hist_100pF_deadTime_large->SetXTitle("Channel Fitted Dead Time (ns)");
  hist_100pF_deadTime_large->SetYTitle("N Channels");
  hist_100pF_deadTime_large->SetLineColor(kBlack);
  hist_100pF_deadTime_large->SetFillColor(kTeal);
  hist_100pF_deadTime_large->SetFillStyle(3003);

  }
  else {

  m_sx.str("");
  m_sx << "Strip ";
  if ( data_type == type_10bit ) m_sx << "10 Bit ADC ";
  else if ( data_type == type_6bit ) m_sx << "6 Bit ADC ";
  m_sx << "Fitted Dead Time with Different Pull-Up Resistors at HV=";
  m_sx << voltage << " volts";

  hist_200Ohm_deadTime = new TH1F(("hist_200Ohm_deadtime"+ps_name).Data(), m_sx.str().c_str(), 50, 200, 400);
  hist_200Ohm_deadTime->SetXTitle("Channel Fitted Dead Time (ns)");
  hist_200Ohm_deadTime->SetYTitle("N Channels");
  hist_200Ohm_deadTime->SetLineColor(kBlack);
  hist_200Ohm_deadTime->SetFillColor(kTeal);
  hist_200Ohm_deadTime->SetFillStyle(3003);

  }

  std::vector<int> layer_vec_tmp  = layer_vec;
  std::vector<int> elink_vec_tmp  = elink_vec;
  std::vector<int> vmm_vec_tmp    = vmm_vec;
  std::vector<int> SCA_ID_vec_tmp = SCA_ID_vec;
  std::vector<int> HV_vec_tmp     = HV_vec;

  std::vector<float> deadtime_150pF;
  std::vector<float> deadtime_200pF;
  std::vector<float> deadtime_300pF;
  std::vector<float> deadtime_470pF;

  std::vector<float> deadtime_300Ohm;
  std::vector<float> deadtime_500Ohm;
  std::vector<float> deadtime_1000Ohm;

  std::vector<float> deadtime_150pF_small;
  std::vector<float> deadtime_200pF_small;
  std::vector<float> deadtime_300pF_small;
  std::vector<float> deadtime_470pF_small;

  std::vector<float> deadtime_150pF_large;
  std::vector<float> deadtime_200pF_large;
  std::vector<float> deadtime_300pF_large;
  std::vector<float> deadtime_470pF_large;

  TArrow * a_150pF = new TArrow();
  TArrow * a_200pF = new TArrow();
  TArrow * a_300pF = new TArrow();
  TArrow * a_470pF = new TArrow();

  TArrow * a_300Ohm  = new TArrow();
  TArrow * a_500Ohm  = new TArrow();
  TArrow * a_1000Ohm = new TArrow();

  a_150pF->SetLineColor(kGreen+2);
  a_200pF->SetLineColor(kBlue);
  a_300pF->SetLineColor(kOrange+2);
  a_470pF->SetLineColor(kRed);

  a_300Ohm ->SetLineColor(kGreen+2);
  a_500Ohm ->SetLineColor(kBlue);
  a_1000Ohm->SetLineColor(kRed);

  float total_max_rate = 0;

  for ( uint ith=0; ith<vmm_vec_tmp.size(); ith++ ) {
    //std::cout << "voltage vector size: " << HV_vec_tmp.size() << std::endl;
    for ( uint ivoltage = 0; ivoltage<HV_vec_tmp.size(); ivoltage++ ) {

      int ilayer  = layer_vec_tmp.at(ith);
      int ielink  = elink_vec_tmp.at(ith);
      int ivmm    = vmm_vec_tmp.at(ith);
      int iSCA_ID = SCA_ID_vec_tmp.at(ith);
      
      int ivolt   = HV_vec_tmp.at(ivoltage);
      
      if ( ivolt != voltage ) continue;
      if ( pads &&  ( iSCA_ID != 993  && iSCA_ID != 1040 ) ) continue;
      if ( !pads && ( iSCA_ID != 1001 && iSCA_ID != 1026 ) ) continue;

      std::vector<chan_plots*> * vmm_ch_plot_vec = GetChannelPlots( ilayer, pads,
								    ivmm,   ivolt,  data_type );
      
      for ( uint ich=0; ich<vmm_ch_plot_vec->size(); ich++ ) {
	
	bool isgood = vmm_ch_plot_vec->at(ich)->is_good;
	float area  = vmm_ch_plot_vec->at(ich)->area;
	int  icap   = 0;
	int  ires   = 0;
	float deadtime = ( vmm_ch_plot_vec->at(ich)->f_expo_eff_tot->GetParameter(2)*1e6 );
	float max_rate = vmm_ch_plot_vec->at(ich)->max_tot_rate;

	if (  pads ) icap = vmm_ch_plot_vec->at(ich)->capacitance;
	if ( !pads ) ires = vmm_ch_plot_vec->at(ich)->resistance;
	
	if ( isgood ) {
	  
	  if ( total_max_rate < max_rate && max_rate < 10000 ) total_max_rate = max_rate;

	  if ( pads ) {	  
	  
	    if      ( icap == 100 ) hist_100pF_deadTime->Fill( deadtime );
	    else if ( icap == 150 ) deadtime_150pF.push_back( deadtime );
	    else if ( icap == 200 ) deadtime_200pF.push_back( deadtime );
	    else if ( icap == 300 ) deadtime_300pF.push_back( deadtime );
	    else if ( icap == 470 ) deadtime_470pF.push_back( deadtime );

	    //For QS2
	    if ( detector_type == 2 ) {

	      if ( area <= 250.0 ) {
	      
		if      ( icap == 100 ) hist_100pF_deadTime_small->Fill( deadtime );
		else if ( icap == 150 ) deadtime_150pF_small.push_back( deadtime );
		else if ( icap == 200 ) deadtime_200pF_small.push_back( deadtime );
		else if ( icap == 300 ) deadtime_300pF_small.push_back( deadtime );
		else if ( icap == 470 ) deadtime_470pF_small.push_back( deadtime );
	      
	      }
	      else {
	      
		if      ( icap == 100 ) hist_100pF_deadTime_large->Fill( deadtime );
		else if ( icap == 150 ) deadtime_150pF_large.push_back( deadtime );
		else if ( icap == 200 ) deadtime_200pF_large.push_back( deadtime );
		else if ( icap == 300 ) deadtime_300pF_large.push_back( deadtime );
		else if ( icap == 470 ) deadtime_470pF_large.push_back( deadtime );
	      }

	    }

	    //For QL1
	    if ( detector_type == 1 ) {

	      if ( area <= 55.0 ) {
	      
		if      ( icap == 100 ) hist_100pF_deadTime_small->Fill( deadtime );
		else if ( icap == 150 ) deadtime_150pF_small.push_back( deadtime );
		else if ( icap == 200 ) deadtime_200pF_small.push_back( deadtime );
		else if ( icap == 300 ) deadtime_300pF_small.push_back( deadtime );
		else if ( icap == 470 ) deadtime_470pF_small.push_back( deadtime );
	      
	      }
	      else {
	      
		if      ( icap == 100 ) hist_100pF_deadTime_large->Fill( deadtime );
		else if ( icap == 150 ) deadtime_150pF_large.push_back( deadtime );
		else if ( icap == 200 ) deadtime_200pF_large.push_back( deadtime );
		else if ( icap == 300 ) deadtime_300pF_large.push_back( deadtime );
		else if ( icap == 470 ) deadtime_470pF_large.push_back( deadtime );
	      }

	    }
	    
	  }
	  else if ( !pads ) {

	    //For QL1
	    /*
	    if ( ilayer == 3 && ivmm == 4 ) {
	      if      ( ires == 200  ) hist_200Ohm_deadTime->Fill( vmm_ch_plot_vec->at(ich)->f_expo_eff_tot->GetParameter(2)*1e6 );
	      else if ( ires == 300  ) deadtime_300Ohm.push_back( deadtime );
	      else if ( ires == 500  ) deadtime_500Ohm.push_back( deadtime );
	      else if ( ires == 1000 ) deadtime_1000Ohm.push_back( deadtime );
	      std::cout << "Deadtime " << deadtime << std::endl;
	    }
	    */

	    //For QS2
	    if ( ilayer == 4 && ivmm == 4 ) {
	      if ( ires == 200  ) {
		hist_200Ohm_deadTime->Fill( vmm_ch_plot_vec->at(ich)->f_expo_eff_tot->GetParameter(2)*1e6 );
		//std::cout << "200 Deadtime " << vmm_ch_plot_vec->at(ich)->f_expo_eff_tot->GetParameter(2)*1e6 << std::endl;
	      }
	      else if ( ires == 300  ) {
		//std::cout << "300 Deadtime " << deadtime << std::endl;
		deadtime_300Ohm.push_back( deadtime );
	      }
	      else if ( ires == 500  ) {
		//std::cout << "500 Deadtime " << deadtime << std::endl;
		deadtime_500Ohm.push_back( deadtime );
	      }
	      else if ( ires == 1000 ) {
		//std::cout << "1000 Deadtime " << deadtime << std::endl;
		deadtime_1000Ohm.push_back( deadtime );
	      }
	    }
	  }
	}
      } // loop over channels
      //std::cout << "I have finished channel looping" << std::endl;
    } // loop over voltages
    std::cout << "I have finished voltage looping for VMM " << ith << " For pads? " << pads << std::endl;
  } // loop over vmms
  std::cout << "I have looped over this vmm" << std::endl;

  c_ch->cd();
  
  if ( pads ) {
    hist_100pF_deadTime->Draw();

    float maximum = hist_100pF_deadTime->GetMaximum();

    for ( uint i=0; i< deadtime_150pF.size(); i++ ) {
      a_150pF->DrawArrow(deadtime_150pF.at(i), 0, 
			 deadtime_150pF.at(i), maximum*0.2, 0.01, "<");
    }
    for ( uint i=0; i< deadtime_200pF.size(); i++ ) {
      a_200pF->DrawArrow(deadtime_200pF.at(i), 0,
			 deadtime_200pF.at(i), maximum*0.2, 0.01, "<");
    }
    for ( uint i=0; i< deadtime_300pF.size(); i++ ) {
      a_300pF->DrawArrow(deadtime_300pF.at(i), 0,
			 deadtime_300pF.at(i), maximum*0.2, 0.01, "<");
    }
    for ( uint i=0; i< deadtime_470pF.size(); i++ ) {
      a_470pF->DrawArrow(deadtime_470pF.at(i), 0,
			 deadtime_470pF.at(i), maximum*0.2, 0.01, "<");
    }

    leg_top->Clear();
    leg_top->AddEntry( hist_100pF_deadTime, "100 pF Channels", "f");
    leg_top->AddEntry( a_150pF,             "150 pF Channels", "l");
    leg_top->AddEntry( a_200pF,             "200 pF Channels", "l");
    leg_top->AddEntry( a_300pF,             "300 pF Channels", "l");
    leg_top->AddEntry( a_470pF,             "470 pF Channels", "l");
    leg_top->Draw();

    c_ch->Update();
    ps3->NewPage();

    //-----------------------------//

    hist_100pF_deadTime_small->Draw();

    maximum = hist_100pF_deadTime_small->GetMaximum();

    for ( uint i=0; i< deadtime_150pF_small.size(); i++ ) {
      a_150pF->DrawArrow(deadtime_150pF_small.at(i), 0,
                         deadtime_150pF_small.at(i), maximum*0.2, 0.01, "<");
    }
    for ( uint i=0; i< deadtime_200pF_small.size(); i++ ) {
      a_200pF->DrawArrow(deadtime_200pF_small.at(i), 0,
                         deadtime_200pF_small.at(i), maximum*0.2, 0.01, "<");
    }
    for ( uint i=0; i< deadtime_300pF_small.size(); i++ ) {
      a_300pF->DrawArrow(deadtime_300pF_small.at(i), 0,
                         deadtime_300pF_small.at(i), maximum*0.2, 0.01, "<");
    }
    for ( uint i=0; i< deadtime_470pF_small.size(); i++ ) {
      a_470pF->DrawArrow(deadtime_470pF_small.at(i), 0,
                         deadtime_470pF_small.at(i), maximum*0.2, 0.01, "<");
    }

    leg_top->Clear();
    leg_top->AddEntry( hist_100pF_deadTime_small, "100 pF Channels", "f");
    leg_top->AddEntry( a_150pF,             "150 pF Channels", "l");
    leg_top->AddEntry( a_200pF,             "200 pF Channels", "l");
    leg_top->AddEntry( a_300pF,             "300 pF Channels", "l");
    leg_top->AddEntry( a_470pF,             "470 pF Channels", "l");
    leg_top->Draw();

    c_ch->Update();
    ps3->NewPage();

    //---------------------//

    hist_100pF_deadTime_large->Draw();
    
    maximum = hist_100pF_deadTime_large->GetMaximum();

    for ( uint i=0; i< deadtime_150pF_large.size(); i++ ) {
      a_150pF->DrawArrow(deadtime_150pF_large.at(i), 0,
                         deadtime_150pF_large.at(i), maximum*0.2, 0.01, "<");
    }
    for ( uint i=0; i< deadtime_200pF_large.size(); i++ ) {
      a_200pF->DrawArrow(deadtime_200pF_large.at(i), 0,
                         deadtime_200pF_large.at(i), maximum*0.2, 0.01, "<");
    }
    for ( uint i=0; i< deadtime_300pF_large.size(); i++ ) {
      a_300pF->DrawArrow(deadtime_300pF_large.at(i), 0,
                         deadtime_300pF_large.at(i), maximum*0.2, 0.01, "<");
    }
    for ( uint i=0; i< deadtime_470pF_large.size(); i++ ) {
      a_470pF->DrawArrow(deadtime_470pF_large.at(i), 0,
                         deadtime_470pF_large.at(i), maximum*0.2, 0.01, "<");
    }

    leg_top->Clear();
    leg_top->AddEntry( hist_100pF_deadTime_large, "100 pF Channels", "f");
    leg_top->AddEntry( a_150pF,             "150 pF Channels", "l");
    leg_top->AddEntry( a_200pF,             "200 pF Channels", "l");
    leg_top->AddEntry( a_300pF,             "300 pF Channels", "l");
    leg_top->AddEntry( a_470pF,             "470 pF Channels", "l");
    leg_top->Draw();

    c_ch->Update();
    ps3->NewPage();

  }
  else {
    hist_200Ohm_deadTime->Draw();

    float maximum = hist_200Ohm_deadTime->GetMaximum();

    for ( uint i=0; i< deadtime_300Ohm.size(); i++ ) {
      a_300Ohm->DrawArrow(deadtime_300Ohm.at(i), 0,
			   deadtime_300Ohm.at(i), maximum*0.2, 0.01, "<");
    }
    for ( uint i=0; i< deadtime_500Ohm.size(); i++ ) {
      a_500Ohm->DrawArrow(deadtime_500Ohm.at(i), 0,
			   deadtime_500Ohm.at(i), maximum*0.2, 0.01, "<");
    }
    for ( uint i=0; i< deadtime_1000Ohm.size(); i++ ) {
      a_1000Ohm->DrawArrow(deadtime_1000Ohm.at(i), 0,
			    deadtime_1000Ohm.at(i), maximum*0.2, 0.01, "<");
    }

    leg_top->Clear();
    leg_top->AddEntry( hist_200Ohm_deadTime, "200 k#Omega Channels",  "f");
    leg_top->AddEntry( a_300Ohm,             "300 k#Omega Channels",  "l");
    leg_top->AddEntry( a_500Ohm,             "500 k#Omega Channels",  "l");
    leg_top->AddEntry( a_1000Ohm,            "  1 M#Omega Channels", "l");
    leg_top->Draw();

    c_ch->Update();
    ps3->NewPage();
    std::cout << "I have finished this loop 1" << std::endl;
  }
  
  //----------------------------------------------------------------------//
  //                    Plot all Efficiency Curves
  //----------------------------------------------------------------------//

  gPad->SetGridx(1);
  gPad->SetGridy(1);

  TH1F* hist_eff = new TH1F(("hist_eff_"+ps_name).Data(), "", 1, 0, total_max_rate);

  m_sx.str("");
  if ( pads )                                 m_sx << "Efficiency vs Hit Rate for Pads";
  else                                        m_sx << "Efficiency vs Hit Rate for Strips";
  if ( data_type == type_10bit )              m_sx << " with 10 Bit ADC mode ";
  else if ( pads  && data_type == type_6bit ) m_sx << " with ToT mode ";
  else if ( !pads && data_type == type_6bit ) m_sx << " with 6 Bit ADC mode ";
  if ( pads )                                 m_sx << "For Different Pi-Networks at HV=";
  else                                        m_sx << "For Different Pull-Up Resistors at HV=";
  m_sx << voltage << " volts";

  hist_eff->SetTitle(m_sx.str().c_str());
  hist_eff->SetXTitle("Channel Hit Rate (kHz)");
  hist_eff->SetYTitle("Efficiency");
  hist_eff->SetMaximum(1.05);
  hist_eff->SetMinimum(0.40);

  hist_eff->Draw();
  
  for ( uint itry = 0; itry<2; itry ++ ) {
    
    for ( uint ith=0; ith<vmm_vec_tmp.size(); ith++ ) {
      
      for ( uint ivoltage = 0; ivoltage<HV_vec_tmp.size(); ivoltage++ ) {
	
	int ilayer  = layer_vec_tmp.at(ith);
	int ielink  = elink_vec_tmp.at(ith);
	int ivmm    = vmm_vec_tmp.at(ith);
	int iSCA_ID = SCA_ID_vec_tmp.at(ith);
	
	int ivolt   = HV_vec_tmp.at(ivoltage);
	
	if ( ivolt != voltage ) continue;
	if ( pads &&  ( iSCA_ID != 993  && iSCA_ID != 1040 ) ) continue;
	if ( !pads && ( iSCA_ID != 1001 && iSCA_ID != 1026 ) ) continue;
	
	std::vector<chan_plots*> * vmm_ch_plot_vec = GetChannelPlots( ilayer, pads,
								      ivmm,   ivolt,  data_type );
	
	for ( uint ich=0; ich<vmm_ch_plot_vec->size(); ich++ ) {
	  
	  bool isgood = vmm_ch_plot_vec->at(ich)->is_good;
	  int  icap = -1;
	  int  ires = -1;
	  if (  pads ) icap = vmm_ch_plot_vec->at(ich)->capacitance;
          if ( !pads ) ires = vmm_ch_plot_vec->at(ich)->resistance;

	  if ( isgood ) {
	    if ( itry == 0 && (  pads && icap != 100 ) ) continue;
	    if ( itry == 1 && (  pads && icap == 100 ) ) continue;
            if ( itry == 0 && ( !pads && ires != 200 ) ) continue;
            if ( itry == 1 && ( !pads && ires == 200 ) ) continue;
	    vmm_ch_plot_vec->at(ich)->f_expo_eff_tot->Draw("same");

	    if ( pads) {
	      if ( icap == 100 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kTeal);
	      if ( icap == 150 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kGreen+2);
	      if ( icap == 200 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kBlue);
	      if ( icap == 300 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kOrange+2);
	      if ( icap == 470 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kRed);
	    }
	    else {
	      if ( ires == 200 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kTeal);
	      if ( ires == 300 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kGreen+2);
	      if ( ires == 500 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kBlue);
	      if ( ires == 1000 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kRed);
	    }

	    vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.Draw("p");
	  }
	}
	
      }
    }

  }
  leg_top->Draw();

  c_ch->Update();
  ps3->NewPage();

  hist_eff->Draw();

  for ( uint itry = 0; itry<2; itry ++ ) {

    for ( uint ith=0; ith<vmm_vec_tmp.size(); ith++ ) {

      for ( uint ivoltage = 0; ivoltage<HV_vec_tmp.size(); ivoltage++ ) {

	int ilayer  = layer_vec_tmp.at(ith);
	int ielink  = elink_vec_tmp.at(ith);
	int ivmm    = vmm_vec_tmp.at(ith);
	int iSCA_ID = SCA_ID_vec_tmp.at(ith);

	int ivolt   = HV_vec_tmp.at(ivoltage);

	if ( ivolt != voltage ) continue;
	if ( pads &&  ( iSCA_ID != 993  && iSCA_ID != 1040 ) ) continue;
	if ( !pads && ( iSCA_ID != 1001 && iSCA_ID != 1026 ) ) continue;

	std::vector<chan_plots*> * vmm_ch_plot_vec = GetChannelPlots( ilayer, pads,
								      ivmm,   ivolt,  data_type );

	for ( uint ich=0; ich<vmm_ch_plot_vec->size(); ich++ ) {

	  bool isgood = vmm_ch_plot_vec->at(ich)->is_good;
	  int  icap = -1;
	  int  ires = -1;
	  if (  pads ) icap = vmm_ch_plot_vec->at(ich)->capacitance;
	  if ( !pads ) ires = vmm_ch_plot_vec->at(ich)->resistance;

	  if ( isgood ) {
	    if ( itry == 0 && (  pads && icap != 100 ) ) continue;
	    if ( itry == 1 && (  pads && icap == 100 ) ) continue;
	    if ( itry == 0 && ( !pads && ires != 200 ) ) continue;
	    if ( itry == 1 && ( !pads && ires == 200 ) ) continue;
	    //if ( itry == 1 ) continue;
	    //vmm_ch_plot_vec->at(ich)->f_1overX_eff_tot->Draw("same");

	    if ( pads) {
	      if ( icap == 100 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kTeal);
	      if ( icap == 150 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kGreen+2);
	      if ( icap == 200 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kBlue);
	      if ( icap == 300 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kOrange+2);
	      if ( icap == 470 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kRed);
	    }
	    else {
	      if ( ires == 200 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kTeal);
	      if ( ires == 300 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kGreen+2);
	      if ( ires == 500 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kBlue);
	      if ( ires == 1000 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kRed);
	    }

	    //std::cout << "I have set the color" << std::endl;
	    vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.Draw("p");

	  }
	}

      }
    }

  }
  leg_top->Draw();

  c_ch->Update();
  ps3->NewPage();
  
  //----------------------------------------------------------------------//

  if ( pads ) {

    m_sx.str("");
    //For QL1
    if ( detector_type == 1 ) {
      m_sx << "Efficiency vs Hit Rate for Pads with Size > 55 cm^{2} ";
    }
    //For QS2
    if ( detector_type == 2 ) {
      m_sx << "Efficiency vs Hit Rate for Pads with Size > 250 cm^{2} ";
    }
    if ( data_type == type_10bit ) m_sx << " with 10 Bit ADC mode ";
    else if ( data_type == type_6bit ) m_sx << " with ToT mode ";
    m_sx << "For Different Pi-Networks at HV=";
    m_sx << voltage << " volts";

    hist_eff->SetTitle(m_sx.str().c_str());
    hist_eff->SetXTitle("Channel Hit Rate (kHz)");
    hist_eff->SetYTitle("Efficiency");
    hist_eff->SetMaximum(1.05);
    hist_eff->SetMinimum(0.40);


    hist_eff->Draw();

    for ( uint itry = 0; itry<2; itry ++ ) {

      for ( uint ith=0; ith<vmm_vec_tmp.size(); ith++ ) {
	
	for ( uint ivoltage = 0; ivoltage<HV_vec_tmp.size(); ivoltage++ ) {

	  int ilayer  = layer_vec_tmp.at(ith);
	  int ielink  = elink_vec_tmp.at(ith);
	  int ivmm    = vmm_vec_tmp.at(ith);
	  int iSCA_ID = SCA_ID_vec_tmp.at(ith);
	  
	  int ivolt   = HV_vec_tmp.at(ivoltage);
	  
	  if ( ivolt != voltage ) continue;
	  if ( pads &&  ( iSCA_ID != 993  && iSCA_ID != 1040 ) ) continue;
	  if ( !pads && ( iSCA_ID != 1001 && iSCA_ID != 1026 ) ) continue;
	  
	  std::vector<chan_plots*> * vmm_ch_plot_vec = GetChannelPlots( ilayer, pads,
									ivmm,   ivolt,  data_type );
	  
	  for ( uint ich=0; ich<vmm_ch_plot_vec->size(); ich++ ) {
	    
	    bool isgood = vmm_ch_plot_vec->at(ich)->is_good;
	    float area  = vmm_ch_plot_vec->at(ich)->area;

	    int  icap = -1;
	    int  ires = -1;
	    if (  pads ) icap = vmm_ch_plot_vec->at(ich)->capacitance;
	    if ( !pads ) ires = vmm_ch_plot_vec->at(ich)->resistance;
	    
	    //if ( isgood && area > 55.0) {
	    //For QS2
	    if ( isgood && area > 250.0) {
	      if ( itry == 0 && ( pads &&icap !=100 ) ) continue;
	      if ( itry == 1 && (  pads && icap == 100 ) ) continue;
	      if ( itry == 0 && ( !pads && ires != 200 ) ) continue;
	      if ( itry == 1 && ( !pads && ires == 200 ) ) continue;
	      vmm_ch_plot_vec->at(ich)->f_expo_eff_tot->Draw("same");

	      if ( pads) {
		if ( icap == 100 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kTeal);
		if ( icap == 150 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kGreen+2);
		if ( icap == 200 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kBlue);
		if ( icap == 300 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kOrange+2);
		if ( icap == 470 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kRed);
	      }
	      else {
		if ( ires == 200 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kTeal);
		if ( ires == 300 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kGreen+2);
		if ( ires == 500 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kBlue);
		if ( ires == 1000 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kRed);
	      }


	      vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.Draw("p");
	    }
	    
      
	  }
	  
	}
      }
    }

    leg_top->Draw();

    c_ch->Update();
    ps3->NewPage();

    hist_eff->Draw();

    for ( uint itry = 0; itry<2; itry ++ ) {

      for ( uint ith=0; ith<vmm_vec_tmp.size(); ith++ ) {

	for ( uint ivoltage = 0; ivoltage<HV_vec_tmp.size(); ivoltage++ ) {

	  int ilayer  = layer_vec_tmp.at(ith);
	  int ielink  = elink_vec_tmp.at(ith);
	  int ivmm    = vmm_vec_tmp.at(ith);
	  int iSCA_ID = SCA_ID_vec_tmp.at(ith);

	  int ivolt   = HV_vec_tmp.at(ivoltage);

	  if ( ivolt != voltage ) continue;
	  if ( pads &&  ( iSCA_ID != 993  && iSCA_ID != 1040 ) ) continue;
	  if ( !pads && ( iSCA_ID != 1001 && iSCA_ID != 1026 ) ) continue;

	  std::vector<chan_plots*> * vmm_ch_plot_vec = GetChannelPlots( ilayer, pads,
									ivmm,   ivolt,  data_type );

	  for ( uint ich=0; ich<vmm_ch_plot_vec->size(); ich++ ) {

	    bool isgood = vmm_ch_plot_vec->at(ich)->is_good;
	    float area  = vmm_ch_plot_vec->at(ich)->area;
	    int  icap = -1;
	    int  ires = -1;
	    if (  pads ) icap = vmm_ch_plot_vec->at(ich)->capacitance;
	    if ( !pads ) ires = vmm_ch_plot_vec->at(ich)->resistance;

	    if ( isgood && area > 250.0) {
	      
	      if ( itry == 0 && ( pads &&icap !=100 ) ) continue;
	      if ( itry == 1 && (  pads && icap == 100 ) ) continue;
	      if ( itry == 0 && ( !pads && ires != 200 ) ) continue;
	      if ( itry == 1 && ( !pads && ires == 200 ) ) continue;
	      //if ( itry == 1 ) continue;

	      //              vmm_ch_plot_vec->at(ich)->f_1overX_eff_tot->Draw("same");

	      if ( pads) {
		if ( icap == 100 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kTeal);
		if ( icap == 150 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kGreen+2);
		if ( icap == 200 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kBlue);
		if ( icap == 300 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kOrange+2);
		if ( icap == 470 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kRed);
	      }
	      else {
		if ( ires == 200 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kTeal);
		if ( ires == 300 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kGreen+2);
		if ( ires == 500 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kBlue);
		if ( ires == 1000 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kRed);
	      }


	      vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.Draw("p");
	      //std::cout << "Plotting large; layer " << ilayer << " vmm " << ivmm << " ch " << ich << " " << area << std::endl;
	      
	    }

	  }

	}
      }
    }

    leg_top->Draw();

    c_ch->Update();
    ps3->NewPage();
    
    //--------------------------------------------------------------//

    m_sx.str("");
    //m_sx << "Efficiency vs Hit Rate for Pads with Size <= 55 cm^{2} ";
    //For QS2
    m_sx << "Efficiency vs Hit Rate for Pads with Size <= 250 cm^{2} ";
    if ( data_type == type_10bit ) m_sx << " with 10 Bit ADC mode ";
    else if ( data_type == type_6bit ) m_sx << " with ToT mode ";
    m_sx << "For Different Pi-Networks at HV=";
    m_sx << voltage << " volts";
    
    hist_eff->SetTitle(m_sx.str().c_str());
    hist_eff->SetXTitle("Channel Hit Rate (kHz)");
    hist_eff->SetYTitle("Efficiency");
    hist_eff->SetMaximum(1.05);
    
    hist_eff->Draw();

    for ( uint itry = 0; itry<2; itry ++ ) {
    
      for ( uint ith=0; ith<vmm_vec_tmp.size(); ith++ ) {
	
	for ( uint ivoltage = 0; ivoltage<HV_vec_tmp.size(); ivoltage++ ) {
	  
	  int ilayer  = layer_vec_tmp.at(ith);
	  int ielink  = elink_vec_tmp.at(ith);
	  int ivmm    = vmm_vec_tmp.at(ith);
	  int iSCA_ID = SCA_ID_vec_tmp.at(ith);
	  
	  int ivolt   = HV_vec_tmp.at(ivoltage);
	  
	  if ( ivolt != voltage ) continue;
	  if ( pads &&  ( iSCA_ID != 993  && iSCA_ID != 1040 ) ) continue;
	  if ( !pads && ( iSCA_ID != 1001 && iSCA_ID != 1026 ) ) continue;
	  
	  std::vector<chan_plots*> * vmm_ch_plot_vec = GetChannelPlots( ilayer, pads,
									ivmm,   ivolt,  data_type );
	  
	  for ( uint ich=0; ich<vmm_ch_plot_vec->size(); ich++ ) {
	    
	    bool isgood = vmm_ch_plot_vec->at(ich)->is_good;
	    float area  = vmm_ch_plot_vec->at(ich)->area;
	    
	    int  icap = -1;
	    int  ires = -1;
	    if (  pads ) icap = vmm_ch_plot_vec->at(ich)->capacitance;
	    if ( !pads ) ires = vmm_ch_plot_vec->at(ich)->resistance;
	    
	    //if ( isgood && area <= 55.0) {
	    //For QS2
	    if ( isgood && area <= 250.0) {
	      
	      if ( itry == 0 && (  pads && icap != 100 ) ) continue;
	      if ( itry == 1 && (  pads && icap == 100 ) ) continue;
	      if ( itry == 0 && ( !pads && ires != 200 ) ) continue;
	      if ( itry == 1 && ( !pads && ires == 200 ) ) continue;
	      vmm_ch_plot_vec->at(ich)->f_expo_eff_tot->Draw("same");

	      if ( pads ) {
		if ( icap == 100 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kTeal);
		if ( icap == 150 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kGreen+2);
		if ( icap == 200 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kBlue);
		if ( icap == 300 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kOrange+2);
		if ( icap == 470 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kRed);
	      }
	      else {
		if ( ires == 200 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kTeal);
		if ( ires == 300 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kGreen+2);
		if ( ires == 500 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kBlue);
		if ( ires == 1000 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kRed);
	      }

	      vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.Draw("p");
	      //std::cout << "Plotting small; layer " << ilayer << " vmm " << ivmm << " ch " << ich << " " << area << std::endl;
	     
	    }
	    
	  }
	}
      }
    }

    leg_top->Draw();

    c_ch->Update();
    ps3->NewPage();

    hist_eff->Draw();

    for ( uint itry = 0; itry<2; itry ++ ) {

      for ( uint ith=0; ith<vmm_vec_tmp.size(); ith++ ) {

	for ( uint ivoltage = 0; ivoltage<HV_vec_tmp.size(); ivoltage++ ) {

	  int ilayer  = layer_vec_tmp.at(ith);
	  int ielink  = elink_vec_tmp.at(ith);
	  int ivmm    = vmm_vec_tmp.at(ith);
	  int iSCA_ID = SCA_ID_vec_tmp.at(ith);

	  int ivolt   = HV_vec_tmp.at(ivoltage);

	  if ( ivolt != voltage ) continue;
	  if ( pads &&  ( iSCA_ID != 993  && iSCA_ID != 1040 ) ) continue;
	  if ( !pads && ( iSCA_ID != 1001 && iSCA_ID != 1026 ) ) continue;

	  std::vector<chan_plots*> * vmm_ch_plot_vec = GetChannelPlots( ilayer, pads,
									ivmm,   ivolt,  data_type );

	  for ( uint ich=0; ich<vmm_ch_plot_vec->size(); ich++ ) {

	    bool isgood = vmm_ch_plot_vec->at(ich)->is_good;
	    float area  = vmm_ch_plot_vec->at(ich)->area;
	    int  icap = -1;
	    int  ires = -1;
	    if (  pads ) icap = vmm_ch_plot_vec->at(ich)->capacitance;
	    if ( !pads ) ires = vmm_ch_plot_vec->at(ich)->resistance;

	    if ( isgood && area <= 250.0) {
	      
	      if ( itry == 0 && ( pads &&icap !=100 ) ) continue;
	      if ( itry == 1 && (  pads && icap == 100 ) ) continue;
	      if ( itry == 0 && ( !pads && ires != 200 ) ) continue;
	      if ( itry == 1 && ( !pads && ires == 200 ) ) continue;
	      //if ( itry == 1 ) continue;

	      //vmm_ch_plot_vec->at(ich)->f_1overX_eff_tot->Draw("same");

	      if ( pads) {
		if ( icap == 100 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kTeal);
		if ( icap == 150 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kGreen+2);
		if ( icap == 200 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kBlue);
		if ( icap == 300 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kOrange+2);
		if ( icap == 470 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kRed);
	      }
	      else {
		if ( ires == 200 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kTeal);
		if ( ires == 300 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kGreen+2);
		if ( ires == 500 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kBlue);
		if ( ires == 1000 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kRed);
	      }


	      vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.Draw("p");
	    }

	  }

	}
      }
    }

    leg_top->Draw();

    c_ch->Update();
    ps3->NewPage();
    
  }

  //------------------------------------------------------------------------------//
  //             Make a Collective Plot of Hit Rate vs KEK Rate
  //------------------------------------------------------------------------------//

  m_sx.str("");
  if ( pads )                                 m_sx << "Pad Hit Rate vs KEK Hit Rate";
  else                                        m_sx << "Strip Hit Rate vs KEK Hit Rate";
  if ( data_type == type_10bit )              m_sx << " with 10 Bit ADC mode ";
  else if ( pads  && data_type == type_6bit ) m_sx << " with ToT mode ";
  else if ( !pads && data_type == type_6bit ) m_sx << " with 6 Bit ADC mode ";
  if ( pads )                                 m_sx << "For Different Pi-Network Channels at HV=";
  else                                        m_sx << "For Different Pull-Up Resistor Channels at HV=";
  m_sx << voltage << " volts";

  hist_eff->SetTitle(m_sx.str().c_str());
  hist_eff->SetXTitle("KEK Hit Rate (kHz)");
  hist_eff->SetYTitle("Detector Hit Rate (kHz)");
  hist_eff->SetBins(1, 0, total_max_rate*1.1 );
  hist_eff->SetMaximum(   total_max_rate*1.1);
  hist_eff->SetMinimum(0);

  if ( pads ) {

    std::vector<chan_plots*> * pad_vmm_ch_vec = GetChannelPlots( 3, pads,
								 1, voltage, data_type );

    TGraph ch_100pF_hit_rate_over_eff = pad_vmm_ch_vec->at(39)->hits_over_eff_vs_KEK_rate_tot;
    TGraph ch_100pF_hit_rate          = pad_vmm_ch_vec->at(39)->hits_vs_KEK_rate_tot;
    TGraph ch_150pF_hit_rate_over_eff = pad_vmm_ch_vec->at(40)->hits_over_eff_vs_KEK_rate_tot;
    TGraph ch_150pF_hit_rate          = pad_vmm_ch_vec->at(40)->hits_vs_KEK_rate_tot;
    TGraph ch_200pF_hit_rate_over_eff = pad_vmm_ch_vec->at(41)->hits_over_eff_vs_KEK_rate_tot;
    TGraph ch_200pF_hit_rate          = pad_vmm_ch_vec->at(41)->hits_vs_KEK_rate_tot;
    TGraph ch_300pF_hit_rate_over_eff = pad_vmm_ch_vec->at(44)->hits_over_eff_vs_KEK_rate_tot;
    TGraph ch_300pF_hit_rate          = pad_vmm_ch_vec->at(44)->hits_vs_KEK_rate_tot;
    TGraph ch_470pF_hit_rate_over_eff = pad_vmm_ch_vec->at(45)->hits_over_eff_vs_KEK_rate_tot;
    TGraph ch_470pF_hit_rate          = pad_vmm_ch_vec->at(45)->hits_vs_KEK_rate_tot;

    TGraph ch_100pF_eff_tot = pad_vmm_ch_vec->at(39)->eff_vs_photon_rate_tot;
    TGraph ch_100pF_eff     = pad_vmm_ch_vec->at(39)->eff_vs_KEK_rate_tot;
    TGraph ch_150pF_eff_tot = pad_vmm_ch_vec->at(40)->eff_vs_photon_rate_tot;
    TGraph ch_150pF_eff     = pad_vmm_ch_vec->at(40)->eff_vs_KEK_rate_tot;
    TGraph ch_200pF_eff_tot = pad_vmm_ch_vec->at(41)->eff_vs_photon_rate_tot;
    TGraph ch_200pF_eff     = pad_vmm_ch_vec->at(41)->eff_vs_KEK_rate_tot;
    TGraph ch_300pF_eff_tot = pad_vmm_ch_vec->at(53)->eff_vs_photon_rate_tot;
    TGraph ch_300pF_eff     = pad_vmm_ch_vec->at(53)->eff_vs_KEK_rate_tot;
    TGraph ch_470pF_eff_tot = pad_vmm_ch_vec->at(52)->eff_vs_photon_rate_tot;
    TGraph ch_470pF_eff     = pad_vmm_ch_vec->at(60)->eff_vs_KEK_rate_tot;

    ch_100pF_hit_rate_over_eff .SetMarkerColor(kTeal);
    ch_100pF_hit_rate          .SetMarkerColor(kTeal);
    ch_150pF_hit_rate_over_eff .SetMarkerColor(kGreen+2);
    ch_150pF_hit_rate          .SetMarkerColor(kGreen+2);
    ch_200pF_hit_rate_over_eff .SetMarkerColor(kBlue);
    ch_200pF_hit_rate          .SetMarkerColor(kBlue);
    ch_300pF_hit_rate_over_eff .SetMarkerColor(kOrange+2);
    ch_300pF_hit_rate          .SetMarkerColor(kOrange+2);
    ch_470pF_hit_rate_over_eff .SetMarkerColor(kRed);
    ch_470pF_hit_rate          .SetMarkerColor(kRed);

    ch_100pF_hit_rate_over_eff .SetMarkerSize(0.8);
    ch_100pF_hit_rate          .SetMarkerSize(0.8);
    ch_150pF_hit_rate_over_eff .SetMarkerSize(0.8);
    ch_150pF_hit_rate          .SetMarkerSize(0.8);
    ch_200pF_hit_rate_over_eff .SetMarkerSize(0.8);
    ch_200pF_hit_rate          .SetMarkerSize(0.8);
    ch_300pF_hit_rate_over_eff .SetMarkerSize(0.8);
    ch_300pF_hit_rate          .SetMarkerSize(0.8);
    ch_470pF_hit_rate_over_eff .SetMarkerSize(0.8);
    ch_470pF_hit_rate          .SetMarkerSize(0.8);

    ch_100pF_eff_tot.SetMarkerColor(kTeal);
    ch_100pF_eff   .SetMarkerColor(kTeal);
    ch_150pF_eff_tot.SetMarkerColor(kGreen+2);
    ch_150pF_eff   .SetMarkerColor(kGreen+2);
    ch_200pF_eff_tot.SetMarkerColor(kBlue);
    ch_200pF_eff   .SetMarkerColor(kBlue);
    ch_300pF_eff_tot.SetMarkerColor(kOrange+2);
    ch_300pF_eff   .SetMarkerColor(kOrange+2);
    ch_470pF_eff_tot.SetMarkerColor(kRed);
    ch_470pF_eff   .SetMarkerColor(kRed);

    ch_100pF_eff_tot.SetLineColor(kTeal);
    ch_100pF_eff   .SetLineColor(kTeal);
    ch_150pF_eff_tot.SetLineColor(kGreen+2);
    ch_150pF_eff   .SetLineColor(kGreen+2);
    ch_200pF_eff_tot.SetLineColor(kBlue);
    ch_200pF_eff   .SetLineColor(kBlue);
    ch_300pF_eff_tot.SetLineColor(kOrange+2);
    ch_300pF_eff   .SetLineColor(kOrange+2);
    ch_470pF_eff_tot.SetLineColor(kRed);
    ch_470pF_eff   .SetLineColor(kRed);

    ch_100pF_eff_tot.SetMarkerSize(0.8);
    ch_100pF_eff     .SetMarkerSize(0.8);
    ch_150pF_eff_tot.SetMarkerSize(0.8);
    ch_150pF_eff     .SetMarkerSize(0.8);
    ch_200pF_eff_tot.SetMarkerSize(0.8);
    ch_200pF_eff     .SetMarkerSize(0.8);
    ch_300pF_eff_tot.SetMarkerSize(0.8);
    ch_300pF_eff     .SetMarkerSize(0.8);
    ch_470pF_eff_tot.SetMarkerSize(0.8);
    ch_470pF_eff     .SetMarkerSize(0.8);

    hist_eff->Draw();

    //ch_100pF_hit_rate_over_eff .Draw("p");
    ch_100pF_hit_rate          .Draw("p");
    //ch_150pF_hit_rate_over_eff .Draw("p");
    ch_150pF_hit_rate          .Draw("p");
    //ch_200pF_hit_rate_over_eff .Draw("p");
    ch_200pF_hit_rate          .Draw("p");
    //ch_300pF_hit_rate_over_eff .Draw("p");
    ch_300pF_hit_rate          .Draw("p");
    //ch_470pF_hit_rate_over_eff .Draw("p");
    ch_470pF_hit_rate          .Draw("p");

    leg_top2->Clear();
    leg_top2->AddEntry( &ch_100pF_hit_rate, "100 pF Ch Hit Rate", "p");
    leg_top2->AddEntry( &ch_150pF_hit_rate, "150 pF Ch Hit Rate", "p");
    leg_top2->AddEntry( &ch_200pF_hit_rate, "200 pF Ch Hit Rate", "p");
    leg_top2->AddEntry( &ch_300pF_hit_rate, "300 pF Ch Hit Rate", "p");
    leg_top2->AddEntry( &ch_470pF_hit_rate, "470 pF Ch Hit Rate", "p");
    leg_top2->Draw();
    
    c_ch->Update();
    ps3->NewPage();

    //-----------------------------------------------------------------//

    hist_eff->Draw();

    ch_100pF_hit_rate_over_eff .Draw("p");
    ch_100pF_hit_rate          .Draw("p");
    ch_150pF_hit_rate_over_eff .Draw("p");
    ch_150pF_hit_rate          .Draw("p");
    ch_200pF_hit_rate_over_eff .Draw("p");
    ch_200pF_hit_rate          .Draw("p");
    //ch_300pF_hit_rate_over_eff .Draw("p");
    //ch_300pF_hit_rate          .Draw("p");
    //ch_470pF_hit_rate_over_eff .Draw("p");
    //ch_470pF_hit_rate          .Draw("p");

    leg_top2->Clear();
    leg_top2->AddEntry( &ch_100pF_hit_rate,          "100 pF Ch Hit Rate",     "p");
    leg_top2->AddEntry( &ch_100pF_hit_rate_over_eff, "100 pF Ch Hit Rate/Eff", "p");
    leg_top2->AddEntry( &ch_150pF_hit_rate,          "150 pF Ch Hit Rate",     "p");
    leg_top2->AddEntry( &ch_150pF_hit_rate_over_eff, "150 pF Ch Hit Rate/Eff", "p");
    leg_top2->AddEntry( &ch_200pF_hit_rate,          "200 pF Ch Hit Rate",     "p");
    leg_top2->AddEntry( &ch_200pF_hit_rate_over_eff, "200 pF Ch Hit Rate/Eff", "p");
    //leg_top2->AddEntry( &ch_300pF_hit_rate, "300 pF Ch Hit Rate", "p");
    //leg_top2->AddEntry( &ch_470pF_hit_rate, "470 pF Ch Hit Rate", "p");
    leg_top2->Draw();

    c_ch->Update();
    ps3->NewPage();

    hist_eff->Draw();

    ch_100pF_hit_rate_over_eff .Draw("p");
    ch_100pF_hit_rate          .Draw("p");
    //ch_150pF_hit_rate_over_eff .Draw("p");
    //ch_150pF_hit_rate          .Draw("p");
    //ch_200pF_hit_rate_over_eff .Draw("p");
    //ch_200pF_hit_rate          .Draw("p");
    ch_300pF_hit_rate_over_eff .Draw("p");
    ch_300pF_hit_rate          .Draw("p");
    ch_470pF_hit_rate_over_eff .Draw("p");
    ch_470pF_hit_rate          .Draw("p");

    leg_top2->Clear();
    leg_top2->AddEntry( &ch_100pF_hit_rate,          "100 pF Ch Hit Rate",     "p");
    leg_top2->AddEntry( &ch_100pF_hit_rate_over_eff, "100 pF Ch Hit Rate/Eff", "p");
    //leg_top2->AddEntry( &ch_150pF_hit_rate, "150 pF Channels", "p");
    //leg_top2->AddEntry( &ch_200pF_hit_rate, "200 pF Channels", "p");
    leg_top2->AddEntry( &ch_300pF_hit_rate,          "300 pF Ch Hit Rate",     "p");
    leg_top2->AddEntry( &ch_300pF_hit_rate_over_eff, "300 pF Ch Hit Rate/Eff", "p");
    leg_top2->AddEntry( &ch_470pF_hit_rate,          "470 pF Ch Hit Rate",     "p");
    leg_top2->AddEntry( &ch_470pF_hit_rate_over_eff, "470 pF Ch Hit Rate/Eff", "p");
    leg_top2->Draw();

    c_ch->Update();
    ps3->NewPage();

    //--------------------------------------------------------//

    m_sx.str("");
    if ( pads )                                 m_sx << "Pad Efficiency KEK Hit Rate";
    else                                        m_sx << "Strip Efficiency vs KEK Hit Rate";
    if ( data_type == type_10bit )              m_sx << " with 10 Bit ADC mode ";
    else if ( pads  && data_type == type_6bit ) m_sx << " with ToT mode ";
    else if ( !pads && data_type == type_6bit ) m_sx << " with 6 Bit ADC mode ";
    if ( pads )                                 m_sx << "For Different Pi-Network Channels at HV=";
    else                                        m_sx << "For Different Pull-Up Resistor Channels at HV=";
    m_sx << voltage << " volts";

    hist_eff->SetTitle(m_sx.str().c_str());
    hist_eff->SetXTitle("KEK Hit Rate (kHz)");
    hist_eff->SetYTitle("Test Pulse Efficiency");
    hist_eff->SetBins(1, 0, total_max_rate*1.1 );
    hist_eff->SetMaximum(   1.2 );
    hist_eff->SetMinimum(0.4);

    hist_eff->Draw();

    //ch_100pF_eff_tot.Draw("p");                                    
    ch_100pF_eff     .Draw("p");
    //ch_150pF_eff_tot.Draw("p");                                    
    ch_150pF_eff     .Draw("p");
    //ch_200pF_eff_tot.Draw("p");                                    
    ch_200pF_eff     .Draw("p");
    //ch_300pF_eff_tot.Draw("p");                                    
    ch_300pF_eff     .Draw("p");
    //ch_470pF_eff_tot.Draw("p");                                    
    ch_470pF_eff     .Draw("p");

    leg_top->Clear();
    leg_top->AddEntry( &ch_100pF_eff, "100 pF Ch Efficiency", "p");
    leg_top->AddEntry( &ch_150pF_eff, "150 pF Ch Efficiency", "p");
    leg_top->AddEntry( &ch_200pF_eff, "200 pF Ch Efficiency", "p");
    leg_top->AddEntry( &ch_300pF_eff, "300 pF Ch Efficiency", "p");
    leg_top->AddEntry( &ch_470pF_eff, "470 pF Ch Efficiency", "p");
    leg_top->Draw();

    c_ch->Update();
    ps3->NewPage();

    //-----------------------------------------------------------------//

    m_sx.str("");
    if ( pads )                                 m_sx << "Pad Efficiency vs Original Hit Rate";
    else                                        m_sx << "Strip Efficiency vs Original Hit Rate";
    if ( data_type == type_10bit )              m_sx << " with 10 Bit ADC mode ";
    else if ( pads  && data_type == type_6bit ) m_sx << " with ToT mode ";
    else if ( !pads && data_type == type_6bit ) m_sx << " with 6 Bit ADC mode ";
    if ( pads )                                 m_sx << "For Different Pi-Network Channels at HV=";
    else                                        m_sx << "For Different Pull-Up Resistor Channels at HV=";
    m_sx << voltage << " volts";

    hist_eff->SetTitle(m_sx.str().c_str());
    hist_eff->SetXTitle("Original Hit Rate (kHz)");
    hist_eff->SetYTitle("Test Pulse Efficiency");
    hist_eff->SetBins(1, 0, total_max_rate*1.1 );
    hist_eff->SetMaximum(   1.2 );
    hist_eff->SetMinimum(0.4);

    hist_eff->Draw();

    ch_100pF_eff_tot.Draw("p");                                          
    //ch_100pF_eff     .Draw("p");
    ch_150pF_eff_tot.Draw("p");                                          
    //ch_150pF_eff     .Draw("p");
    ch_200pF_eff_tot.Draw("p");                                          
    //ch_200pF_eff     .Draw("p");
    ch_300pF_eff_tot.Draw("p");                                          
    //ch_300pF_eff     .Draw("p");
    ch_470pF_eff_tot.Draw("p");                                          
    //ch_470pF_eff     .Draw("p");

    pad_vmm_ch_vec->at(39)->f_expo_eff_tot->Draw("same");
    pad_vmm_ch_vec->at(40)->f_expo_eff_tot->Draw("same");
    pad_vmm_ch_vec->at(41)->f_expo_eff_tot->Draw("same");
    pad_vmm_ch_vec->at(44)->f_expo_eff_tot->Draw("same");
    pad_vmm_ch_vec->at(45)->f_expo_eff_tot->Draw("same");
    
    leg_top->Clear();
    leg_top->AddEntry( &ch_100pF_eff_tot, "100 pF Ch Efficiency", "pl");
    leg_top->AddEntry( &ch_150pF_eff_tot, "150 pF Ch Efficiency", "pl");
    leg_top->AddEntry( &ch_200pF_eff_tot, "200 pF Ch Efficiency", "pl");
    leg_top->AddEntry( &ch_300pF_eff_tot, "300 pF Ch Efficiency", "pl");
    leg_top->AddEntry( &ch_470pF_eff_tot, "470 pF Ch Efficiency", "pl");
    leg_top->Draw();

    c_ch->Update();
    ps3->NewPage();

    //---------------------------------------------------------------------//


  }
  else {
    std::vector<chan_plots*> * strip_vmm_ch_vec;
    
    //For QL1
    if ( detector_type == 1 ) {
      strip_vmm_ch_vec = GetChannelPlots( 3, pads, 4, voltage, data_type );
    }
    //For QS2
    if ( detector_type == 2 ) {
      strip_vmm_ch_vec = GetChannelPlots( 4, pads, 4, voltage, data_type );
    }
	
    TGraph ch_200Ohm_hit_rate_over_eff  = strip_vmm_ch_vec->at(57)->hits_over_eff_vs_KEK_rate_tot;
    TGraph ch_200Ohm_hit_rate           = strip_vmm_ch_vec->at(57)->hits_vs_KEK_rate_tot;
    TGraph ch_300Ohm_hit_rate_over_eff  = strip_vmm_ch_vec->at(20)->hits_over_eff_vs_KEK_rate_tot;
    TGraph ch_300Ohm_hit_rate           = strip_vmm_ch_vec->at(20)->hits_vs_KEK_rate_tot;
    TGraph ch_500Ohm_hit_rate_over_eff  = strip_vmm_ch_vec->at(45)->hits_over_eff_vs_KEK_rate_tot;
    TGraph ch_500Ohm_hit_rate           = strip_vmm_ch_vec->at(45)->hits_vs_KEK_rate_tot;
    TGraph ch_1000Ohm_hit_rate_over_eff = strip_vmm_ch_vec->at(32)->hits_over_eff_vs_KEK_rate_tot;
    TGraph ch_1000Ohm_hit_rate          = strip_vmm_ch_vec->at(32)->hits_vs_KEK_rate_tot;


    ch_200Ohm_hit_rate_over_eff .SetMarkerColor(kTeal);
    ch_200Ohm_hit_rate          .SetMarkerColor(kTeal);
    ch_300Ohm_hit_rate_over_eff .SetMarkerColor(kGreen+2);
    ch_300Ohm_hit_rate          .SetMarkerColor(kGreen+2);
    ch_500Ohm_hit_rate_over_eff .SetMarkerColor(kBlue);
    ch_500Ohm_hit_rate          .SetMarkerColor(kBlue);
    ch_1000Ohm_hit_rate_over_eff.SetMarkerColor(kRed);
    ch_1000Ohm_hit_rate         .SetMarkerColor(kRed);

    ch_200Ohm_hit_rate_over_eff .SetMarkerSize(0.8);
    ch_200Ohm_hit_rate          .SetMarkerSize(0.8);
    ch_300Ohm_hit_rate_over_eff .SetMarkerSize(0.8);
    ch_300Ohm_hit_rate          .SetMarkerSize(0.8);
    ch_500Ohm_hit_rate_over_eff .SetMarkerSize(0.8);
    ch_500Ohm_hit_rate          .SetMarkerSize(0.8);
    ch_1000Ohm_hit_rate_over_eff.SetMarkerSize(0.8);
    ch_1000Ohm_hit_rate         .SetMarkerSize(0.8);

    TGraph ch_200Ohm_eff_tot = strip_vmm_ch_vec->at(57)->eff_vs_photon_rate_tot;
    TGraph ch_200Ohm_eff     = strip_vmm_ch_vec->at(57)->eff_vs_KEK_rate_tot;
    TGraph ch_300Ohm_eff_tot = strip_vmm_ch_vec->at(20)->eff_vs_photon_rate_tot;
    TGraph ch_300Ohm_eff     = strip_vmm_ch_vec->at(20)->eff_vs_KEK_rate_tot;
    TGraph ch_500Ohm_eff_tot = strip_vmm_ch_vec->at(45)->eff_vs_photon_rate_tot;
    TGraph ch_500Ohm_eff     = strip_vmm_ch_vec->at(45)->eff_vs_KEK_rate_tot;
    TGraph ch_1000Ohm_eff_tot= strip_vmm_ch_vec->at(32)->eff_vs_photon_rate_tot;
    TGraph ch_1000Ohm_eff    = strip_vmm_ch_vec->at(32)->eff_vs_KEK_rate_tot;

    ch_200Ohm_eff_tot.SetMarkerColor(kTeal);
    ch_200Ohm_eff    .SetMarkerColor(kTeal);
    ch_300Ohm_eff_tot.SetMarkerColor(kGreen+2);
    ch_300Ohm_eff    .SetMarkerColor(kGreen+2);
    ch_500Ohm_eff_tot.SetMarkerColor(kBlue);
    ch_500Ohm_eff    .SetMarkerColor(kBlue);
    ch_1000Ohm_eff_tot.SetMarkerColor(kRed);
    ch_1000Ohm_eff    .SetMarkerColor(kRed);

    ch_200Ohm_eff_tot.SetLineColor(kTeal);
    ch_200Ohm_eff    .SetLineColor(kTeal);
    ch_300Ohm_eff_tot.SetLineColor(kGreen+2);
    ch_300Ohm_eff    .SetLineColor(kGreen+2);
    ch_500Ohm_eff_tot.SetLineColor(kBlue);
    ch_500Ohm_eff    .SetLineColor(kBlue);
    ch_1000Ohm_eff_tot.SetLineColor(kRed);
    ch_1000Ohm_eff    .SetLineColor(kRed);

    ch_200Ohm_eff_tot.SetMarkerSize(0.8);
    ch_200Ohm_eff    .SetMarkerSize(0.8);
    ch_300Ohm_eff_tot.SetMarkerSize(0.8);
    ch_300Ohm_eff    .SetMarkerSize(0.8);
    ch_500Ohm_eff_tot.SetMarkerSize(0.8);
    ch_500Ohm_eff    .SetMarkerSize(0.8);
    ch_1000Ohm_eff_tot.SetMarkerSize(0.8);
    ch_1000Ohm_eff    .SetMarkerSize(0.8);

    hist_eff->Draw();

    //ch_200Ohm_hit_rate_over_eff .Draw("p");
    ch_200Ohm_hit_rate          .Draw("p");
    //ch_300Ohm_hit_rate_over_eff .Draw("p");
    ch_300Ohm_hit_rate          .Draw("p");
    //ch_500Ohm_hit_rate_over_eff .Draw("p");
    ch_500Ohm_hit_rate          .Draw("p");
    //ch_1000Ohm_hit_rate_over_eff .Draw("p");
    ch_1000Ohm_hit_rate          .Draw("p");

    leg_top2->Clear();
    leg_top2->AddEntry( &ch_200Ohm_hit_rate, "200 k#Omega Channels", "p");
    leg_top2->AddEntry( &ch_300Ohm_hit_rate, "300 k#Omega Channels", "p");
    leg_top2->AddEntry( &ch_500Ohm_hit_rate, "500 k#Omega Channels", "p");
    leg_top2->AddEntry( &ch_1000Ohm_hit_rate,"  1 M#Omega Channels", "p");
    leg_top2->Draw();

    c_ch->Update();
    ps3->NewPage();

    //-----------------------------------------------//

    hist_eff->Draw();

    ch_200Ohm_hit_rate_over_eff .Draw("p");
    ch_200Ohm_hit_rate          .Draw("p");
    //ch_300Ohm_hit_rate_over_eff .Draw("p");
    //ch_300Ohm_hit_rate          .Draw("p");
    ch_500Ohm_hit_rate_over_eff .Draw("p");
    ch_500Ohm_hit_rate          .Draw("p");
    ch_1000Ohm_hit_rate_over_eff .Draw("p");
    ch_1000Ohm_hit_rate          .Draw("p");

    leg_top2->Clear();
    leg_top2->AddEntry( &ch_200Ohm_hit_rate, "200 k#Omega Channels", "p");
    //leg_top2->AddEntry( &ch_300Ohm_hit_rate, "300 k#Omega Channels", "p");
    leg_top2->AddEntry( &ch_500Ohm_hit_rate, "500 k#Omega Channels", "p");
    leg_top2->AddEntry( &ch_1000Ohm_hit_rate,"  1 M#Omega Channels", "p");
    leg_top2->Draw();

    c_ch->Update();
    ps3->NewPage();

    hist_eff->Draw();

    ch_200Ohm_hit_rate_over_eff .Draw("p");
    ch_200Ohm_hit_rate          .Draw("p");
    ch_300Ohm_hit_rate_over_eff .Draw("p");
    ch_300Ohm_hit_rate          .Draw("p");
    //ch_500Ohm_hit_rate_over_eff .Draw("p");
    //ch_500Ohm_hit_rate          .Draw("p");
    //ch_1000Ohm_hit_rate_over_eff .Draw("p");
    //ch_1000Ohm_hit_rate          .Draw("p");

    leg_top2->Clear();
    leg_top2->AddEntry( &ch_200Ohm_hit_rate, "200 k#Omega Channels", "p");
    leg_top2->AddEntry( &ch_300Ohm_hit_rate, "300 k#Omega Channels", "p");
    //leg_top2->AddEntry( &ch_500Ohm_hit_rate, "500 k#Omega Channels", "p");
    //leg_top2->AddEntry( &ch_1000Ohm_hit_rate,"1   M#Omega Channels", "p");
    leg_top2->Draw();

    c_ch->Update();
    ps3->NewPage();

    //---------------------------------------------------//

    m_sx.str("");
    if ( pads )                                 m_sx << "Pad Efficiency vs KEK Hit Rate";
    else                                        m_sx << "Strip Efficiency vs KEK Hit Rate";
    if ( data_type == type_10bit )              m_sx << " with 10 Bit ADC mode ";
    else if ( pads  && data_type == type_6bit ) m_sx << " with ToT mode ";
    else if ( !pads && data_type == type_6bit ) m_sx << " with 6 Bit ADC mode ";
    if ( pads )                                 m_sx << "For Different Pi-Network Channels at HV=";
    else                                        m_sx << "For Different Pull-Up Resistor Channels at HV=";
    m_sx << voltage << " volts";

    hist_eff->SetTitle(m_sx.str().c_str());
    hist_eff->SetXTitle("KEK Hit Rate (kHz)");
    hist_eff->SetYTitle("Test Pulse Efficiency");
    hist_eff->SetBins(1, 0, total_max_rate*1.1 );
    hist_eff->SetMaximum(   1.2 );
    hist_eff->SetMinimum(0.6);

    hist_eff->Draw();

    //ch_200Ohm_eff_tot.Draw("p");
    ch_200Ohm_eff     .Draw("p");                                                
    //ch_300Ohm_eff_tot.Draw("p");
    ch_300Ohm_eff     .Draw("p");                                                
    //ch_500Ohm_eff_tot.Draw("p");
    ch_500Ohm_eff     .Draw("p");                                                
    //ch_1000Ohm_eff_tot.Draw("p");
    ch_1000Ohm_eff     .Draw("p");                                               

    leg_top->Clear();
    leg_top->AddEntry( &ch_200Ohm_eff,  "200 k#Omega Ch Efficiency", "p");
    leg_top->AddEntry( &ch_300Ohm_eff,  "300 k#Omega Ch Efficiency", "p");
    leg_top->AddEntry( &ch_500Ohm_eff,  "500 k#Omega Ch Efficiency", "p");
    leg_top->AddEntry( &ch_1000Ohm_eff, "  1 M#Omega Ch Efficiency", "p");
    leg_top->Draw();

    c_ch->Update();
    ps3->NewPage();

    m_sx.str("");
    if ( pads )                                 m_sx << "Pad Efficiency vs Original Hit Rate";
    else                                        m_sx << "Strip Efficiency vs Original Hit Rate";
    if ( data_type == type_10bit )              m_sx << " with 10 Bit ADC mode ";
    else if ( pads  && data_type == type_6bit ) m_sx << " with ToT mode ";
    else if ( !pads && data_type == type_6bit ) m_sx << " with 6 Bit ADC mode ";
    if ( pads )                                 m_sx << "For Different Pi-Network Channels at HV=";
    else                                        m_sx << "For Different Pull-Up Resistor Channels at HV=";
    m_sx << voltage << " volts";

    hist_eff->SetTitle(m_sx.str().c_str());
    hist_eff->SetXTitle("Original Hit Rate (kHz)");
    hist_eff->SetYTitle("Test Pulse Efficiency");
    hist_eff->SetBins(1, 0, total_max_rate*1.1 );
    hist_eff->SetMaximum(   1.2 );
    hist_eff->SetMinimum(0.6);

    hist_eff->Draw();

    ch_200Ohm_eff_tot.Draw("p");
    //ch_200Ohm_eff     .Draw("p");                                           
    ch_300Ohm_eff_tot.Draw("p");
    //ch_300Ohm_eff     .Draw("p");                                           
    ch_500Ohm_eff_tot.Draw("p");
    //ch_500Ohm_eff     .Draw("p");                                           
    ch_1000Ohm_eff_tot.Draw("p");
    //ch_1000Ohm_eff     .Draw("p");                                           

    strip_vmm_ch_vec->at(57)->f_expo_eff_tot->Draw("same");
    strip_vmm_ch_vec->at(20)->f_expo_eff_tot->Draw("same");
    strip_vmm_ch_vec->at(45)->f_expo_eff_tot->Draw("same");
    strip_vmm_ch_vec->at(32)->f_expo_eff_tot->Draw("same");

    leg_top->Clear();
    leg_top->AddEntry( &ch_200Ohm_eff_tot,  "200 k#Omega Ch Efficiency", "lp");
    leg_top->AddEntry( &ch_300Ohm_eff_tot,  "300 k#Omega Ch Efficiency", "lp");
    leg_top->AddEntry( &ch_500Ohm_eff_tot,  "500 k#Omega Ch Efficiency", "lp");
    leg_top->AddEntry( &ch_1000Ohm_eff_tot, "  1 M#Omega Ch Efficiency", "lp");
    leg_top->Draw();

    c_ch->Update();
    ps3->NewPage();


  }

  ps3->Close();

  if ( detector_type == 1 ) {
    gSystem->Exec(("ps2pdf plotsQL1/"+ps_name+".eps").Data());
    gSystem->Exec("mv "+ps_name+".pdf plotsQL1/"+ps_name+".pdf");
  }

  if ( detector_type == 2 ) {
    gSystem->Exec(("ps2pdf plotsQS2/"+ps_name+".eps").Data());
    gSystem->Exec("mv "+ps_name+".pdf plotsQS2/"+ps_name+".pdf");
  }
  
  gPad->SetGridx(0);
  gPad->SetGridy(0);


}

void decoded_event_tree::plotSummaryL3(bool pads, int voltage, int data_type) {

  gStyle->SetOptStat(0);

  m_sx.str("");
  m_sx << "L3_plot_summary_" ;
  if ( pads ) m_sx << "_pads";
  else        m_sx << "_strips";
  if ( data_type == type_10bit ) m_sx << "_10bit";
  else if ( data_type == type_6bit ) m_sx << "_6bit";
  m_sx << "_" << voltage;

  TString ps_name = m_sx.str();

  TPostScript * ps3 = new TPostScript(("plots/"+ps_name+".eps").Data(),112);

  TCanvas * c_ch = new TCanvas(("c_ch"+ps_name).Data(), "c_ch");

  TH1F* hist_100pF_deadTime;
  TH1F* hist_100pF_deadTime_small;
  TH1F* hist_100pF_deadTime_large;

  TH1F* hist_200Ohm_deadTime;

  if ( pads ) {

  m_sx.str("");
  m_sx << "Pad ";
  if ( data_type == type_10bit ) m_sx << "10 bit ADC ";
  else if ( data_type == type_6bit ) m_sx << "ToT ";
  m_sx << "Fitted Dead Time with Different Pi-Networks at HV=";
  m_sx << voltage << " volts";

  hist_100pF_deadTime = new TH1F(("hist_100pF_deadtime"+ps_name).Data(), m_sx.str().c_str(), 50, 200, 400);
  hist_100pF_deadTime->SetXTitle("Channel Fitted Dead Time (ns)");
  hist_100pF_deadTime->SetYTitle("N Channels");
  hist_100pF_deadTime->SetLineColor(kBlack);
  hist_100pF_deadTime->SetFillColor(kTeal);
  hist_100pF_deadTime->SetFillStyle(3003);

  m_sx.str("");
  //m_sx << "Pad with Size <= 55 cm^{2} ";
  //For QS2
  m_sx << "Pad with Size <= 250 cm^{2} ";
  if ( data_type == type_10bit ) m_sx << "10 Bit ADC ";
  else if ( data_type == type_6bit ) m_sx << "ToT ";
  m_sx << "Fitted Dead Time with Different Pi-Networks at HV=";
  m_sx << voltage << " volts";

  hist_100pF_deadTime_small = new TH1F(("hist_100pF_deadtime_small_"+ps_name).Data(), m_sx.str().c_str(), 50, 200, 400);
  hist_100pF_deadTime_small->SetXTitle("Channel Fitted Dead Time (ns)");
  hist_100pF_deadTime_small->SetYTitle("N Channels");
  hist_100pF_deadTime_small->SetLineColor(kBlack);
  hist_100pF_deadTime_small->SetFillColor(kTeal);
  hist_100pF_deadTime_small->SetFillStyle(3003);

  m_sx.str("");
  //m_sx << "Pad with Size > 55 cm^{2} ";
  //For QS2
  m_sx << "Pad with Size > 250 cm^{2} ";
  if ( data_type == type_10bit ) m_sx << "10 Bit ADC ";
  else if ( data_type == type_6bit ) m_sx << "ToT ";
  m_sx << "Fitted Dead Time with Different Pi-Networks at HV=";
  m_sx << voltage << " volts";

  hist_100pF_deadTime_large = new TH1F(("hist_100pF_deadtime_small_"+ps_name).Data(), m_sx.str().c_str(), 50, 200, 400);
  hist_100pF_deadTime_large->SetXTitle("Channel Fitted Dead Time (ns)");
  hist_100pF_deadTime_large->SetYTitle("N Channels");
  hist_100pF_deadTime_large->SetLineColor(kBlack);
  hist_100pF_deadTime_large->SetFillColor(kTeal);
  hist_100pF_deadTime_large->SetFillStyle(3003);

  }
  else {

  m_sx.str("");
  m_sx << "Strip ";
  if ( data_type == type_10bit ) m_sx << "10 Bit ADC ";
  else if ( data_type == type_6bit ) m_sx << "6 Bit ADC ";
  m_sx << "Fitted Dead Time with Different Pull-Up Resistors at HV=";
  m_sx << voltage << " volts";

  hist_200Ohm_deadTime = new TH1F(("hist_200Ohm_deadtime"+ps_name).Data(), m_sx.str().c_str(), 50, 200, 400);
  hist_200Ohm_deadTime->SetXTitle("Channel Fitted Dead Time (ns)");
  hist_200Ohm_deadTime->SetYTitle("N Channels");
  hist_200Ohm_deadTime->SetLineColor(kBlack);
  hist_200Ohm_deadTime->SetFillColor(kTeal);
  hist_200Ohm_deadTime->SetFillStyle(3003);

  }

  std::vector<int> layer_vec_tmp  = layer_vec;
  std::vector<int> elink_vec_tmp  = elink_vec;
  std::vector<int> vmm_vec_tmp    = vmm_vec;
  std::vector<int> SCA_ID_vec_tmp = SCA_ID_vec;
  std::vector<int> HV_vec_tmp     = HV_vec;

  std::vector<float> deadtime_150pF;
  std::vector<float> deadtime_200pF;
  std::vector<float> deadtime_300pF;
  std::vector<float> deadtime_470pF;

  std::vector<float> deadtime_300Ohm;
  std::vector<float> deadtime_500Ohm;
  std::vector<float> deadtime_1000Ohm;

  std::vector<float> deadtime_150pF_small;
  std::vector<float> deadtime_200pF_small;
  std::vector<float> deadtime_300pF_small;
  std::vector<float> deadtime_470pF_small;

  std::vector<float> deadtime_150pF_large;
  std::vector<float> deadtime_200pF_large;
  std::vector<float> deadtime_300pF_large;
  std::vector<float> deadtime_470pF_large;

  TArrow * a_150pF = new TArrow();
  TArrow * a_200pF = new TArrow();
  TArrow * a_300pF = new TArrow();
  TArrow * a_470pF = new TArrow();

  TArrow * a_300Ohm  = new TArrow();
  TArrow * a_500Ohm  = new TArrow();
  TArrow * a_1000Ohm = new TArrow();

  a_150pF->SetLineColor(kGreen+2);
  a_200pF->SetLineColor(kBlue);
  a_300pF->SetLineColor(kOrange+2);
  a_470pF->SetLineColor(kRed);

  a_300Ohm ->SetLineColor(kGreen+2);
  a_500Ohm ->SetLineColor(kBlue);
  a_1000Ohm->SetLineColor(kRed);

  float total_max_rate = 0;

  for ( uint ith=0; ith<vmm_vec_tmp.size(); ith++ ) {
    //std::cout << "voltage vector size: " << HV_vec_tmp.size() << std::endl;
    for ( uint ivoltage = 0; ivoltage<HV_vec_tmp.size(); ivoltage++ ) {

      int ilayer  = layer_vec_tmp.at(ith);
      int ielink  = elink_vec_tmp.at(ith);
      int ivmm    = vmm_vec_tmp.at(ith);
      int iSCA_ID = SCA_ID_vec_tmp.at(ith);
      
      int ivolt   = HV_vec_tmp.at(ivoltage);
      
      if ( ivolt != voltage ) continue;
      if ( pads &&  ( iSCA_ID != 993  && iSCA_ID != 1040 ) ) continue;
      if ( !pads && ( iSCA_ID != 1001 && iSCA_ID != 1026 ) ) continue;

      std::vector<chan_plots*> * vmm_ch_plot_vec = GetChannelPlots( ilayer, pads,
								    ivmm,   ivolt,  data_type );
      
      for ( uint ich=0; ich<vmm_ch_plot_vec->size(); ich++ ) {
	
	bool isgood = vmm_ch_plot_vec->at(ich)->is_good;
	float area  = vmm_ch_plot_vec->at(ich)->area;
	int  icap   = 0;
	int  ires   = 0;
	float deadtime = ( vmm_ch_plot_vec->at(ich)->f_expo_eff_tot->GetParameter(2)*1e6 );
	float max_rate = vmm_ch_plot_vec->at(ich)->max_tot_rate;

	if (  pads ) icap = vmm_ch_plot_vec->at(ich)->capacitance;
	if ( !pads ) ires = vmm_ch_plot_vec->at(ich)->resistance;
	
	if ( isgood ) {
	  
	  if ( total_max_rate < max_rate && max_rate < 10000 ) total_max_rate = max_rate;

	  if ( pads ) {	  

	    if ( ilayer == 3 ) {
	      
	      if      ( icap == 100 ) hist_100pF_deadTime->Fill( deadtime );
	      else if ( icap == 150 ) deadtime_150pF.push_back( deadtime );
	      else if ( icap == 200 ) deadtime_200pF.push_back( deadtime );
	      else if ( icap == 300 ) deadtime_300pF.push_back( deadtime );
	      else if ( icap == 470 ) deadtime_470pF.push_back( deadtime );
	    
	      if ( area <= 250.0 ) {
	      
		if      ( icap == 100 ) hist_100pF_deadTime_small->Fill( deadtime );
		else if ( icap == 150 ) deadtime_150pF_small.push_back( deadtime );
		else if ( icap == 200 ) deadtime_200pF_small.push_back( deadtime );
		else if ( icap == 300 ) deadtime_300pF_small.push_back( deadtime );
		else if ( icap == 470 ) deadtime_470pF_small.push_back( deadtime );
	      
	      }
	      else {
	      
		if      ( icap == 100 ) hist_100pF_deadTime_large->Fill( deadtime );
		else if ( icap == 150 ) deadtime_150pF_large.push_back( deadtime );
		else if ( icap == 200 ) deadtime_200pF_large.push_back( deadtime );
		else if ( icap == 300 ) deadtime_300pF_large.push_back( deadtime );
		else if ( icap == 470 ) deadtime_470pF_large.push_back( deadtime );
	      
	      }
	    }
	    
	  }
	  else if ( !pads ) {
	    
	    if ( ilayer == 3 && ivmm == 4 ) {
	      if      ( ires == 200  ) hist_200Ohm_deadTime->Fill( vmm_ch_plot_vec->at(ich)->f_expo_eff_tot->GetParameter(2)*1e6 );
	      else if ( ires == 300  ) deadtime_300Ohm.push_back( deadtime );
	      else if ( ires == 500  ) deadtime_500Ohm.push_back( deadtime );
	      else if ( ires == 1000 ) deadtime_1000Ohm.push_back( deadtime );
	    }
	  }
	}
      } // loop over channels
      //std::cout << "I have finished channel looping" << std::endl;
    } // loop over voltages
    std::cout << "I have finished voltage looping for VMM " << ith << " For pads? " << pads << std::endl;
  } // loop over vmms
  std::cout << "I have looped over this vmm" << std::endl;

  c_ch->cd();
  
  if ( pads ) {
    hist_100pF_deadTime->Draw();

    float maximum = hist_100pF_deadTime->GetMaximum();

    for ( uint i=0; i< deadtime_150pF.size(); i++ ) {
      a_150pF->DrawArrow(deadtime_150pF.at(i), 0, 
			 deadtime_150pF.at(i), maximum*0.2, 0.01, "<");
    }
    for ( uint i=0; i< deadtime_200pF.size(); i++ ) {
      a_200pF->DrawArrow(deadtime_200pF.at(i), 0,
			 deadtime_200pF.at(i), maximum*0.2, 0.01, "<");
    }
    for ( uint i=0; i< deadtime_300pF.size(); i++ ) {
      a_300pF->DrawArrow(deadtime_300pF.at(i), 0,
			 deadtime_300pF.at(i), maximum*0.2, 0.01, "<");
    }
    for ( uint i=0; i< deadtime_470pF.size(); i++ ) {
      a_470pF->DrawArrow(deadtime_470pF.at(i), 0,
			 deadtime_470pF.at(i), maximum*0.2, 0.01, "<");
    }

    leg_top->Clear();
    leg_top->AddEntry( hist_100pF_deadTime, "100 pF Channels", "f");
    leg_top->AddEntry( a_150pF,             "150 pF Channels", "l");
    leg_top->AddEntry( a_200pF,             "200 pF Channels", "l");
    leg_top->AddEntry( a_300pF,             "300 pF Channels", "l");
    leg_top->AddEntry( a_470pF,             "470 pF Channels", "l");
    leg_top->Draw();

    c_ch->Update();
    ps3->NewPage();

    //-----------------------------//

    hist_100pF_deadTime_small->Draw();

    maximum = hist_100pF_deadTime_small->GetMaximum();

    for ( uint i=0; i< deadtime_150pF_small.size(); i++ ) {
      a_150pF->DrawArrow(deadtime_150pF_small.at(i), 0,
                         deadtime_150pF_small.at(i), maximum*0.2, 0.01, "<");
    }
    for ( uint i=0; i< deadtime_200pF_small.size(); i++ ) {
      a_200pF->DrawArrow(deadtime_200pF_small.at(i), 0,
                         deadtime_200pF_small.at(i), maximum*0.2, 0.01, "<");
    }
    for ( uint i=0; i< deadtime_300pF_small.size(); i++ ) {
      a_300pF->DrawArrow(deadtime_300pF_small.at(i), 0,
                         deadtime_300pF_small.at(i), maximum*0.2, 0.01, "<");
    }
    for ( uint i=0; i< deadtime_470pF_small.size(); i++ ) {
      a_470pF->DrawArrow(deadtime_470pF_small.at(i), 0,
                         deadtime_470pF_small.at(i), maximum*0.2, 0.01, "<");
    }

    leg_top->Clear();
    leg_top->AddEntry( hist_100pF_deadTime_small, "100 pF Channels", "f");
    leg_top->AddEntry( a_150pF,             "150 pF Channels", "l");
    leg_top->AddEntry( a_200pF,             "200 pF Channels", "l");
    leg_top->AddEntry( a_300pF,             "300 pF Channels", "l");
    leg_top->AddEntry( a_470pF,             "470 pF Channels", "l");
    leg_top->Draw();

    c_ch->Update();
    ps3->NewPage();

    //---------------------//

    hist_100pF_deadTime_large->Draw();
    
    maximum = hist_100pF_deadTime_large->GetMaximum();

    for ( uint i=0; i< deadtime_150pF_large.size(); i++ ) {
      a_150pF->DrawArrow(deadtime_150pF_large.at(i), 0,
                         deadtime_150pF_large.at(i), maximum*0.2, 0.01, "<");
    }
    for ( uint i=0; i< deadtime_200pF_large.size(); i++ ) {
      a_200pF->DrawArrow(deadtime_200pF_large.at(i), 0,
                         deadtime_200pF_large.at(i), maximum*0.2, 0.01, "<");
    }
    for ( uint i=0; i< deadtime_300pF_large.size(); i++ ) {
      a_300pF->DrawArrow(deadtime_300pF_large.at(i), 0,
                         deadtime_300pF_large.at(i), maximum*0.2, 0.01, "<");
    }
    for ( uint i=0; i< deadtime_470pF_large.size(); i++ ) {
      a_470pF->DrawArrow(deadtime_470pF_large.at(i), 0,
                         deadtime_470pF_large.at(i), maximum*0.2, 0.01, "<");
    }

    leg_top->Clear();
    leg_top->AddEntry( hist_100pF_deadTime_large, "100 pF Channels", "f");
    leg_top->AddEntry( a_150pF,             "150 pF Channels", "l");
    leg_top->AddEntry( a_200pF,             "200 pF Channels", "l");
    leg_top->AddEntry( a_300pF,             "300 pF Channels", "l");
    leg_top->AddEntry( a_470pF,             "470 pF Channels", "l");
    leg_top->Draw();

    c_ch->Update();
    ps3->NewPage();

  }
  else {
    hist_200Ohm_deadTime->Draw();

    float maximum = hist_200Ohm_deadTime->GetMaximum();

    for ( uint i=0; i< deadtime_300Ohm.size(); i++ ) {
      a_300Ohm->DrawArrow(deadtime_300Ohm.at(i), 0,
			   deadtime_300Ohm.at(i), maximum*0.2, 0.01, "<");
    }
    for ( uint i=0; i< deadtime_500Ohm.size(); i++ ) {
      a_500Ohm->DrawArrow(deadtime_500Ohm.at(i), 0,
			   deadtime_500Ohm.at(i), maximum*0.2, 0.01, "<");
    }
    for ( uint i=0; i< deadtime_1000Ohm.size(); i++ ) {
      a_1000Ohm->DrawArrow(deadtime_1000Ohm.at(i), 0,
			    deadtime_1000Ohm.at(i), maximum*0.2, 0.01, "<");
    }

    leg_top->Clear();
    leg_top->AddEntry( hist_200Ohm_deadTime, "200 k#Omega Channels",  "f");
    leg_top->AddEntry( a_300Ohm,             "300 k#Omega Channels",  "l");
    leg_top->AddEntry( a_500Ohm,             "500 k#Omega Channels",  "l");
    leg_top->AddEntry( a_1000Ohm,            "  1 M#Omega Channels", "l");
    leg_top->Draw();

    c_ch->Update();
    ps3->NewPage();
  }
  
  //----------------------------------------------------------------------//
  //                    Plot all Efficiency Curves
  //----------------------------------------------------------------------//

  gPad->SetGridx(1);
  gPad->SetGridy(1);

  TH1F* hist_eff = new TH1F(("hist_eff_"+ps_name).Data(), "", 1, 0, total_max_rate);

  m_sx.str("");
  if ( pads )                                 m_sx << "Efficiency vs Hit Rate for Pads";
  else                                        m_sx << "Efficiency vs Hit Rate for Strips";
  if ( data_type == type_10bit )              m_sx << " with 10 Bit ADC mode ";
  else if ( pads  && data_type == type_6bit ) m_sx << " with ToT mode ";
  else if ( !pads && data_type == type_6bit ) m_sx << " with 6 Bit ADC mode ";
  if ( pads )                                 m_sx << "For Different Pi-Networks at HV=";
  else                                        m_sx << "For Different Pull-Up Resistors at HV=";
  m_sx << voltage << " volts";

  hist_eff->SetTitle(m_sx.str().c_str());
  hist_eff->SetXTitle("Channel Hit Rate (kHz)");
  hist_eff->SetYTitle("Efficiency");
  hist_eff->SetMaximum(1.05);
  hist_eff->SetMinimum(0.40);

  hist_eff->Draw();

  for ( uint itry = 0; itry<2; itry ++ ) {
    
    for ( uint ith=0; ith<vmm_vec_tmp.size(); ith++ ) {
      
      for ( uint ivoltage = 0; ivoltage<HV_vec_tmp.size(); ivoltage++ ) {
	
	int ilayer  = layer_vec_tmp.at(ith);
	int ielink  = elink_vec_tmp.at(ith);
	int ivmm    = vmm_vec_tmp.at(ith);
	int iSCA_ID = SCA_ID_vec_tmp.at(ith);
	
	int ivolt   = HV_vec_tmp.at(ivoltage);
	
	if ( ivolt != voltage ) continue;
	if ( pads &&  ( iSCA_ID != 993  && iSCA_ID != 1040 ) ) continue;
	if ( !pads && ( iSCA_ID != 1001 && iSCA_ID != 1026 ) ) continue;
	
	std::vector<chan_plots*> * vmm_ch_plot_vec = GetChannelPlots( ilayer, pads,
								      ivmm,   ivolt,  data_type );
	if ( ilayer == 3 ) {
	  for ( uint ich=0; ich<vmm_ch_plot_vec->size(); ich++ ) {
	  
	    bool isgood = vmm_ch_plot_vec->at(ich)->is_good;
	    int  icap = -1;
	    int  ires = -1;
	    if (  pads ) icap = vmm_ch_plot_vec->at(ich)->capacitance;
	    if ( !pads ) ires = vmm_ch_plot_vec->at(ich)->resistance;

	    if ( isgood ) {
	      if ( itry == 0 && (  pads && icap != 100 ) ) continue;
	      if ( itry == 1 && (  pads && icap == 100 ) ) continue;
	      if ( itry == 0 && ( !pads && ires != 200 ) ) continue;
	      if ( itry == 1 && ( !pads && ires == 200 ) ) continue;
	      vmm_ch_plot_vec->at(ich)->f_expo_eff_tot->Draw("same");

	      if ( pads) {
		if ( icap == 100 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kTeal);
		if ( icap == 150 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kGreen+2);
		if ( icap == 200 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kBlue);
		if ( icap == 300 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kOrange+2);
		if ( icap == 470 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kRed);
	      }
	      else {
		if ( ires == 200 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kTeal);
		if ( ires == 300 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kGreen+2);
		if ( ires == 500 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kBlue);
		if ( ires == 1000 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kRed);
	      }

	      vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.Draw("p");
	    }
	  }
	}
	
      }
    }

  }
  leg_top->Draw();

  c_ch->Update();
  ps3->NewPage();

  hist_eff->Draw();

  for ( uint itry = 0; itry<2; itry ++ ) {

    for ( uint ith=0; ith<vmm_vec_tmp.size(); ith++ ) {

      for ( uint ivoltage = 0; ivoltage<HV_vec_tmp.size(); ivoltage++ ) {

	int ilayer  = layer_vec_tmp.at(ith);
	int ielink  = elink_vec_tmp.at(ith);
	int ivmm    = vmm_vec_tmp.at(ith);
	int iSCA_ID = SCA_ID_vec_tmp.at(ith);

	int ivolt   = HV_vec_tmp.at(ivoltage);

	if ( ivolt != voltage ) continue;
	if ( pads &&  ( iSCA_ID != 993  && iSCA_ID != 1040 ) ) continue;
	if ( !pads && ( iSCA_ID != 1001 && iSCA_ID != 1026 ) ) continue;

	std::vector<chan_plots*> * vmm_ch_plot_vec = GetChannelPlots( ilayer, pads,
								      ivmm,   ivolt,  data_type );
	if ( ilayer == 3 ) {
	  
	  for ( uint ich=0; ich<vmm_ch_plot_vec->size(); ich++ ) {

	    bool isgood = vmm_ch_plot_vec->at(ich)->is_good;
	    int  icap = -1;
	    int  ires = -1;
	    if (  pads ) icap = vmm_ch_plot_vec->at(ich)->capacitance;
	    if ( !pads ) ires = vmm_ch_plot_vec->at(ich)->resistance;

	    if ( isgood ) {
	      if ( itry == 0 && (  pads && icap != 100 ) ) continue;
	      if ( itry == 1 && (  pads && icap == 100 ) ) continue;
	      if ( itry == 0 && ( !pads && ires != 200 ) ) continue;
	      if ( itry == 1 && ( !pads && ires == 200 ) ) continue;
	      //if ( itry == 1 ) continue;
	      //vmm_ch_plot_vec->at(ich)->f_1overX_eff_tot->Draw("same");

	      if ( pads) {
		if ( icap == 100 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kTeal);
		if ( icap == 150 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kGreen+2);
		if ( icap == 200 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kBlue);
		if ( icap == 300 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kOrange+2);
		if ( icap == 470 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kRed);
	      }
	      else {
		if ( ires == 200 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kTeal);
		if ( ires == 300 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kGreen+2);
		if ( ires == 500 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kBlue);
		if ( ires == 1000 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kRed);
	      }


	      vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.Draw("p");

	    }
	  }
	}

      }
    }

  }
  leg_top->Draw();

  c_ch->Update();
  ps3->NewPage();
  
  //----------------------------------------------------------------------//

  if ( pads ) {

    m_sx.str("");
    //m_sx << "Efficiency vs Hit Rate for Pads with Size > 55 cm^{2} ";
    //For QS2
    m_sx << "Efficiency vs Hit Rate for Pads with Size > 250 cm^{2} ";
    if ( data_type == type_10bit ) m_sx << " with 10 Bit ADC mode ";
    else if ( data_type == type_6bit ) m_sx << " with ToT mode ";
    m_sx << "For Different Pi-Networks at HV=";
    m_sx << voltage << " volts";

    hist_eff->SetTitle(m_sx.str().c_str());
    hist_eff->SetXTitle("Channel Hit Rate (kHz)");
    hist_eff->SetYTitle("Efficiency");
    hist_eff->SetMaximum(1.05);
    hist_eff->SetMinimum(0.40);


    hist_eff->Draw();

    for ( uint itry = 0; itry<2; itry ++ ) {

      for ( uint ith=0; ith<vmm_vec_tmp.size(); ith++ ) {
	
	for ( uint ivoltage = 0; ivoltage<HV_vec_tmp.size(); ivoltage++ ) {

	  int ilayer  = layer_vec_tmp.at(ith);
	  int ielink  = elink_vec_tmp.at(ith);
	  int ivmm    = vmm_vec_tmp.at(ith);
	  int iSCA_ID = SCA_ID_vec_tmp.at(ith);
	  
	  int ivolt   = HV_vec_tmp.at(ivoltage);
	  
	  if ( ivolt != voltage ) continue;
	  if ( pads &&  ( iSCA_ID != 993  && iSCA_ID != 1040 ) ) continue;
	  if ( !pads && ( iSCA_ID != 1001 && iSCA_ID != 1026 ) ) continue;
	  
	  std::vector<chan_plots*> * vmm_ch_plot_vec = GetChannelPlots( ilayer, pads,
									ivmm,   ivolt,  data_type );
	  if ( ilayer == 3 ) { 
	    for ( uint ich=0; ich<vmm_ch_plot_vec->size(); ich++ ) {
	    
	      bool isgood = vmm_ch_plot_vec->at(ich)->is_good;
	      float area  = vmm_ch_plot_vec->at(ich)->area;
	      int  icap = -1;
	      int  ires = -1;
	      if (  pads ) icap = vmm_ch_plot_vec->at(ich)->capacitance;
	      if ( !pads ) ires = vmm_ch_plot_vec->at(ich)->resistance;
	    
	      //if ( isgood && area > 55.0) {
	      //For QS2
	      if ( isgood && area > 250.0) {
		if ( itry == 0 && ( pads &&icap !=100 ) ) continue;
		if ( itry == 1 && (  pads && icap == 100 ) ) continue;
		if ( itry == 0 && ( !pads && ires != 200 ) ) continue;
		if ( itry == 1 && ( !pads && ires == 200 ) ) continue;
		vmm_ch_plot_vec->at(ich)->f_expo_eff_tot->Draw("same");

		if ( pads) {
		  if ( icap == 100 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kTeal);
		  if ( icap == 150 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kGreen+2);
		  if ( icap == 200 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kBlue);
		  if ( icap == 300 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kOrange+2);
		  if ( icap == 470 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kRed);
		}
		else {
		  if ( ires == 200 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kTeal);
		  if ( ires == 300 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kGreen+2);
		  if ( ires == 500 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kBlue);
		  if ( ires == 1000 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kRed);
		}


		vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.Draw("p");
	      }
	    
      
	    }
	  }
	  
	}
      }
    }

    leg_top->Draw();

    c_ch->Update();
    ps3->NewPage();

    hist_eff->Draw();

    for ( uint itry = 0; itry<2; itry ++ ) {

      for ( uint ith=0; ith<vmm_vec_tmp.size(); ith++ ) {

	for ( uint ivoltage = 0; ivoltage<HV_vec_tmp.size(); ivoltage++ ) {

	  int ilayer  = layer_vec_tmp.at(ith);
	  int ielink  = elink_vec_tmp.at(ith);
	  int ivmm    = vmm_vec_tmp.at(ith);
	  int iSCA_ID = SCA_ID_vec_tmp.at(ith);

	  int ivolt   = HV_vec_tmp.at(ivoltage);

	  if ( ivolt != voltage ) continue;
	  if ( pads &&  ( iSCA_ID != 993  && iSCA_ID != 1040 ) ) continue;
	  if ( !pads && ( iSCA_ID != 1001 && iSCA_ID != 1026 ) ) continue;

	  std::vector<chan_plots*> * vmm_ch_plot_vec = GetChannelPlots( ilayer, pads,
									ivmm,   ivolt,  data_type );
	  if ( ilayer == 3 ) {

	    for ( uint ich=0; ich<vmm_ch_plot_vec->size(); ich++ ) {

	      bool isgood = vmm_ch_plot_vec->at(ich)->is_good;
	      float area  = vmm_ch_plot_vec->at(ich)->area;
	      int  icap = -1;
	      int  ires = -1;
	      if (  pads ) icap = vmm_ch_plot_vec->at(ich)->capacitance;
	      if ( !pads ) ires = vmm_ch_plot_vec->at(ich)->resistance;

	      if ( isgood && area > 250.0) {
		if ( itry == 0 && ( pads &&icap !=100 ) ) continue;
		if ( itry == 1 && (  pads && icap == 100 ) ) continue;
		if ( itry == 0 && ( !pads && ires != 200 ) ) continue;
		if ( itry == 1 && ( !pads && ires == 200 ) ) continue;
		//if ( itry == 1 ) continue;

		//              vmm_ch_plot_vec->at(ich)->f_1overX_eff_tot->Draw("same");

		if ( pads) {
		  if ( icap == 100 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kTeal);
		  if ( icap == 150 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kGreen+2);
		  if ( icap == 200 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kBlue);
		  if ( icap == 300 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kOrange+2);
		  if ( icap == 470 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kRed);
		}
		else {
		  if ( ires == 200 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kTeal);
		  if ( ires == 300 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kGreen+2);
		  if ( ires == 500 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kBlue);
		  if ( ires == 1000 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kRed);
		}


		vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.Draw("p");
	      }

	    }
	  }

	}
      }
    }

    leg_top->Draw();

    c_ch->Update();
    ps3->NewPage();
    
    //--------------------------------------------------------------//

    m_sx.str("");
    //m_sx << "Efficiency vs Hit Rate for Pads with Size <= 55 cm^{2} ";
    //For QS2
    m_sx << "Efficiency vs Hit Rate for Pads with Size <= 250 cm^{2} ";
    if ( data_type == type_10bit ) m_sx << " with 10 Bit ADC mode ";
    else if ( data_type == type_6bit ) m_sx << " with ToT mode ";
    m_sx << "For Different Pi-Networks at HV=";
    m_sx << voltage << " volts";
    
    hist_eff->SetTitle(m_sx.str().c_str());
    hist_eff->SetXTitle("Channel Hit Rate (kHz)");
    hist_eff->SetYTitle("Efficiency");
    hist_eff->SetMaximum(1.05);
    
    hist_eff->Draw();

    for ( uint itry = 0; itry<2; itry ++ ) {
    
      for ( uint ith=0; ith<vmm_vec_tmp.size(); ith++ ) {
	
	for ( uint ivoltage = 0; ivoltage<HV_vec_tmp.size(); ivoltage++ ) {
	  
	  int ilayer  = layer_vec_tmp.at(ith);
	  int ielink  = elink_vec_tmp.at(ith);
	  int ivmm    = vmm_vec_tmp.at(ith);
	  int iSCA_ID = SCA_ID_vec_tmp.at(ith);
	  
	  int ivolt   = HV_vec_tmp.at(ivoltage);
	  
	  if ( ivolt != voltage ) continue;
	  if ( pads &&  ( iSCA_ID != 993  && iSCA_ID != 1040 ) ) continue;
	  if ( !pads && ( iSCA_ID != 1001 && iSCA_ID != 1026 ) ) continue;
	  
	  std::vector<chan_plots*> * vmm_ch_plot_vec = GetChannelPlots( ilayer, pads,
									ivmm,   ivolt,  data_type );
	  if ( ilayer == 3 ) {
	    for ( uint ich=0; ich<vmm_ch_plot_vec->size(); ich++ ) {
	    
	      bool isgood = vmm_ch_plot_vec->at(ich)->is_good;
	      float area  = vmm_ch_plot_vec->at(ich)->area;
	    
	      int  icap = -1;
	      int  ires = -1;
	      if (  pads ) icap = vmm_ch_plot_vec->at(ich)->capacitance;
	      if ( !pads ) ires = vmm_ch_plot_vec->at(ich)->resistance;
	    
	      //if ( isgood && area <= 55.0) {
	      //For QS2
	      if ( isgood && area <= 250.0) {
		if ( itry == 0 && (  pads && icap != 100 ) ) continue;
		if ( itry == 1 && (  pads && icap == 100 ) ) continue;
		if ( itry == 0 && ( !pads && ires != 200 ) ) continue;
		if ( itry == 1 && ( !pads && ires == 200 ) ) continue;
		vmm_ch_plot_vec->at(ich)->f_expo_eff_tot->Draw("same");

		if ( pads ) {
		  if ( icap == 100 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kTeal);
		  if ( icap == 150 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kGreen+2);
		  if ( icap == 200 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kBlue);
		  if ( icap == 300 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kOrange+2);
		  if ( icap == 470 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kRed);
		}
		else {
		  if ( ires == 200 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kTeal);
		  if ( ires == 300 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kGreen+2);
		  if ( ires == 500 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kBlue);
		  if ( ires == 1000 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kRed);
		}

		vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.Draw("p");
	      
	      }
	    
	    }
	  }
	}
      }
    }

    leg_top->Draw();

    c_ch->Update();
    ps3->NewPage();

    hist_eff->Draw();

    for ( uint itry = 0; itry<2; itry ++ ) {

      for ( uint ith=0; ith<vmm_vec_tmp.size(); ith++ ) {

	for ( uint ivoltage = 0; ivoltage<HV_vec_tmp.size(); ivoltage++ ) {

	  int ilayer  = layer_vec_tmp.at(ith);
	  int ielink  = elink_vec_tmp.at(ith);
	  int ivmm    = vmm_vec_tmp.at(ith);
	  int iSCA_ID = SCA_ID_vec_tmp.at(ith);

	  int ivolt   = HV_vec_tmp.at(ivoltage);

	  if ( ivolt != voltage ) continue;
	  if ( pads &&  ( iSCA_ID != 993  && iSCA_ID != 1040 ) ) continue;
	  if ( !pads && ( iSCA_ID != 1001 && iSCA_ID != 1026 ) ) continue;

	  std::vector<chan_plots*> * vmm_ch_plot_vec = GetChannelPlots( ilayer, pads,
									ivmm,   ivolt,  data_type );
	  if ( ilayer == 3 ) {
	    
	    for ( uint ich=0; ich<vmm_ch_plot_vec->size(); ich++ ) {

	      bool isgood = vmm_ch_plot_vec->at(ich)->is_good;
	      float area  = vmm_ch_plot_vec->at(ich)->area;
	      int  icap = -1;
	      int  ires = -1;
	      if (  pads ) icap = vmm_ch_plot_vec->at(ich)->capacitance;
	      if ( !pads ) ires = vmm_ch_plot_vec->at(ich)->resistance;

	      if ( isgood && area <= 250.0) {
		if ( itry == 0 && ( pads &&icap !=100 ) ) continue;
		if ( itry == 1 && (  pads && icap == 100 ) ) continue;
		if ( itry == 0 && ( !pads && ires != 200 ) ) continue;
		if ( itry == 1 && ( !pads && ires == 200 ) ) continue;
		//if ( itry == 1 ) continue;

		//vmm_ch_plot_vec->at(ich)->f_1overX_eff_tot->Draw("same");

		if ( pads) {
		  if ( icap == 100 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kTeal);
		  if ( icap == 150 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kGreen+2);
		  if ( icap == 200 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kBlue);
		  if ( icap == 300 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kOrange+2);
		  if ( icap == 470 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kRed);
		}
		else {
		  if ( ires == 200 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kTeal);
		  if ( ires == 300 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kGreen+2);
		  if ( ires == 500 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kBlue);
		  if ( ires == 1000 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kRed);
		}


		vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.Draw("p");
	      }

	    }
	  }

	}
      }
    }

    leg_top->Draw();

    c_ch->Update();
        
  }
  ps3->Close();

  gSystem->Exec(("ps2pdf plots/"+ps_name+".eps").Data());
  gSystem->Exec("mv "+ps_name+".pdf plots/"+ps_name+".pdf");

  gPad->SetGridx(0);
  gPad->SetGridy(0);

}

void decoded_event_tree::plotSummaryL4(bool pads, int voltage, int data_type) {

  gStyle->SetOptStat(0);

  m_sx.str("");
  m_sx << "L4_plot_summary_" ;
  if ( pads ) m_sx << "_pads";
  else        m_sx << "_strips";
  if ( data_type == type_10bit ) m_sx << "_10bit";
  else if ( data_type == type_6bit ) m_sx << "_6bit";
  m_sx << "_" << voltage;

  TString ps_name = m_sx.str();

  TPostScript * ps3 = new TPostScript(("plots/"+ps_name+".eps").Data(),112);

  TCanvas * c_ch = new TCanvas(("c_ch"+ps_name).Data(), "c_ch");

  TH1F* hist_100pF_deadTime;
  TH1F* hist_100pF_deadTime_small;
  TH1F* hist_100pF_deadTime_large;

  TH1F* hist_200Ohm_deadTime;

  if ( pads ) {

  m_sx.str("");
  m_sx << "Pad ";
  if ( data_type == type_10bit ) m_sx << "10 bit ADC ";
  else if ( data_type == type_6bit ) m_sx << "ToT ";
  m_sx << "Fitted Dead Time with Different Pi-Networks at HV=";
  m_sx << voltage << " volts";

  hist_100pF_deadTime = new TH1F(("hist_100pF_deadtime"+ps_name).Data(), m_sx.str().c_str(), 50, 200, 400);
  hist_100pF_deadTime->SetXTitle("Channel Fitted Dead Time (ns)");
  hist_100pF_deadTime->SetYTitle("N Channels");
  hist_100pF_deadTime->SetLineColor(kBlack);
  hist_100pF_deadTime->SetFillColor(kTeal);
  hist_100pF_deadTime->SetFillStyle(3003);

  m_sx.str("");
  //m_sx << "Pad with Size <= 55 cm^{2} ";
  //For QS2
  m_sx << "Pad with Size <= 250 cm^{2} ";
  if ( data_type == type_10bit ) m_sx << "10 Bit ADC ";
  else if ( data_type == type_6bit ) m_sx << "ToT ";
  m_sx << "Fitted Dead Time with Different Pi-Networks at HV=";
  m_sx << voltage << " volts";

  hist_100pF_deadTime_small = new TH1F(("hist_100pF_deadtime_small_"+ps_name).Data(), m_sx.str().c_str(), 50, 200, 400);
  hist_100pF_deadTime_small->SetXTitle("Channel Fitted Dead Time (ns)");
  hist_100pF_deadTime_small->SetYTitle("N Channels");
  hist_100pF_deadTime_small->SetLineColor(kBlack);
  hist_100pF_deadTime_small->SetFillColor(kTeal);
  hist_100pF_deadTime_small->SetFillStyle(3003);

  m_sx.str("");
  //m_sx << "Pad with Size > 55 cm^{2} ";
  //For QS2
  m_sx << "Pad with Size > 250 cm^{2} ";
  if ( data_type == type_10bit ) m_sx << "10 Bit ADC ";
  else if ( data_type == type_6bit ) m_sx << "ToT ";
  m_sx << "Fitted Dead Time with Different Pi-Networks at HV=";
  m_sx << voltage << " volts";

  hist_100pF_deadTime_large = new TH1F(("hist_100pF_deadtime_small_"+ps_name).Data(), m_sx.str().c_str(), 50, 200, 400);
  hist_100pF_deadTime_large->SetXTitle("Channel Fitted Dead Time (ns)");
  hist_100pF_deadTime_large->SetYTitle("N Channels");
  hist_100pF_deadTime_large->SetLineColor(kBlack);
  hist_100pF_deadTime_large->SetFillColor(kTeal);
  hist_100pF_deadTime_large->SetFillStyle(3003);

  }
  else {

  m_sx.str("");
  m_sx << "Strip ";
  if ( data_type == type_10bit ) m_sx << "10 Bit ADC ";
  else if ( data_type == type_6bit ) m_sx << "6 Bit ADC ";
  m_sx << "Fitted Dead Time with Different Pull-Up Resistors at HV=";
  m_sx << voltage << " volts";

  hist_200Ohm_deadTime = new TH1F(("hist_200Ohm_deadtime"+ps_name).Data(), m_sx.str().c_str(), 50, 200, 400);
  hist_200Ohm_deadTime->SetXTitle("Channel Fitted Dead Time (ns)");
  hist_200Ohm_deadTime->SetYTitle("N Channels");
  hist_200Ohm_deadTime->SetLineColor(kBlack);
  hist_200Ohm_deadTime->SetFillColor(kTeal);
  hist_200Ohm_deadTime->SetFillStyle(3003);

  }

  std::vector<int> layer_vec_tmp  = layer_vec;
  std::vector<int> elink_vec_tmp  = elink_vec;
  std::vector<int> vmm_vec_tmp    = vmm_vec;
  std::vector<int> SCA_ID_vec_tmp = SCA_ID_vec;
  std::vector<int> HV_vec_tmp     = HV_vec;

  std::vector<float> deadtime_150pF;
  std::vector<float> deadtime_200pF;
  std::vector<float> deadtime_300pF;
  std::vector<float> deadtime_470pF;

  std::vector<float> deadtime_300Ohm;
  std::vector<float> deadtime_500Ohm;
  std::vector<float> deadtime_1000Ohm;

  std::vector<float> deadtime_150pF_small;
  std::vector<float> deadtime_200pF_small;
  std::vector<float> deadtime_300pF_small;
  std::vector<float> deadtime_470pF_small;

  std::vector<float> deadtime_150pF_large;
  std::vector<float> deadtime_200pF_large;
  std::vector<float> deadtime_300pF_large;
  std::vector<float> deadtime_470pF_large;

  TArrow * a_150pF = new TArrow();
  TArrow * a_200pF = new TArrow();
  TArrow * a_300pF = new TArrow();
  TArrow * a_470pF = new TArrow();

  TArrow * a_300Ohm  = new TArrow();
  TArrow * a_500Ohm  = new TArrow();
  TArrow * a_1000Ohm = new TArrow();

  a_150pF->SetLineColor(kGreen+2);
  a_200pF->SetLineColor(kBlue);
  a_300pF->SetLineColor(kOrange+2);
  a_470pF->SetLineColor(kRed);

  a_300Ohm ->SetLineColor(kGreen+2);
  a_500Ohm ->SetLineColor(kBlue);
  a_1000Ohm->SetLineColor(kRed);

  float total_max_rate = 0;

  for ( uint ith=0; ith<vmm_vec_tmp.size(); ith++ ) {
    //std::cout << "voltage vector size: " << HV_vec_tmp.size() << std::endl;
    for ( uint ivoltage = 0; ivoltage<HV_vec_tmp.size(); ivoltage++ ) {

      int ilayer  = layer_vec_tmp.at(ith);
      int ielink  = elink_vec_tmp.at(ith);
      int ivmm    = vmm_vec_tmp.at(ith);
      int iSCA_ID = SCA_ID_vec_tmp.at(ith);
      
      int ivolt   = HV_vec_tmp.at(ivoltage);
      
      if ( ivolt != voltage ) continue;
      if ( pads &&  ( iSCA_ID != 993  && iSCA_ID != 1040 ) ) continue;
      if ( !pads && ( iSCA_ID != 1001 && iSCA_ID != 1026 ) ) continue;

      std::vector<chan_plots*> * vmm_ch_plot_vec = GetChannelPlots( ilayer, pads,
								    ivmm,   ivolt,  data_type );
      
      for ( uint ich=0; ich<vmm_ch_plot_vec->size(); ich++ ) {
	
	bool isgood = vmm_ch_plot_vec->at(ich)->is_good;
	float area  = vmm_ch_plot_vec->at(ich)->area;
	int  icap   = 0;
	int  ires   = 0;
	float deadtime = ( vmm_ch_plot_vec->at(ich)->f_expo_eff_tot->GetParameter(2)*1e6 );
	float max_rate = vmm_ch_plot_vec->at(ich)->max_tot_rate;

	if (  pads ) icap = vmm_ch_plot_vec->at(ich)->capacitance;
	if ( !pads ) ires = vmm_ch_plot_vec->at(ich)->resistance;
	
	if ( isgood ) {
	  
	  if ( total_max_rate < max_rate && max_rate < 10000 ) total_max_rate = max_rate;

	  if ( pads ) {	  

	    if ( ilayer == 4 ) {
	      
	      if      ( icap == 100 ) hist_100pF_deadTime->Fill( deadtime );
	      else if ( icap == 150 ) deadtime_150pF.push_back( deadtime );
	      else if ( icap == 200 ) deadtime_200pF.push_back( deadtime );
	      else if ( icap == 300 ) deadtime_300pF.push_back( deadtime );
	      else if ( icap == 470 ) deadtime_470pF.push_back( deadtime );
	    
	      if ( area <= 250.0 ) {
	      
		if      ( icap == 100 ) hist_100pF_deadTime_small->Fill( deadtime );
		else if ( icap == 150 ) deadtime_150pF_small.push_back( deadtime );
		else if ( icap == 200 ) deadtime_200pF_small.push_back( deadtime );
		else if ( icap == 300 ) deadtime_300pF_small.push_back( deadtime );
		else if ( icap == 470 ) deadtime_470pF_small.push_back( deadtime );
	      
	      }
	      else {
	      
		if      ( icap == 100 ) hist_100pF_deadTime_large->Fill( deadtime );
		else if ( icap == 150 ) deadtime_150pF_large.push_back( deadtime );
		else if ( icap == 200 ) deadtime_200pF_large.push_back( deadtime );
		else if ( icap == 300 ) deadtime_300pF_large.push_back( deadtime );
		else if ( icap == 470 ) deadtime_470pF_large.push_back( deadtime );
	      
	      }
	    }
	    
	  }
	  else if ( !pads ) {
	    
	    if ( ilayer == 3 && ivmm == 4 ) {
	      if      ( ires == 200  ) hist_200Ohm_deadTime->Fill( vmm_ch_plot_vec->at(ich)->f_expo_eff_tot->GetParameter(2)*1e6 );
	      else if ( ires == 300  ) deadtime_300Ohm.push_back( deadtime );
	      else if ( ires == 500  ) deadtime_500Ohm.push_back( deadtime );
	      else if ( ires == 1000 ) deadtime_1000Ohm.push_back( deadtime );
	    }
	  }
	}
      } // loop over channels
      //std::cout << "I have finished channel looping" << std::endl;
    } // loop over voltages
    std::cout << "I have finished voltage looping for VMM " << ith << " For pads? " << pads << std::endl;
  } // loop over vmms
  std::cout << "I have looped over this vmm" << std::endl;

  c_ch->cd();
  
  if ( pads ) {
    hist_100pF_deadTime->Draw();

    float maximum = hist_100pF_deadTime->GetMaximum();

    for ( uint i=0; i< deadtime_150pF.size(); i++ ) {
      a_150pF->DrawArrow(deadtime_150pF.at(i), 0, 
			 deadtime_150pF.at(i), maximum*0.2, 0.01, "<");
    }
    for ( uint i=0; i< deadtime_200pF.size(); i++ ) {
      a_200pF->DrawArrow(deadtime_200pF.at(i), 0,
			 deadtime_200pF.at(i), maximum*0.2, 0.01, "<");
    }
    for ( uint i=0; i< deadtime_300pF.size(); i++ ) {
      a_300pF->DrawArrow(deadtime_300pF.at(i), 0,
			 deadtime_300pF.at(i), maximum*0.2, 0.01, "<");
    }
    for ( uint i=0; i< deadtime_470pF.size(); i++ ) {
      a_470pF->DrawArrow(deadtime_470pF.at(i), 0,
			 deadtime_470pF.at(i), maximum*0.2, 0.01, "<");
    }

    leg_top->Clear();
    leg_top->AddEntry( hist_100pF_deadTime, "100 pF Channels", "f");
    leg_top->AddEntry( a_150pF,             "150 pF Channels", "l");
    leg_top->AddEntry( a_200pF,             "200 pF Channels", "l");
    leg_top->AddEntry( a_300pF,             "300 pF Channels", "l");
    leg_top->AddEntry( a_470pF,             "470 pF Channels", "l");
    leg_top->Draw();

    c_ch->Update();
    ps3->NewPage();

    //-----------------------------//

    hist_100pF_deadTime_small->Draw();

    maximum = hist_100pF_deadTime_small->GetMaximum();

    for ( uint i=0; i< deadtime_150pF_small.size(); i++ ) {
      a_150pF->DrawArrow(deadtime_150pF_small.at(i), 0,
                         deadtime_150pF_small.at(i), maximum*0.2, 0.01, "<");
    }
    for ( uint i=0; i< deadtime_200pF_small.size(); i++ ) {
      a_200pF->DrawArrow(deadtime_200pF_small.at(i), 0,
                         deadtime_200pF_small.at(i), maximum*0.2, 0.01, "<");
    }
    for ( uint i=0; i< deadtime_300pF_small.size(); i++ ) {
      a_300pF->DrawArrow(deadtime_300pF_small.at(i), 0,
                         deadtime_300pF_small.at(i), maximum*0.2, 0.01, "<");
    }
    for ( uint i=0; i< deadtime_470pF_small.size(); i++ ) {
      a_470pF->DrawArrow(deadtime_470pF_small.at(i), 0,
                         deadtime_470pF_small.at(i), maximum*0.2, 0.01, "<");
    }

    leg_top->Clear();
    leg_top->AddEntry( hist_100pF_deadTime_small, "100 pF Channels", "f");
    leg_top->AddEntry( a_150pF,             "150 pF Channels", "l");
    leg_top->AddEntry( a_200pF,             "200 pF Channels", "l");
    leg_top->AddEntry( a_300pF,             "300 pF Channels", "l");
    leg_top->AddEntry( a_470pF,             "470 pF Channels", "l");
    leg_top->Draw();

    c_ch->Update();
    ps3->NewPage();

    //---------------------//

    hist_100pF_deadTime_large->Draw();
    
    maximum = hist_100pF_deadTime_large->GetMaximum();

    for ( uint i=0; i< deadtime_150pF_large.size(); i++ ) {
      a_150pF->DrawArrow(deadtime_150pF_large.at(i), 0,
                         deadtime_150pF_large.at(i), maximum*0.2, 0.01, "<");
    }
    for ( uint i=0; i< deadtime_200pF_large.size(); i++ ) {
      a_200pF->DrawArrow(deadtime_200pF_large.at(i), 0,
                         deadtime_200pF_large.at(i), maximum*0.2, 0.01, "<");
    }
    for ( uint i=0; i< deadtime_300pF_large.size(); i++ ) {
      a_300pF->DrawArrow(deadtime_300pF_large.at(i), 0,
                         deadtime_300pF_large.at(i), maximum*0.2, 0.01, "<");
    }
    for ( uint i=0; i< deadtime_470pF_large.size(); i++ ) {
      a_470pF->DrawArrow(deadtime_470pF_large.at(i), 0,
                         deadtime_470pF_large.at(i), maximum*0.2, 0.01, "<");
    }

    leg_top->Clear();
    leg_top->AddEntry( hist_100pF_deadTime_large, "100 pF Channels", "f");
    leg_top->AddEntry( a_150pF,             "150 pF Channels", "l");
    leg_top->AddEntry( a_200pF,             "200 pF Channels", "l");
    leg_top->AddEntry( a_300pF,             "300 pF Channels", "l");
    leg_top->AddEntry( a_470pF,             "470 pF Channels", "l");
    leg_top->Draw();

    c_ch->Update();
    ps3->NewPage();

  }
  else {
    hist_200Ohm_deadTime->Draw();

    float maximum = hist_200Ohm_deadTime->GetMaximum();

    for ( uint i=0; i< deadtime_300Ohm.size(); i++ ) {
      a_300Ohm->DrawArrow(deadtime_300Ohm.at(i), 0,
			   deadtime_300Ohm.at(i), maximum*0.2, 0.01, "<");
    }
    for ( uint i=0; i< deadtime_500Ohm.size(); i++ ) {
      a_500Ohm->DrawArrow(deadtime_500Ohm.at(i), 0,
			   deadtime_500Ohm.at(i), maximum*0.2, 0.01, "<");
    }
    for ( uint i=0; i< deadtime_1000Ohm.size(); i++ ) {
      a_1000Ohm->DrawArrow(deadtime_1000Ohm.at(i), 0,
			    deadtime_1000Ohm.at(i), maximum*0.2, 0.01, "<");
    }

    leg_top->Clear();
    leg_top->AddEntry( hist_200Ohm_deadTime, "200 k#Omega Channels",  "f");
    leg_top->AddEntry( a_300Ohm,             "300 k#Omega Channels",  "l");
    leg_top->AddEntry( a_500Ohm,             "500 k#Omega Channels",  "l");
    leg_top->AddEntry( a_1000Ohm,            "  1 M#Omega Channels", "l");
    leg_top->Draw();

    c_ch->Update();
    ps3->NewPage();
  }
  
  //----------------------------------------------------------------------//
  //                    Plot all Efficiency Curves
  //----------------------------------------------------------------------//

  gPad->SetGridx(1);
  gPad->SetGridy(1);

  TH1F* hist_eff = new TH1F(("hist_eff_"+ps_name).Data(), "", 1, 0, total_max_rate);

  m_sx.str("");
  if ( pads )                                 m_sx << "Efficiency vs Hit Rate for Pads";
  else                                        m_sx << "Efficiency vs Hit Rate for Strips";
  if ( data_type == type_10bit )              m_sx << " with 10 Bit ADC mode ";
  else if ( pads  && data_type == type_6bit ) m_sx << " with ToT mode ";
  else if ( !pads && data_type == type_6bit ) m_sx << " with 6 Bit ADC mode ";
  if ( pads )                                 m_sx << "For Different Pi-Networks at HV=";
  else                                        m_sx << "For Different Pull-Up Resistors at HV=";
  m_sx << voltage << " volts";

  hist_eff->SetTitle(m_sx.str().c_str());
  hist_eff->SetXTitle("Channel Hit Rate (kHz)");
  hist_eff->SetYTitle("Efficiency");
  hist_eff->SetMaximum(1.05);
  hist_eff->SetMinimum(0.40);

  hist_eff->Draw();

  for ( uint itry = 0; itry<2; itry ++ ) {
    
    for ( uint ith=0; ith<vmm_vec_tmp.size(); ith++ ) {
      
      for ( uint ivoltage = 0; ivoltage<HV_vec_tmp.size(); ivoltage++ ) {
	
	int ilayer  = layer_vec_tmp.at(ith);
	int ielink  = elink_vec_tmp.at(ith);
	int ivmm    = vmm_vec_tmp.at(ith);
	int iSCA_ID = SCA_ID_vec_tmp.at(ith);
	
	int ivolt   = HV_vec_tmp.at(ivoltage);
	
	if ( ivolt != voltage ) continue;
	if ( pads &&  ( iSCA_ID != 993  && iSCA_ID != 1040 ) ) continue;
	if ( !pads && ( iSCA_ID != 1001 && iSCA_ID != 1026 ) ) continue;
	
	std::vector<chan_plots*> * vmm_ch_plot_vec = GetChannelPlots( ilayer, pads,
								      ivmm,   ivolt,  data_type );
	if ( ilayer == 4 ) {
	  for ( uint ich=0; ich<vmm_ch_plot_vec->size(); ich++ ) {
	  
	    bool isgood = vmm_ch_plot_vec->at(ich)->is_good;
	    int  icap = -1;
	    int  ires = -1;
	    if (  pads ) icap = vmm_ch_plot_vec->at(ich)->capacitance;
	    if ( !pads ) ires = vmm_ch_plot_vec->at(ich)->resistance;

	    if ( isgood ) {
	      if ( itry == 0 && (  pads && icap != 100 ) ) continue;
	      if ( itry == 1 && (  pads && icap == 100 ) ) continue;
	      if ( itry == 0 && ( !pads && ires != 200 ) ) continue;
	      if ( itry == 1 && ( !pads && ires == 200 ) ) continue;
	      vmm_ch_plot_vec->at(ich)->f_expo_eff_tot->Draw("same");

	      if ( pads) {
		if ( icap == 100 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kTeal);
		if ( icap == 150 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kGreen+2);
		if ( icap == 200 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kBlue);
		if ( icap == 300 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kOrange+2);
		if ( icap == 470 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kRed);
	      }
	      else {
		if ( ires == 200 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kTeal);
		if ( ires == 300 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kGreen+2);
		if ( ires == 500 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kBlue);
		if ( ires == 1000 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kRed);
	      }

	      vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.Draw("p");
	    }
	  }
	}
	
      }
    }

  }
  leg_top->Draw();

  c_ch->Update();
  ps3->NewPage();

  hist_eff->Draw();

  for ( uint itry = 0; itry<2; itry ++ ) {

    for ( uint ith=0; ith<vmm_vec_tmp.size(); ith++ ) {

      for ( uint ivoltage = 0; ivoltage<HV_vec_tmp.size(); ivoltage++ ) {

	int ilayer  = layer_vec_tmp.at(ith);
	int ielink  = elink_vec_tmp.at(ith);
	int ivmm    = vmm_vec_tmp.at(ith);
	int iSCA_ID = SCA_ID_vec_tmp.at(ith);

	int ivolt   = HV_vec_tmp.at(ivoltage);

	if ( ivolt != voltage ) continue;
	if ( pads &&  ( iSCA_ID != 993  && iSCA_ID != 1040 ) ) continue;
	if ( !pads && ( iSCA_ID != 1001 && iSCA_ID != 1026 ) ) continue;

	std::vector<chan_plots*> * vmm_ch_plot_vec = GetChannelPlots( ilayer, pads,
								      ivmm,   ivolt,  data_type );
	if ( ilayer == 4 ) {
	  
	  for ( uint ich=0; ich<vmm_ch_plot_vec->size(); ich++ ) {

	    bool isgood = vmm_ch_plot_vec->at(ich)->is_good;
	    int  icap = -1;
	    int  ires = -1;
	    if (  pads ) icap = vmm_ch_plot_vec->at(ich)->capacitance;
	    if ( !pads ) ires = vmm_ch_plot_vec->at(ich)->resistance;

	    if ( isgood ) {
	      if ( itry == 0 && (  pads && icap != 100 ) ) continue;
	      if ( itry == 1 && (  pads && icap == 100 ) ) continue;
	      if ( itry == 0 && ( !pads && ires != 200 ) ) continue;
	      if ( itry == 1 && ( !pads && ires == 200 ) ) continue;
	      //if ( itry == 1 ) continue;
	      //vmm_ch_plot_vec->at(ich)->f_1overX_eff_tot->Draw("same");

	      if ( pads) {
		if ( icap == 100 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kTeal);
		if ( icap == 150 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kGreen+2);
		if ( icap == 200 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kBlue);
		if ( icap == 300 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kOrange+2);
		if ( icap == 470 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kRed);
	      }
	      else {
		if ( ires == 200 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kTeal);
		if ( ires == 300 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kGreen+2);
		if ( ires == 500 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kBlue);
		if ( ires == 1000 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kRed);
	      }


	      vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.Draw("p");

	    }
	  }
	}

      }
    }

  }
  leg_top->Draw();

  c_ch->Update();
  ps3->NewPage();
  
  //----------------------------------------------------------------------//

  if ( pads ) {

    m_sx.str("");
    //m_sx << "Efficiency vs Hit Rate for Pads with Size > 55 cm^{2} ";
    //For QS2
    m_sx << "Efficiency vs Hit Rate for Pads with Size > 250 cm^{2} ";
    if ( data_type == type_10bit ) m_sx << " with 10 Bit ADC mode ";
    else if ( data_type == type_6bit ) m_sx << " with ToT mode ";
    m_sx << "For Different Pi-Networks at HV=";
    m_sx << voltage << " volts";

    hist_eff->SetTitle(m_sx.str().c_str());
    hist_eff->SetXTitle("Channel Hit Rate (kHz)");
    hist_eff->SetYTitle("Efficiency");
    hist_eff->SetMaximum(1.05);
    hist_eff->SetMinimum(0.40);


    hist_eff->Draw();

    for ( uint itry = 0; itry<2; itry ++ ) {

      for ( uint ith=0; ith<vmm_vec_tmp.size(); ith++ ) {
	
	for ( uint ivoltage = 0; ivoltage<HV_vec_tmp.size(); ivoltage++ ) {

	  int ilayer  = layer_vec_tmp.at(ith);
	  int ielink  = elink_vec_tmp.at(ith);
	  int ivmm    = vmm_vec_tmp.at(ith);
	  int iSCA_ID = SCA_ID_vec_tmp.at(ith);
	  
	  int ivolt   = HV_vec_tmp.at(ivoltage);
	  
	  if ( ivolt != voltage ) continue;
	  if ( pads &&  ( iSCA_ID != 993  && iSCA_ID != 1040 ) ) continue;
	  if ( !pads && ( iSCA_ID != 1001 && iSCA_ID != 1026 ) ) continue;
	  
	  std::vector<chan_plots*> * vmm_ch_plot_vec = GetChannelPlots( ilayer, pads,
									ivmm,   ivolt,  data_type );
	  if ( ilayer == 4 ) { 
	    for ( uint ich=0; ich<vmm_ch_plot_vec->size(); ich++ ) {
	    
	      bool isgood = vmm_ch_plot_vec->at(ich)->is_good;
	      float area  = vmm_ch_plot_vec->at(ich)->area;
	      int  icap = -1;
	      int  ires = -1;
	      if (  pads ) icap = vmm_ch_plot_vec->at(ich)->capacitance;
	      if ( !pads ) ires = vmm_ch_plot_vec->at(ich)->resistance;
	    
	      //if ( isgood && area > 55.0) {
	      //For QS2
	      if ( isgood && area > 250.0) {
		if ( itry == 0 && ( pads &&icap !=100 ) ) continue;
		if ( itry == 1 && (  pads && icap == 100 ) ) continue;
		if ( itry == 0 && ( !pads && ires != 200 ) ) continue;
		if ( itry == 1 && ( !pads && ires == 200 ) ) continue;
		vmm_ch_plot_vec->at(ich)->f_expo_eff_tot->Draw("same");

		if ( pads) {
		  if ( icap == 100 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kTeal);
		  if ( icap == 150 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kGreen+2);
		  if ( icap == 200 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kBlue);
		  if ( icap == 300 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kOrange+2);
		  if ( icap == 470 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kRed);
		}
		else {
		  if ( ires == 200 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kTeal);
		  if ( ires == 300 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kGreen+2);
		  if ( ires == 500 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kBlue);
		  if ( ires == 1000 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kRed);
		}


		vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.Draw("p");
	      }
	    
      
	    }
	  }
	  
	}
      }
    }

    leg_top->Draw();

    c_ch->Update();
    ps3->NewPage();

    hist_eff->Draw();

    for ( uint itry = 0; itry<2; itry ++ ) {

      for ( uint ith=0; ith<vmm_vec_tmp.size(); ith++ ) {

	for ( uint ivoltage = 0; ivoltage<HV_vec_tmp.size(); ivoltage++ ) {

	  int ilayer  = layer_vec_tmp.at(ith);
	  int ielink  = elink_vec_tmp.at(ith);
	  int ivmm    = vmm_vec_tmp.at(ith);
	  int iSCA_ID = SCA_ID_vec_tmp.at(ith);

	  int ivolt   = HV_vec_tmp.at(ivoltage);

	  if ( ivolt != voltage ) continue;
	  if ( pads &&  ( iSCA_ID != 993  && iSCA_ID != 1040 ) ) continue;
	  if ( !pads && ( iSCA_ID != 1001 && iSCA_ID != 1026 ) ) continue;

	  std::vector<chan_plots*> * vmm_ch_plot_vec = GetChannelPlots( ilayer, pads,
									ivmm,   ivolt,  data_type );
	  if ( ilayer == 4 ) {

	    for ( uint ich=0; ich<vmm_ch_plot_vec->size(); ich++ ) {

	      bool isgood = vmm_ch_plot_vec->at(ich)->is_good;
	      float area  = vmm_ch_plot_vec->at(ich)->area;
	      int  icap = -1;
	      int  ires = -1;
	      if (  pads ) icap = vmm_ch_plot_vec->at(ich)->capacitance;
	      if ( !pads ) ires = vmm_ch_plot_vec->at(ich)->resistance;

	      if ( isgood && area > 250.0) {
		if ( itry == 0 && ( pads &&icap !=100 ) ) continue;
		if ( itry == 1 && (  pads && icap == 100 ) ) continue;
		if ( itry == 0 && ( !pads && ires != 200 ) ) continue;
		if ( itry == 1 && ( !pads && ires == 200 ) ) continue;
		//if ( itry == 1 ) continue;

		//              vmm_ch_plot_vec->at(ich)->f_1overX_eff_tot->Draw("same");

		if ( pads) {
		  if ( icap == 100 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kTeal);
		  if ( icap == 150 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kGreen+2);
		  if ( icap == 200 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kBlue);
		  if ( icap == 300 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kOrange+2);
		  if ( icap == 470 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kRed);
		}
		else {
		  if ( ires == 200 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kTeal);
		  if ( ires == 300 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kGreen+2);
		  if ( ires == 500 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kBlue);
		  if ( ires == 1000 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kRed);
		}


		vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.Draw("p");
	      }

	    }
	  }

	}
      }
    }

    leg_top->Draw();

    c_ch->Update();
    ps3->NewPage();
    
    //--------------------------------------------------------------//

    m_sx.str("");
    //m_sx << "Efficiency vs Hit Rate for Pads with Size <= 55 cm^{2} ";
    //For QS2
    m_sx << "Efficiency vs Hit Rate for Pads with Size <= 250 cm^{2} ";
    if ( data_type == type_10bit ) m_sx << " with 10 Bit ADC mode ";
    else if ( data_type == type_6bit ) m_sx << " with ToT mode ";
    m_sx << "For Different Pi-Networks at HV=";
    m_sx << voltage << " volts";
    
    hist_eff->SetTitle(m_sx.str().c_str());
    hist_eff->SetXTitle("Channel Hit Rate (kHz)");
    hist_eff->SetYTitle("Efficiency");
    hist_eff->SetMaximum(1.05);
    
    hist_eff->Draw();

    for ( uint itry = 0; itry<2; itry ++ ) {
    
      for ( uint ith=0; ith<vmm_vec_tmp.size(); ith++ ) {
	
	for ( uint ivoltage = 0; ivoltage<HV_vec_tmp.size(); ivoltage++ ) {
	  
	  int ilayer  = layer_vec_tmp.at(ith);
	  int ielink  = elink_vec_tmp.at(ith);
	  int ivmm    = vmm_vec_tmp.at(ith);
	  int iSCA_ID = SCA_ID_vec_tmp.at(ith);
	  
	  int ivolt   = HV_vec_tmp.at(ivoltage);
	  
	  if ( ivolt != voltage ) continue;
	  if ( pads &&  ( iSCA_ID != 993  && iSCA_ID != 1040 ) ) continue;
	  if ( !pads && ( iSCA_ID != 1001 && iSCA_ID != 1026 ) ) continue;
	  
	  std::vector<chan_plots*> * vmm_ch_plot_vec = GetChannelPlots( ilayer, pads,
									ivmm,   ivolt,  data_type );
	  if ( ilayer == 4 ) {
	    for ( uint ich=0; ich<vmm_ch_plot_vec->size(); ich++ ) {
	    
	      bool isgood = vmm_ch_plot_vec->at(ich)->is_good;
	      float area  = vmm_ch_plot_vec->at(ich)->area;
	    
	      int  icap = -1;
	      int  ires = -1;
	      if (  pads ) icap = vmm_ch_plot_vec->at(ich)->capacitance;
	      if ( !pads ) ires = vmm_ch_plot_vec->at(ich)->resistance;
	    
	      //if ( isgood && area <= 55.0) {
	      //For QS2
	      if ( isgood && area <= 250.0) {
		if ( itry == 0 && (  pads && icap != 100 ) ) continue;
		if ( itry == 1 && (  pads && icap == 100 ) ) continue;
		if ( itry == 0 && ( !pads && ires != 200 ) ) continue;
		if ( itry == 1 && ( !pads && ires == 200 ) ) continue;
		vmm_ch_plot_vec->at(ich)->f_expo_eff_tot->Draw("same");

		if ( pads ) {
		  if ( icap == 100 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kTeal);
		  if ( icap == 150 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kGreen+2);
		  if ( icap == 200 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kBlue);
		  if ( icap == 300 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kOrange+2);
		  if ( icap == 470 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kRed);
		}
		else {
		  if ( ires == 200 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kTeal);
		  if ( ires == 300 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kGreen+2);
		  if ( ires == 500 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kBlue);
		  if ( ires == 1000 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kRed);
		}

		vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.Draw("p");
	      
	      }
	    
	    }
	  }
	}
      }
    }

    leg_top->Draw();

    c_ch->Update();
    ps3->NewPage();

    hist_eff->Draw();

    for ( uint itry = 0; itry<2; itry ++ ) {

      for ( uint ith=0; ith<vmm_vec_tmp.size(); ith++ ) {

	for ( uint ivoltage = 0; ivoltage<HV_vec_tmp.size(); ivoltage++ ) {

	  int ilayer  = layer_vec_tmp.at(ith);
	  int ielink  = elink_vec_tmp.at(ith);
	  int ivmm    = vmm_vec_tmp.at(ith);
	  int iSCA_ID = SCA_ID_vec_tmp.at(ith);

	  int ivolt   = HV_vec_tmp.at(ivoltage);

	  if ( ivolt != voltage ) continue;
	  if ( pads &&  ( iSCA_ID != 993  && iSCA_ID != 1040 ) ) continue;
	  if ( !pads && ( iSCA_ID != 1001 && iSCA_ID != 1026 ) ) continue;

	  std::vector<chan_plots*> * vmm_ch_plot_vec = GetChannelPlots( ilayer, pads,
									ivmm,   ivolt,  data_type );
	  if ( ilayer == 4 ) {
	    
	    for ( uint ich=0; ich<vmm_ch_plot_vec->size(); ich++ ) {

	      bool isgood = vmm_ch_plot_vec->at(ich)->is_good;
	      float area  = vmm_ch_plot_vec->at(ich)->area;
	      int  icap = -1;
	      int  ires = -1;
	      if (  pads ) icap = vmm_ch_plot_vec->at(ich)->capacitance;
	      if ( !pads ) ires = vmm_ch_plot_vec->at(ich)->resistance;

	      if ( isgood && area <= 250.0) {
		if ( itry == 0 && ( pads &&icap !=100 ) ) continue;
		if ( itry == 1 && (  pads && icap == 100 ) ) continue;
		if ( itry == 0 && ( !pads && ires != 200 ) ) continue;
		if ( itry == 1 && ( !pads && ires == 200 ) ) continue;
		//if ( itry == 1 ) continue;

		//vmm_ch_plot_vec->at(ich)->f_1overX_eff_tot->Draw("same");

		if ( pads) {
		  if ( icap == 100 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kTeal);
		  if ( icap == 150 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kGreen+2);
		  if ( icap == 200 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kBlue);
		  if ( icap == 300 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kOrange+2);
		  if ( icap == 470 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kRed);
		}
		else {
		  if ( ires == 200 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kTeal);
		  if ( ires == 300 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kGreen+2);
		  if ( ires == 500 )  vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kBlue);
		  if ( ires == 1000 ) vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.SetMarkerColor(kRed);
		}


		vmm_ch_plot_vec->at(ich)->eff_vs_photon_rate_tot.Draw("p");
	      }

	    }
	  }

	}
      }
    }

    leg_top->Draw();

    c_ch->Update();
        
  }
  ps3->Close();

  gSystem->Exec(("ps2pdf plots/"+ps_name+".eps").Data());
  gSystem->Exec("mv "+ps_name+".pdf plots/"+ps_name+".pdf");

  gPad->SetGridx(0);
  gPad->SetGridy(0);

}

void decoded_event_tree::plotChannelPlots(bool pads, bool isAnalog, int data_type, int detector_type) {

  gStyle->SetOptStat(0);

  m_sx.str("");
  m_sx << "plot_chan" ;
  if ( pads ) m_sx << "_pads";
  else        m_sx << "_strips";
  if ( isAnalog ) m_sx << "_analog";
  else            m_sx << "_digital";
  if ( data_type == type_10bit ) m_sx << "_10bit";
  else if ( data_type == type_6bit ) m_sx << "_6bit";

  TString ps_name = m_sx.str();   
  TPostScript * ps3;

  if ( detector_type == 1 ) {
    ps3 = new TPostScript(("plotsQL1/"+ps_name+".eps").Data(),112);   
  }

  if ( detector_type == 2 ) {
    ps3 = new TPostScript(("plotsQS2/"+ps_name+".eps").Data(),112);   
  }
  
  TCanvas * c_ch = new TCanvas(("c_ch"+ps_name).Data(), "c_ch");
  TH1F* hist = new TH1F(("hist"+ps_name).Data(), "", 1, 0, max_kek_rate*1.1);

  std::vector<int> layer_vec_tmp ;
  std::vector<int> elink_vec_tmp;
  std::vector<int> vmm_vec_tmp;
  std::vector<int> SCA_ID_vec_tmp;

  std::vector<int> HV_vec_tmp;

  if ( isAnalog ) {
    layer_vec_tmp  = layer_vec_ana;
    elink_vec_tmp  = elink_vec_ana;
    vmm_vec_tmp    = vmm_vec_ana;
    SCA_ID_vec_tmp = SCA_ID_vec_ana;

    HV_vec_tmp     = HV_vec_ana;
  }
  else {
    layer_vec_tmp  = layer_vec;
    elink_vec_tmp  = elink_vec;
    vmm_vec_tmp    = vmm_vec;
    SCA_ID_vec_tmp = SCA_ID_vec;

    HV_vec_tmp     = HV_vec;
  }

  //---------------------------------------------//
  //  init plots for all channels 
  //---------------------------------------------// 

  for ( uint ith=0; ith<vmm_vec_tmp.size(); ith++ ) {

    for ( uint ivoltage = 0; ivoltage<HV_vec_tmp.size(); ivoltage++ ) {

      int ilayer  = layer_vec_tmp.at(ith);
      int ielink  = elink_vec_tmp.at(ith);
      int ivmm    = vmm_vec_tmp.at(ith);
      int iSCA_ID = SCA_ID_vec_tmp.at(ith);

      int ivolt   = HV_vec_tmp.at(ivoltage);

      if ( pads &&  ( iSCA_ID != 993  && iSCA_ID != 1040 ) ) continue;
      if ( !pads && ( iSCA_ID != 1001 && iSCA_ID != 1026 ) ) continue;

      std::vector<chan_plots*> * vmm_ch_plot_vec;

      // pFEBs
      if ( pads && ( iSCA_ID == 1040 ||
		     iSCA_ID == 993 ) ) {
	
	if ( isAnalog ) { 
	  vmm_ch_plot_vec = GetAnalogChannelPlots( ilayer, true,
						   ivmm,   ivolt );
	}
	else {
          vmm_ch_plot_vec = GetChannelPlots( ilayer, true,
                                             ivmm,   ivolt,  data_type );
        }

      }
      else if ( !pads && ( iSCA_ID == 1001 ||
			   iSCA_ID == 1026 ) ) { // sFEB

        if ( isAnalog ) {
          vmm_ch_plot_vec = GetAnalogChannelPlots( ilayer, false,
                                                   ivmm,   ivolt );
        }
	else {
          vmm_ch_plot_vec = GetChannelPlots( ilayer, false,
                                             ivmm,   ivolt, data_type );
        }

      }	
      else {
	//std::cout << "Warning: Unrecognized VMM type"
        //          << " layer "  << ilayer
        //          << " vmm "    << ivmm
        //          << " SCA ID " << iSCA_ID << std::endl;
	continue;
      }

      for ( int ich=0; ich<vmm_ch_plot_vec->size(); ich++ ) {
	if ( vmm_ch_plot_vec->at(ich)->npoints != 0 ) {	  
	  vmm_ch_plot_vec->at(ich)->FitAll( max_kek_rate );
	  vmm_ch_plot_vec->at(ich)->DrawAll( max_kek_rate, ps3, c_ch, hist );
	}
      }
      
    } // loop over different voltages
  } // loop over different VMMs to be plotted

  ps3->Close();

  if ( detector_type == 1 ) {
    gSystem->Exec(("ps2pdf plotsQL1/"+ps_name+".eps").Data());
    gSystem->Exec("mv "+ps_name+".pdf plotsQL1/"+ps_name+".pdf");
  }
 
  if ( detector_type == 2 ) {
    gSystem->Exec(("ps2pdf plotsQS2/"+ps_name+".eps").Data());
    gSystem->Exec("mv "+ps_name+".pdf plotsQS2/"+ps_name+".pdf");
  }
}

void decoded_event_tree::plotChannelComparePlots( int data_type ) {

  gStyle->SetOptStat(0);

  m_sx.str("");
  m_sx << "plot_chan_compare" ;
  if ( data_type == type_10bit )      m_sx << "_10bit";
  else if ( data_type == type_6bit  ) m_sx << "_6bit";

  TString ps_name = m_sx.str();

  TPostScript * ps3 = new TPostScript(("plots/"+ps_name+".eps").Data(),112);

  TCanvas * c_ch_comp = new TCanvas(("c_ch_comp"+ps_name).Data(), "c_ch_comp");
  TH1F* hist_comp = new TH1F(("hist_comp"+ps_name).Data(), "", 1, 0, max_kek_rate*1.1);

  //---------------------------------------------//
  //  init plots for all channels                  
  //---------------------------------------------//

  for ( uint ith=0; ith<vmm_vec.size(); ith++ ) {

    for ( uint ivoltage = 0; ivoltage<HV_vec.size(); ivoltage++ ) {

      int ilayer  = layer_vec.at(ith);
      int ielink  = elink_vec.at(ith);
      int ivmm    = vmm_vec.at(ith);
      int iSCA_ID = SCA_ID_vec.at(ith);

      int ivolt   = HV_vec.at(ivoltage);

      if ( ilayer == 3 ) {

	for ( int ich=0; ich<GetChannelComparePlots( ilayer, true, ivmm, ivolt, data_type )->size(); ich++ ) {
	  
	  //std::cout << "plotting compare" << std::endl;
	  
	  GetChannelComparePlots( ilayer, true, ivmm, ivolt, data_type )->at(ich)->MakePlots();
	  GetChannelComparePlots( ilayer, true, ivmm, ivolt, data_type )->at(ich)->FitAll(  max_kek_rate );
	  GetChannelComparePlots( ilayer, true, ivmm, ivolt, data_type )->at(ich)->DrawAll( max_kek_rate, ps3, 
											    c_ch_comp,    hist_comp );
	  
	}
      }

    }
  }

  ps3->Close();

  gSystem->Exec(("ps2pdf plots/"+ps_name+".eps").Data());
  gSystem->Exec("mv "+ps_name+".pdf plots/"+ps_name+".pdf");

}

float decoded_event_tree::GetChanEfficiency( TH1F * h_rel_bcid, int n_triggers, float photon_rate ) {

  if ( !h_rel_bcid ) return -1.0;

  std::cout << h_rel_bcid->GetName() ;

  std::vector<int> pad_rel_bcid;
  pad_rel_bcid.resize(0);

  float n_photon_hits_per25ns = photon_rate * 1000.0 * 25e-9;

  for ( uint ibcid = 1; ibcid < h_rel_bcid->GetNbinsX()+1; ibcid++ ){
    pad_rel_bcid.push_back( h_rel_bcid->GetBinContent( ibcid ) );
  }
      
  int intime_bcid    = 0;
  int max_intime_hit = 0;
  
  for ( uint ibc =0; ibc<pad_rel_bcid.size(); ibc++ ) {
    
    std::cout << " " << pad_rel_bcid.at(ibc) ;

    if ( max_intime_hit < pad_rel_bcid.at( ibc ) ) {
      max_intime_hit = pad_rel_bcid.at(ibc);
      intime_bcid = ibc;
    }
  }

  std::cout << std::endl;

  if ( intime_bcid == 0 || intime_bcid == 7 ) {
    std::cout << "Error: Cannot compute efficiency, aborting " << h_rel_bcid->GetName() << std::endl;
    return -1.0;
  }

  int n_in_time_hit = ( pad_rel_bcid.at(intime_bcid-1) +
			pad_rel_bcid.at(intime_bcid)   +
			pad_rel_bcid.at(intime_bcid+1) );

  float avg_in_time = ( pad_rel_bcid.at(intime_bcid-1) +
			pad_rel_bcid.at(intime_bcid)*2 +
			pad_rel_bcid.at(intime_bcid+1)*3 )/3.0/n_in_time_hit;

  float efficiency  = ( n_in_time_hit - (n_photon_hits_per25ns*avg_in_time +
					 pad_rel_bcid.at(pad_rel_bcid.size()-1)*(3.0-avg_in_time)) )*1.0/n_triggers;

  std::cout << "Efficiency: "   << efficiency 
	    << " intime_bcid "  << intime_bcid 
	    << " n_intime_hit " << n_in_time_hit 
	    << " n_triggers "    << n_triggers << " hit/trig " << n_in_time_hit*1.0/n_triggers << std::endl;

  return efficiency;

}


void decoded_event_tree::plotPadHits( TH1F * h_pad_vmmB, TH1F *h_pad_vmmC,
				      int layer, int n_triggers, 
				      float kek_photon_rate, float attenuation, int voltage,
                                      TString filename, int data_type ) {
  
  std::vector<TH1F*> * null_vmmB = new std::vector<TH1F*>;
  null_vmmB->resize(0);
  std::vector<TH1F*> * null_vmmC = new std::vector<TH1F*>;
  null_vmmC->resize(0);

  plotPadHits( h_pad_vmmB, h_pad_vmmC,
	       null_vmmB,  null_vmmC,
	       layer,
	       n_triggers, 0, 
	       kek_photon_rate, attenuation, voltage,
	       filename, false, data_type );

}

void decoded_event_tree::plotPadHits( TH1F * h_pad_vmmB, TH1F *h_pad_vmmC, 
				      std::vector<TH1F*> * h_pad_vmmB_eff_vec, 
				      std::vector<TH1F*> * h_pad_vmmC_eff_vec,
				      int layer, 
				      int n_triggers, int n_triggers_TP,
				      float kek_photon_rate, float attenuation, int voltage,
				      TString filename, bool isTP, int data_type ) {

  //  gStyle->SetPalette(kTemperatureMap);
  gStyle->SetPalette(55); //rainbow

  //  c1->cd();

  c1->SetLogy(0);
  c1->SetGridy(1);

  gStyle->SetOptStat(0);

  //--------------------------------------------------------------//
  //                   Check Histogram Vector Size
  //--------------------------------------------------------------//

  if ( isTP ) {
    if ( h_pad_vmmB_eff_vec->size() < 64 || h_pad_vmmC_eff_vec->size() < 64) {
      std::cout << "Error:  efficiency histogram vector size is less than 64: Aborting Efficiency plotting.  Only plotting photon rate" << std::endl;

      plotPadHits( h_pad_vmmB, h_pad_vmmC,
		   h_pad_vmmB_eff_vec,
		   h_pad_vmmC_eff_vec,
		   layer,
		   n_triggers, 0,  
		   kek_photon_rate, attenuation, voltage,
		   filename,   false, data_type );
    }
  }

  //--------------------------------------------------------------//
  //                    Plot Per Channel Hits
  //--------------------------------------------------------------//

  if ( h_pad_vmmB ) {
    
    //------------------------------------------------------------//
    //                   Draw the Photon Rate
    //------------------------------------------------------------//
    
    h_pad_vmmB->Scale(1.0/n_triggers/(200e-6)); // convert to kHz
    
    h_pad_vmmB->SetXTitle("VMM Channel Number");
    h_pad_vmmB->SetYTitle("Hit Rate (kHz)");
    h_pad_vmmB->SetMinimum(0.0);
    h_pad_vmmB->Draw("hist");

    Tl->DrawLatex(0.0, h_pad_vmmB->GetMaximum()*0.1, filename);
    c1->Update();
    ps->NewPage();

    //-----------------------------------------------------------//
    //      Get Photon Rate and Efficiency for Every Channel
    //-----------------------------------------------------------//

    for ( int ich=0; ich<64; ich++ ) {
      
      float hit_rate   = h_pad_vmmB->GetBinContent( ich+1 ); // bin = chan# + 1
      
      float efficiency = -1.0;
      if ( isTP ) efficiency = GetChanEfficiency( h_pad_vmmB_eff_vec->at(ich), n_triggers_TP, hit_rate );
      
      if ( attenuation >= 100000 ) {
	GetChannelPlots( layer, true, 1, voltage, data_type )->at(ich)->AddNoisePoint( hit_rate );
      }
      
      else {

	if ( isTP ) {
	  GetChannelPlots( layer, true, 1, voltage, data_type )->at(ich)->AddPoint( kek_photon_rate, hit_rate, efficiency );
	}
	else {
	  GetChannelPlots( layer, true, 1, voltage, data_type )->at(ich)->AddPoint( kek_photon_rate, hit_rate );
	}

      }
    }

  }

  //--------------------------------------------------------------//

  if ( h_pad_vmmC ) {

    h_pad_vmmC->Scale(1.0/n_triggers/(200e-6)); // convert to kHz    
    h_pad_vmmC->SetXTitle("VMM Channel Number");
    h_pad_vmmC->SetYTitle("Hit Rate (kHz)");

    h_pad_vmmC->SetMinimum(0.0);
    h_pad_vmmC->Draw("hist");

    Tl->DrawLatex(0.0, h_pad_vmmC->GetMaximum()*0.1, filename);

    c1->Update();
    ps->NewPage();

    //-----------------------------------------------------------//
    //      Get Photon Rate and Efficiency for Every Channel       
    //-----------------------------------------------------------//

    for ( int ich=0; ich<64; ich++ ) {

      float hit_rate   = h_pad_vmmC->GetBinContent( ich+1 ); // bin = chan# + 1 

      float efficiency = -1.0;
      if ( isTP ) efficiency = GetChanEfficiency( h_pad_vmmC_eff_vec->at(ich), n_triggers_TP, hit_rate );

      if ( attenuation >= 100000 ) GetChannelPlots( layer, true, 2, voltage, data_type )->at(ich)->AddNoisePoint( hit_rate );
      else {

        if ( isTP ) {
          GetChannelPlots( layer, true, 2, voltage, data_type )->at(ich)->AddPoint( kek_photon_rate, hit_rate, efficiency );
        }
        else {
          GetChannelPlots( layer, true, 2, voltage, data_type )->at(ich)->AddPoint( kek_photon_rate, hit_rate );
        }

      }
    }

  }

  //--------------------------------------------------------------//
  //                   Plot Mapped Pad Hits
  //--------------------------------------------------------------//

  c1->SetLogy(0);
  c1->SetGridy(0);
  
  if ( layer == 3 && h_pad_vmmB && h_pad_vmmC ) {

    m_sx.str("");
    m_sx << "h_layer3_pad_mapped_hits" << nhists;
    nhists++;

    TH2F *h_layer3_pad_mapped_hits = new TH2F(m_sx.str().c_str(), "Hit Rate on Layer 3 Pads", 7, -0.5, 6.5, 16, -0.5, 15.5);

    std::vector<int> vec_hits_vmmB;
    vec_hits_vmmB.resize(0);

    for ( int ibin=1; ibin<h_pad_vmmB->GetNbinsX()+1; ibin++ ) {      
      vec_hits_vmmB.push_back( h_pad_vmmB->GetBinContent( ibin ) );
    }    

    std::vector<int> vec_hits_vmmC;
    vec_hits_vmmC.resize(0);

    for ( int ibin=1; ibin<h_pad_vmmC->GetNbinsX()+1; ibin++ ) {
      vec_hits_vmmC.push_back( h_pad_vmmC->GetBinContent( ibin ) );
    }

    for ( int ich=0; ich<=55; ich++ ) {
      int ibinx = ich%7+1;
      int ibiny = 8-(ich/7);

      h_layer3_pad_mapped_hits->SetBinContent( ibinx, ibiny, vec_hits_vmmC.at(ich) );
    }

    for ( int ich=63; ich>=8; ich-- ) {
      int ibinx = (ich-8)%7+1;
      int ibiny = 16-(ich-8)/7;

      h_layer3_pad_mapped_hits->SetBinContent( ibinx, ibiny, vec_hits_vmmB.at(ich) );
    }

    h_layer3_pad_mapped_hits->SetXTitle("Horizontal");
    h_layer3_pad_mapped_hits->SetYTitle("Vertical");

    h_layer3_pad_mapped_hits->Draw("COLZ");

    Tl->DrawLatex(0.0, 0.0, filename);

    c1->Update();
    ps->NewPage();

  }

  if ( layer == 4 && h_pad_vmmB && h_pad_vmmC ) {

    m_sx.str("");
    m_sx << "h_layer4_pad_mapped_hits" << nhists;
    nhists++;

    TH2F *h_layer4_pad_mapped_hits = new TH2F(m_sx.str().c_str(), "Hit Rate on Layer 4 Pads", 7, -0.5, 6.5, 16, -0.5, 15.5);

    std::vector<int> vec_hits_vmmB;
    vec_hits_vmmB.resize(0);

    for ( int ibin=1; ibin<h_pad_vmmB->GetNbinsX()+1; ibin++ ) {
      vec_hits_vmmB.push_back( h_pad_vmmB->GetBinContent( ibin ) );
    }

    std::vector<int> vec_hits_vmmC;
    vec_hits_vmmC.resize(0);

    for ( int ibin=1; ibin<h_pad_vmmC->GetNbinsX()+1; ibin++ ) {
      vec_hits_vmmC.push_back( h_pad_vmmC->GetBinContent( ibin ) );
    }

    for ( int ich=8; ich<=63; ich++ ) {
      int ibinx = (ich-8)%7+1;
      int ibiny = (ich-8)/7+1;

      h_layer4_pad_mapped_hits->SetBinContent( ibinx, ibiny, vec_hits_vmmB.at(ich) );
    }

    for ( int ich=55; ich>=0; ich-- ) {
      int ibinx = ich%7+1;
      int ibiny = 16-(ich/7);

      h_layer4_pad_mapped_hits->SetBinContent( ibinx, ibiny, vec_hits_vmmC.at(ich) );
    }

    h_layer4_pad_mapped_hits->SetXTitle("Bottom Side");
    h_layer4_pad_mapped_hits->SetYTitle("Right Side");

    h_layer4_pad_mapped_hits->Draw("COLZ");

    Tl->DrawLatex(0.0, 0.0, filename);
    
    c1->Update();
    ps->NewPage();

  }

}

void decoded_event_tree::Loop(TString filename, double attenuation, double KEK_photon_rate, 
			      int voltage, bool is25ns, bool loopEvt, int data_type, int detector_type)
{

  Loop(filename,  "", attenuation, KEK_photon_rate,
       voltage,   is25ns,      loopEvt,
       data_type, false, detector_type);

}

void decoded_event_tree::Loop(TString filename, TString filename_TP, double attenuation, double KEK_photon_rate, 
			      int voltage, bool is25ns, bool loopEvt, int data_type, bool isTP, int detector_type)
{
  //   In a ROOT session, you can do:
  //      root> .L decoded_event_tree.C
  //      root> decoded_event_tree t
  //      root> t.GetEntry(12); // Fill t data members with entry number 12
  //      root> t.Show();       // Show values of entry 12
  //      root> t.Show(16);     // Read and show values of entry 16
  //      root> t.Loop();       // Loop on all entries
  //
  //--------------------------------------------------------------------//
  //    by  b_branchname->GetEntry(ientry); //read only this branch
  //--------------------------------------------------------------------//
  
  //--------------------------------------------------------------------//
  //                       Load Tree from File
  //--------------------------------------------------------------------//
  
  std::cout << "loading tree from file" << filename.Data() << std::endl;
  
  TFile * file = new TFile(filename.Data());
  
  TFile * file_TP;
  if (isTP) file_TP = new TFile(filename_TP.Data());

  if ( !file ) {
    std::cout << "Error: File " << filename.Data() << " does not exist, aborting loop " << std::endl;
    return;
  }

  if ( isTP && !file_TP ) {
    std::cout << "Error: File " << filename_TP.Data() << " does not exist, only run over background rate file " << std::endl;
    Loop(filename,  attenuation, KEK_photon_rate, 
	 voltage,   is25ns,      loopEvt, 
	 data_type, detector_type);
    return;
  }

  //--------------------------------------------------------------------//
  //                      
  //--------------------------------------------------------------------//

  TTree * tree = (TTree*) file->Get("decoded_event_tree");
  
  if ( loopEvt ) Init(tree);
  
  if ( max_kek_rate < KEK_photon_rate ) max_kek_rate = KEK_photon_rate;
  
  //---------------------------------------------------------------------//
  //                  Hit Counters for pFEB Channels
  //---------------------------------------------------------------------//
  
  //   tree->Draw("m_null_elink_id>>h_null_elink_id", "m_null_elink_id == 25", "goff");
  TH1F *h_null = (TH1F*) file->Get("h_nhits");
  TH1F *h_null_TP;

  if ( isTP ) h_null_TP = (TH1F*) file_TP->Get("h_nhits");

  double n_triggers = h_null->GetEntries();
  double n_triggers_TP;

  if ( isTP ) n_triggers_TP = h_null_TP->GetEntries();

  std::cout << "number of triggers " << n_triggers << std::endl;
  std::cout << "KEK photon rate " << KEK_photon_rate << std::endl;
  
  //---------------------------------------------------------------------//
  //              For Background Only Runs Plot Background
  //---------------------------------------------------------------------//

  TH1F * h_layer3_pad_vmmB_hits;
  TH1F * h_layer3_pad_vmmC_hits;
  TH1F * h_layer4_pad_vmmB_hits;
  TH1F * h_layer4_pad_vmmC_hits;
  
  // For QL1
  if ( detector_type == 1 ) {
    h_layer3_pad_vmmB_hits = (TH1F*) file->Get("elink_23_vmm_1_channel_hits");
    h_layer3_pad_vmmC_hits = (TH1F*) file->Get("elink_20_vmm_2_channel_hits");
    h_layer4_pad_vmmB_hits = (TH1F*) file->Get("elink_19_vmm_1_channel_hits");
    h_layer4_pad_vmmC_hits = (TH1F*) file->Get("elink_16_vmm_2_channel_hits");
  }

  // For QS2 (Just swapped)
  if ( detector_type == 2 ) {
    h_layer4_pad_vmmB_hits = (TH1F*) file->Get("elink_23_vmm_1_channel_hits");
    h_layer4_pad_vmmC_hits = (TH1F*) file->Get("elink_20_vmm_2_channel_hits");
    h_layer3_pad_vmmB_hits = (TH1F*) file->Get("elink_19_vmm_1_channel_hits");
    h_layer3_pad_vmmC_hits = (TH1F*) file->Get("elink_16_vmm_2_channel_hits");
  }

  TH1F * h_layer3_strip_vmm0_hits;
  TH1F * h_layer3_strip_vmm1_hits;
  TH1F * h_layer3_strip_vmm2_hits;
  TH1F * h_layer3_strip_vmm3_hits;
  TH1F * h_layer3_strip_vmm4_hits;
  TH1F * h_layer3_strip_vmm5_hits;
  TH1F * h_layer3_strip_vmm6_hits;
  TH1F * h_layer3_strip_vmm7_hits;

  TH1F * h_layer4_strip_vmm0_hits;
  TH1F * h_layer4_strip_vmm1_hits;
  TH1F * h_layer4_strip_vmm2_hits;
  TH1F * h_layer4_strip_vmm3_hits;
  TH1F * h_layer4_strip_vmm4_hits;
  TH1F * h_layer4_strip_vmm5_hits;
  TH1F * h_layer4_strip_vmm6_hits;
  TH1F * h_layer4_strip_vmm7_hits;
  
  // For QL1
  if ( detector_type == 1 ) {
    h_layer3_strip_vmm0_hits = (TH1F*) file->Get("elink_10_vmm_0_channel_hits");
    h_layer3_strip_vmm1_hits = (TH1F*) file->Get("elink_10_vmm_1_channel_hits");
    h_layer3_strip_vmm2_hits = (TH1F*) file->Get("elink_8_vmm_2_channel_hits");
    h_layer3_strip_vmm3_hits = (TH1F*) file->Get("elink_8_vmm_3_channel_hits");
    h_layer3_strip_vmm4_hits = (TH1F*) file->Get("elink_9_vmm_4_channel_hits");
    h_layer3_strip_vmm5_hits = (TH1F*) file->Get("elink_9_vmm_5_channel_hits");
    h_layer3_strip_vmm6_hits = (TH1F*) file->Get("elink_11_vmm_6_channel_hits");
    h_layer3_strip_vmm7_hits = (TH1F*) file->Get("elink_11_vmm_7_channel_hits");
  
    h_layer4_strip_vmm0_hits = (TH1F*) file->Get("elink_25_vmm_0_channel_hits");
    h_layer4_strip_vmm1_hits = (TH1F*) file->Get("elink_25_vmm_1_channel_hits");
    h_layer4_strip_vmm2_hits = (TH1F*) file->Get("elink_24_vmm_2_channel_hits");
    h_layer4_strip_vmm3_hits = (TH1F*) file->Get("elink_24_vmm_3_channel_hits");
    h_layer4_strip_vmm4_hits = (TH1F*) file->Get("elink_25_vmm_4_channel_hits");
    h_layer4_strip_vmm5_hits = (TH1F*) file->Get("elink_27_vmm_5_channel_hits");
    h_layer4_strip_vmm6_hits = (TH1F*) file->Get("elink_27_vmm_6_channel_hits");
    h_layer4_strip_vmm7_hits = (TH1F*) file->Get("elink_27_vmm_7_channel_hits");
  }

  // For QS2 (just swapped)
  if ( detector_type == 2 ) {
    h_layer4_strip_vmm0_hits = (TH1F*) file->Get("elink_10_vmm_0_channel_hits");
    h_layer4_strip_vmm1_hits = (TH1F*) file->Get("elink_10_vmm_1_channel_hits");
    h_layer4_strip_vmm2_hits = (TH1F*) file->Get("elink_8_vmm_2_channel_hits");
    h_layer4_strip_vmm3_hits = (TH1F*) file->Get("elink_8_vmm_3_channel_hits");
    h_layer4_strip_vmm4_hits = (TH1F*) file->Get("elink_9_vmm_4_channel_hits");
    h_layer4_strip_vmm5_hits = (TH1F*) file->Get("elink_9_vmm_5_channel_hits");
    h_layer4_strip_vmm6_hits = (TH1F*) file->Get("elink_11_vmm_6_channel_hits");
    h_layer4_strip_vmm7_hits = (TH1F*) file->Get("elink_11_vmm_7_channel_hits");
  
    h_layer3_strip_vmm0_hits = (TH1F*) file->Get("elink_25_vmm_0_channel_hits");
    h_layer3_strip_vmm1_hits = (TH1F*) file->Get("elink_25_vmm_1_channel_hits");
    h_layer3_strip_vmm2_hits = (TH1F*) file->Get("elink_24_vmm_2_channel_hits");
    h_layer3_strip_vmm3_hits = (TH1F*) file->Get("elink_24_vmm_3_channel_hits");
    h_layer3_strip_vmm4_hits = (TH1F*) file->Get("elink_25_vmm_4_channel_hits");
    h_layer3_strip_vmm5_hits = (TH1F*) file->Get("elink_27_vmm_5_channel_hits");
    h_layer3_strip_vmm6_hits = (TH1F*) file->Get("elink_27_vmm_6_channel_hits");
    h_layer3_strip_vmm7_hits = (TH1F*) file->Get("elink_27_vmm_7_channel_hits");
  }
  
  std::vector<TH1F*> * h_layer3_pad_vmmB_efficiencies = new std::vector<TH1F*>;
  std::vector<TH1F*> * h_layer3_pad_vmmC_efficiencies = new std::vector<TH1F*>;
  std::vector<TH1F*> * h_layer4_pad_vmmB_efficiencies = new std::vector<TH1F*>;
  std::vector<TH1F*> * h_layer4_pad_vmmC_efficiencies = new std::vector<TH1F*>;
  std::vector<TH1F*> * h_layer3_strip_vmm4_efficiencies = new std::vector<TH1F*>;
  std::vector<TH1F*> * h_layer4_strip_vmm3_efficiencies = new std::vector<TH1F*>;

  //For QS2
  std::vector<TH1F*> * h_layer3_strip_vmm3_efficiencies = new std::vector<TH1F*>;
  std::vector<TH1F*> * h_layer4_strip_vmm4_efficiencies = new std::vector<TH1F*>;
  
  if ( h_layer3_pad_vmmB_hits ) {
    h_layer3_pad_vmmB_hits->SetTitle("Layer 3 Pad Hit Rate (VMM B)");
  }
  
  if ( h_layer3_pad_vmmC_hits ) {
    h_layer3_pad_vmmC_hits->SetTitle("Layer 3 Pad Hit Rate (VMM C)");
  }
  
  if ( h_layer4_pad_vmmB_hits ) {
    h_layer4_pad_vmmB_hits->SetTitle("Layer 4 Pad Hit Rate (VMM B)");
  }
  
  if ( h_layer4_pad_vmmC_hits ) {
    h_layer4_pad_vmmC_hits->SetTitle("Layer 4 Pad Hit Rate (VMM C)");
  }
  
  if ( h_layer3_strip_vmm4_hits ) {
    h_layer3_strip_vmm4_hits->SetTitle("Layer 3 Strip Hit Rate (VMM 4)");
  }
  
  if ( h_layer4_strip_vmm3_hits ) {
    h_layer4_strip_vmm3_hits->SetTitle("Layer 4 Strip Hit Rate (VMM 3)");
  }

  //For QS2

  if ( h_layer3_strip_vmm3_hits ) {
    h_layer3_strip_vmm3_hits->SetTitle("Layer 3 Strip Hit Rate (VMM 3)");
  }
  
  if ( h_layer4_strip_vmm4_hits ) {
    h_layer4_strip_vmm4_hits->SetTitle("Layer 4 Strip Hit Rate (VMM 4)");
  }
  
  //---------------------------------------------------------------------//
  //                Get Efficiencies from Test Pulse Runs
  //---------------------------------------------------------------------//

  if ( isTP ) {

    std::vector<int> layers;
    std::vector<int> elinks;
    std::vector<int> vmms;
    std::vector<int> ispads;

    //For QL1
    if ( detector_type == 1 ) {
      layers = {3,  3,  4,  4,  3,  4 };
      elinks = {23, 20, 19, 16, 9,  24};
      vmms   = {1,  2,  1,  2,  4,  3 };
      ispads = {1,  1,  1,  1,  0,  0 };
    }

    //For QS2
    if ( detector_type == 2 ) {
      layers = {3,  3,  4,  4,  3,  4 };
      elinks = {19, 16, 23, 20, 24, 9};
      vmms   = {1,  2,  1,  2,  3,  4 };
      ispads = {1,  1,  1,  1,  0,  0 };
    }
    
    h_layer3_pad_vmmB_efficiencies->resize(0);
    h_layer3_pad_vmmC_efficiencies->resize(0);
    h_layer4_pad_vmmB_efficiencies->resize(0);
    h_layer4_pad_vmmC_efficiencies->resize(0);
    //Commented out for QL1
    //h_layer3_strip_vmm4_efficiencies->resize(0);
    //h_layer4_strip_vmm3_efficiencies->resize(0);

    //For QS2
    h_layer3_strip_vmm3_efficiencies->resize(0);
    h_layer4_strip_vmm4_efficiencies->resize(0);
    
    for ( uint iv = 0; iv < vmms.size(); iv++ ){

      int ilayer = layers.at(iv);
      int ielink = elinks.at(iv);
      int ivmm   = vmms.at(iv);
      bool ispad = ispads.at(iv);

      for ( int ich=0; ich<64; ich++ ){

	m_sx.str("");
	m_sx << "elink_" << ielink << "_vmm_" << ivmm << "_chan_" << ich << "_diff_bcid";
	
	TH1F * h_rel_bcid = (TH1F*) file_TP->Get(m_sx.str().c_str());
	
	if ( ilayer == 3 && ivmm == 1 &&  ispad ) h_layer3_pad_vmmB_efficiencies->push_back(   h_rel_bcid );
	if ( ilayer == 3 && ivmm == 2 &&  ispad ) h_layer3_pad_vmmC_efficiencies->push_back(   h_rel_bcid );
	if ( ilayer == 4 && ivmm == 1 &&  ispad ) h_layer4_pad_vmmB_efficiencies->push_back(   h_rel_bcid );
	if ( ilayer == 4 && ivmm == 2 &&  ispad ) h_layer4_pad_vmmC_efficiencies->push_back(   h_rel_bcid );
	if ( ilayer == 3 && ivmm == 4 && !ispad ) h_layer3_strip_vmm4_efficiencies->push_back( h_rel_bcid );
	if ( ilayer == 4 && ivmm == 3 && !ispad ) h_layer4_strip_vmm3_efficiencies->push_back( h_rel_bcid );

	//For QS2
	if ( ilayer == 3 && ivmm == 3 && !ispad ) h_layer3_strip_vmm3_efficiencies->push_back( h_rel_bcid );
	if ( ilayer == 4 && ivmm == 4 && !ispad ) h_layer4_strip_vmm4_efficiencies->push_back( h_rel_bcid );

      }

    }

  }

  

  //---------------------------------------------------------------------//
  //                       Add Data to Plots
  //---------------------------------------------------------------------//

  //For QL1 
  if ( detector_type == 1 ) {
    if ( !isTP ) {
      plotStripHits( h_layer3_strip_vmm4_hits, 3, 4, n_triggers, KEK_photon_rate, attenuation, voltage, filename, data_type );
      plotStripHits( h_layer4_strip_vmm3_hits, 4, 3, n_triggers, KEK_photon_rate, attenuation, voltage, filename, data_type );
      plotPadHits( h_layer3_pad_vmmB_hits, h_layer3_pad_vmmC_hits, 3, n_triggers, KEK_photon_rate, attenuation, voltage, filename, data_type);
      plotPadHits( h_layer4_pad_vmmB_hits, h_layer4_pad_vmmC_hits, 4, n_triggers, KEK_photon_rate, attenuation, voltage, filename, data_type);
  }
    else {
      plotStripHits( h_layer3_strip_vmm4_hits, 
		     h_layer3_strip_vmm4_efficiencies, 
		     3, 4, 
		     n_triggers,      n_triggers_TP, 
		     KEK_photon_rate, attenuation, voltage,
		     filename,        isTP,         data_type );

      plotStripHits( h_layer4_strip_vmm3_hits,
		     h_layer4_strip_vmm3_efficiencies,
		     4, 3, 
		     n_triggers,      n_triggers_TP, 
		     KEK_photon_rate, attenuation, voltage,
		     filename,        isTP,          data_type );
      
      plotPadHits( h_layer3_pad_vmmB_hits,         h_layer3_pad_vmmC_hits,
		   h_layer3_pad_vmmB_efficiencies, h_layer3_pad_vmmC_efficiencies,
		   3, n_triggers, n_triggers_TP, 
		   KEK_photon_rate, attenuation, voltage,
		   filename, true, data_type);
      plotPadHits( h_layer4_pad_vmmB_hits,         h_layer4_pad_vmmC_hits, 
		   h_layer4_pad_vmmB_efficiencies, h_layer4_pad_vmmC_efficiencies,
		   4, n_triggers, n_triggers_TP, 
		   KEK_photon_rate, attenuation, voltage,
		   filename, true, data_type);
    }
  }

  //For QS2
  if ( detector_type == 2 ) {

    if ( !isTP ) {

      plotPadHits( h_layer3_pad_vmmB_hits, h_layer3_pad_vmmC_hits, 3, n_triggers, KEK_photon_rate, attenuation, voltage, filename, data_type);
      plotPadHits( h_layer4_pad_vmmB_hits, h_layer4_pad_vmmC_hits, 4, n_triggers, KEK_photon_rate, attenuation, voltage, filename, data_type);
      plotStripHits( h_layer3_strip_vmm3_hits, 3, 3, n_triggers, KEK_photon_rate, attenuation, voltage, filename, data_type );
      plotStripHits( h_layer4_strip_vmm4_hits, 4, 4, n_triggers, KEK_photon_rate, attenuation, voltage, filename, data_type );

  }

    else {

      plotStripHits( h_layer3_strip_vmm3_hits, 
		     h_layer3_strip_vmm3_efficiencies, 
		     3, 3, 
		     n_triggers,      n_triggers_TP, 
		     KEK_photon_rate, attenuation, voltage,
		     filename,        isTP,         data_type );

      plotStripHits( h_layer4_strip_vmm4_hits,
		     h_layer4_strip_vmm4_efficiencies,
		     4, 4, 
		     n_triggers,      n_triggers_TP, 
		     KEK_photon_rate, attenuation, voltage,
		     filename,        isTP,          data_type );
      
      plotPadHits( h_layer3_pad_vmmB_hits,         h_layer3_pad_vmmC_hits,
		   h_layer3_pad_vmmB_efficiencies, h_layer3_pad_vmmC_efficiencies,
		   3, n_triggers, n_triggers_TP, 
		   KEK_photon_rate, attenuation, voltage,
		   filename, true, data_type);
      plotPadHits( h_layer4_pad_vmmB_hits,         h_layer4_pad_vmmC_hits, 
		   h_layer4_pad_vmmB_efficiencies, h_layer4_pad_vmmC_efficiencies,
		   4, n_triggers, n_triggers_TP, 
		   KEK_photon_rate, attenuation, voltage,
		   filename, true, data_type);

    }
    
  }
  //---------------------------------------------------------------------//
  //                       Loop Over Tree
  //---------------------------------------------------------------------//
  
  if ( loopEvt ) {
    
    if (fChain == 0) return;
     
    Long64_t nentries = fChain->GetEntriesFast();
    
    Long64_t nbytes = 0, nb = 0;
    for (Long64_t jentry=0; jentry<nentries;jentry++) {
      
      //------------------------------------------------------------------//
      //                   Load Event From Tree
      //------------------------------------------------------------------//
      Long64_t ientry = LoadTree(jentry);
      if (ientry < 0) break;
      nb = fChain->GetEntry(jentry);   nbytes += nb;
      // if (Cut(ientry) < 0) continue;
      
      //-------------------------------------------------------------------//
      //                  Find NHits in Each Channel
       //-------------------------------------------------------------------//
      
    }
     
    //------------------------------------------//
  }

  file->Close();
  
}

void decoded_event_tree::AnalyzeAnalog(TString filename, double attenuation, double KEK_photon_rate, int voltage, bool is25ns,
				       int ilayer, int SCA_ID, int ivmm, int ich )
{

  TFile * file_ana = new TFile(filename.Data(), "READ");

  // add baseline in mV to plot
  double baseline = GetMedian( filename );
  GetAnalogChannelPlots( ilayer, true, ivmm, voltage )->at(ich)->AddBaselinePoint( KEK_photon_rate, baseline*1000 );

  //TGraph * h_waveform = (TGraph*) file_ana->Get("Graph");
  TH2F   * h_pulse_height_vs_ToT = ( TH2F* ) file_ana->Get("h_pulse_height_vs_ToT");

  int n_pulses = 0;

  if ( h_pulse_height_vs_ToT ) n_pulses = h_pulse_height_vs_ToT->Integral(60,1000,0,100);
  
  bool isPad = true;
  
  if ( SCA_ID == 993 ||
       SCA_ID == 1040 ) {
    isPad = true;
  }
  else if ( SCA_ID == 1001 ||
	    SCA_ID == 1026 ) {
    isPad = false;
  }
  else {
    std::cout << "Error: SCA ID not recognized" << std::endl;
  }

  std::cout << "analog rate " << n_pulses/1.6
            << " KEK_photon_rate " << KEK_photon_rate * GetAnalogChannelPlots( ilayer, isPad, ivmm, voltage )->at(ich)->area << std::endl;

  // 1.6 milisecond per waveform so npulses/1.6 = hit rate in kHz
  GetAnalogChannelPlots( ilayer, isPad, ivmm, voltage )->at(ich)->AddPoint( KEK_photon_rate, n_pulses/1.6 );

  file_ana->Close();

  //delete h_waveform;
  //  delete h_pulse_height_vs_ToT;
  //delete file_ana;

}

int decoded_event_tree::GetNPulse( TGraph * h_waveform, double threshold,
				   double attenuation, double KEK_photon_rate, int voltage, bool is25ns,
				   int ilayer, int SCA_ID, int ivmm, int ich) {

  int npoint = h_waveform->GetN();

  Double_t * time_vec = h_waveform->GetX();
  Double_t * volt_vec = h_waveform->GetY();

  //------------------------------------------//

  int total_num_pulses = 0;
  bool over_th_flag    = false;
  int n_time_tot       = 0;
  double max_pulse_amp = 0;

  //-----------------------------------------//
  
  m_sx.str("");
  m_sx << "pulse_height_vs_tot_"
       << "ly"  << ilayer << "_"
       << "vmm" << ivmm << "_"
       << "ich" << ich << "_"
       << "at"  << attenuation ;

  TH2F * h_pulse_height_vs_width = new TH2F( m_sx.str().c_str(), m_sx.str().c_str(), 100, 0, 1000, 170, 1300 ); 

  //-----------------------------------------//

  for ( int ip=0; ip<npoint; ip++ ) {
    
    // first time going over threshold
    if ( volt_vec[ip] > threshold && 
	 !over_th_flag ) {
      over_th_flag = true;
      n_time_tot = 1;
      max_pulse_amp = volt_vec[ip];
    }
    
    // if continue to be over threshold
    else if ( volt_vec[ip] > threshold && 
	      over_th_flag ){
      n_time_tot++;
      if ( max_pulse_amp < volt_vec[ip] ) max_pulse_amp = volt_vec[ip];
    }

    // if first time under threshold after over threhold
    else if ( volt_vec[ip] <= threshold &&
	      over_th_flag ) {
      // time in nanoseconds vs height in milivolts
      total_num_pulses += 1;
      h_pulse_height_vs_width->Fill( n_time_tot*0.1, max_pulse_amp*1000 );
      false;
    }

    // if continue to be under threshold
    else {
      over_th_flag = false;
    }

  }


}

std::vector<double> decoded_event_tree::FindHits(TH1F * h_hits, std::vector<int> channels) {


  std::vector<double> vec_hits;
  vec_hits.resize(0);

  if ( !h_hits ) return vec_hits;

  for ( int ichan=1; ichan < h_hits->GetNbinsX()+1; ichan++ ) {

    for ( uint i=0; i<channels.size(); i++ ) {
      if ( ichan-1 == channels.at(i) ) {
	vec_hits.push_back( h_hits->GetBinContent( ichan ) );
      }
    }
  }

  return vec_hits;
} 

void decoded_event_tree::PlotHistos( int data_type, int detector_type ) {

  gStyle->SetOptStat(0);

  //--------------------------------------------------------//

  ps->Close();

  if ( detector_type == 1 ) {
    gSystem->Exec("ps2pdf plotsQL1/plots.eps");
    gSystem->Exec("mv plots.pdf plotsQL1/plots.pdf");
  }

  if ( detector_type == 2 ) {
    gSystem->Exec("ps2pdf plotsQS2/plots.eps");
    gSystem->Exec("mv plots.pdf plotsQS2/plots.pdf");
  }

  //-------------------------------------------------------//

  plotChannelPlots( true, false, data_type, detector_type );
  plotChannelPlots( false, false, data_type, detector_type );
  plotChannelPlots( true, true, data_type, detector_type );
  plotChannelPlots( false, true, data_type, detector_type );
  //plotChannelComparePlots( data_type );

  std::cout << "finished plotting" << std::endl;

  return;

}

int decoded_event_tree::binarySearch(std::vector<double> sorted_vec, int l, int r, double x) {
  if (r >= l) {
    int mid = l + (r - l) / 2;

    if (sorted_vec.at(mid) == x)
      return mid;

    if (sorted_vec.at(mid) > x)
      return binarySearch(sorted_vec, l, mid - 1, x);

    return binarySearch(sorted_vec, mid + 1, r, x);
  }
  else {
    return r;
  }
}

std::vector<double> decoded_event_tree::sort_vector( std::vector<double> vec ) {

  std::vector<double> sorted_vec;
  if ( vec.size() == 0 ) return sorted_vec;

  sorted_vec.push_back( vec.at(0) );

  for ( int i=1; i<vec.size(); i++ ) {

    int sorted_loc = binarySearch( sorted_vec, 0, sorted_vec.size()-1, vec.at(i) );
    if ( sorted_loc < 0 ) sorted_loc = 0;
    auto it = sorted_vec.begin() + sorted_loc;

    if ( vec.at(i) > *it ) {
      sorted_vec.insert( it+1, vec.at(i) );
    }

    else {
      sorted_vec.insert( it, vec.at(i) );
    }

  }

  if ( true ) {

    int previous_i = -0xFFFF;
    bool sorted_wrong = false;

    std::cout << "sorted vector ";
    for ( int i=0; i<sorted_vec.size(); i++ ) {

      std::cout << sorted_vec.at(i) << " ";
      if ( sorted_vec.at(i) < previous_i ) {
        sorted_wrong = true;
      }
      previous_i = sorted_vec.at(i);
    }
    std::cout << std::endl;
    std::cout << "Vector Size " << sorted_vec.size() << std::endl;
    if ( sorted_wrong ) std::cout << "Error: sorting was incorrect" << std::endl;

  }

  return sorted_vec;

}
