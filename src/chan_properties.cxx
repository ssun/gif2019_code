#include<chan_properties.h>
#include<data_properties.h>

chan_properties::chan_properties() {
  verbose = false;
}

chan_properties::~chan_properties() {
}

int chan_properties::getPad_PiCap( int layer_number, int ivmm, int ch_number ) {
  
  TString VMM_letter;

  if ( ivmm == 1 ) VMM_letter = "B";
  else             VMM_letter = "C";

  return getPad_PiCap(layer_number, VMM_letter, ch_number);

}
int chan_properties::getPad_PiCap( int layer_number, TString vmm_letter, int ch_number ) {

  if ( vmm_letter.CompareTo("C") == 0 ) {

    if ( ch_number == 16 ) { return 150; }
    if ( ch_number == 17 ) { return 200; }
    if ( ch_number == 20 ) { return 300; }
    if ( ch_number == 21 ) { return 470; }

    if ( ch_number == 24 ) { return 150; }
    if ( ch_number == 25 ) { return 200; }
    if ( ch_number == 28 ) { return 300; }
    if ( ch_number == 29 ) { return 470; }

  }
  else if ( vmm_letter.CompareTo("B") == 0 ) {

    if ( ch_number == 32 ) { return 150; }
    if ( ch_number == 33 ) { return 200; }
    if ( ch_number == 36 ) { return 300; }
    if ( ch_number == 37 ) { return 470; }

    if ( ch_number == 40 ) { return 150; }
    if ( ch_number == 41 ) { return 200; }
    if ( ch_number == 44 ) { return 300; }
    if ( ch_number == 45 ) { return 470; }

    if ( ch_number == 48 ) { return 150; }
    if ( ch_number == 49 ) { return 200; }
    if ( ch_number == 52 ) { return 300; }
    if ( ch_number == 53 ) { return 470; }

    if ( ch_number == 56 ) { return 150; }
    if ( ch_number == 57 ) { return 200; }
    if ( ch_number == 60 ) { return 300; }
    if ( ch_number == 61 ) { return 470; }

  }

  return 100;

}

int chan_properties::getStrip_Resistance( int layer_number, int ivmm, int ich ) {

  if ( ( layer_number == 3 || layer_number == 4 ) && ivmm == 4 ) { // only VMM 4 is modified
    
    if ( ich == 0  || ich == 1  || ich == 4  || ich == 5 ) {
      return 1000;
    }
    if ( ich == 8  || ich == 9  || ich == 12 || ich == 13 ) {
      return 500;
    }
    if ( ich == 16 || ich == 17 || ich == 20 || ich == 21 ) {
      return 300;
    }
    if ( ich == 24 || ich == 25 || ich == 28 || ich == 29 ) {
      return 200;
    }
    if ( ich == 32 || ich == 33 || ich == 36 || ich == 37 ) {
      return 1000;
    }
    if ( ich == 40 || ich == 41 || ich == 44 || ich == 45 ) {
      return 500;
    }
    if ( ich == 48 || ich == 49 || ich == 52 || ich == 53 ) {
      return 300;
    }
    if ( ich == 56 || ich == 57 || ich == 60 || ich == 61 ) {
      return 200;
    }

  }

  return 200; // default is 200 kOhm
}

std::pair<int,int> chan_properties::getComparablePadChan( int layer_number, TString vmm_letter, int ch_number ) {

  int ivmm=-1;

  if ( vmm_letter.CompareTo("C") == 0 ) {
    ivmm = 2;
  }
  else if ( vmm_letter.CompareTo("B") == 0 ) {
    ivmm = 1;
  }

  return getComparablePadChan( layer_number, ivmm, ch_number );

}

std::pair<int,int> chan_properties::getComparablePadChan( int layer_number, int ivmm, int ch_number ) {

  int comparable_vmm = -1;
  int comparable_ch  = -1;
  int comparable_layer = -1;

  if ( layer_number == 3 ) {
    comparable_layer = 4;

    if ( ivmm == 1 ) {
      if ( ch_number >= 8 && 
	   ch_number <= 63 ) {
	comparable_vmm = 2;
	comparable_ch  = 55-(ch_number-8);
      }
    }
    if ( ivmm == 2 ) {
      if ( ch_number >= 0 &&
	   ch_number <= 55 ) {
        comparable_vmm = 1;
        comparable_ch  = 63-ch_number;
      }
    }    
  }

  if ( layer_number == 4 ) {
    comparable_layer = 3;

    if ( ivmm == 1 ) {
      if ( ch_number >= 8 &&
           ch_number <= 63 ) {
        comparable_vmm = 2;
        comparable_ch  = 55-(ch_number-8);
      }
    }
    if ( ivmm == 2 ) {
      if ( ch_number >= 0 &&
           ch_number <= 55 ) {
        comparable_vmm = 1;
        comparable_ch  = 63-ch_number;
      }
    }
  }

  if (verbose)  std::cout << " layer " << layer_number     
			  << " vmm "   << ivmm 
			  << " ch "    << ch_number << " compared with "
			  << " layer " << comparable_layer 
			  << " vmm "   << comparable_vmm 
			  << " ch "    << comparable_ch << std::endl;

  std::pair<int, int> comparable_vmm_ch = std::make_pair( comparable_vmm, comparable_ch );

  return comparable_vmm_ch;

}

std::pair<int,int> chan_properties::getComparableStripChan( int layer_number, int ivmm, int ch_number ) {
  
  int comparable_vmm = -1;
  int comparable_ch = -1;
  int comparable_layer = -1;

  if ( layer_number == 3 ) {
    comparable_layer = 4;

    if ( ivmm == 4 ) {
      comparable_vmm = 3;
      comparable_ch = 63-ch_number;
    }
  }

  if (verbose) std::cout << " layer " << layer_number
			 << " vmm "   << ivmm
			 << " ch "    << ch_number << " compared with "
			 << " layer " << comparable_layer
			 << " vmm "   << comparable_vmm
			 << " ch "    << comparable_ch << std:: endl;

  std::pair<int, int> comparable_vmm_ch = std::make_pair( comparable_vmm, comparable_ch );

  return comparable_vmm_ch;


}
    

double chan_properties::getPadArea( int layer_number, int ivmm, int ch_number, int detector_type ) {

  TString VMM_letter;

  if ( ivmm == 1 ) VMM_letter = "B";
  else             VMM_letter = "C";

  return getPadArea(layer_number, VMM_letter, ch_number, detector_type);

}

double chan_properties::getPadArea( int layer_number, TString vmm_letter, int ch_number, int detector_type ) {

  if ( detector_type == 1 ) {

    if ( layer_number == 3 ) {

      if ( vmm_letter.CompareTo("C") == 0 ) {

	if ( ch_number == 0 ) {  return  39.22; }
	if ( ch_number == 1 ) {  return 114.42; }
	if ( ch_number == 2 ) {  return 111.82; }
	if ( ch_number == 3 ) {  return 110.98; }
	if ( ch_number == 4 ) {  return 111.85; }
	if ( ch_number == 5 ) {  return 114.47; }
	if ( ch_number == 6 ) {  return  35.90; }
	if ( ch_number == 7 ) {  return  37.39; }
	if ( ch_number == 8 ) {  return 108.46; }
	if ( ch_number == 9 ) {  return 105.99; }
	if ( ch_number == 10 ) { return 105.20; }
	if ( ch_number == 11 ) { return 106.02; }
	if ( ch_number == 12 ) { return 108.51; }
	if ( ch_number == 13 ) { return  34.07; }
	if ( ch_number == 14 ) { return  35.56; }
	if ( ch_number == 15 ) { return 102.49; }
	if ( ch_number == 16 ) { return 100.16; }
	if ( ch_number == 17 ) { return  99.41; }
	if ( ch_number == 18 ) { return 100.19; }
	if ( ch_number == 19 ) { return 102.54; }
	if ( ch_number == 20 ) { return  32.23; }
	if ( ch_number == 21 ) { return  33.73; }
	if ( ch_number == 22 ) { return  96.52; }
	if ( ch_number == 23 ) { return  94.33; }
	if ( ch_number == 24 ) { return  93.62; }
	if ( ch_number == 25 ) { return  94.35; }
	if ( ch_number == 26 ) { return  96.57; }
	if ( ch_number == 27 ) { return  30.40; }
	if ( ch_number == 28 ) { return  31.90; }
	if ( ch_number == 29 ) { return  90.55; }
	if ( ch_number == 30 ) { return  88.50; }
	if ( ch_number == 31 ) { return  87.84; }
	if ( ch_number == 32 ) { return  88.52; }
	if ( ch_number == 33 ) { return  90.60; }
	if ( ch_number == 34 ) { return  28.57; }
	if ( ch_number == 35 ) { return  30.06; }
	if ( ch_number == 36 ) { return  84.59; }
	if ( ch_number == 37 ) { return  82.67; }
	if ( ch_number == 38 ) { return  82.05; }
	if ( ch_number == 39 ) { return  82.69; }
	if ( ch_number == 40 ) { return  84.64; }
	if ( ch_number == 41 ) { return  26.74; }
	if ( ch_number == 42 ) { return  28.23; }
	if ( ch_number == 43 ) { return  78.62; }
	if ( ch_number == 44 ) { return  76.83; }
	if ( ch_number == 45 ) { return  76.26; }
	if ( ch_number == 46 ) { return  76.86; }
	if ( ch_number == 47 ) { return  78.67; }
	if ( ch_number == 48 ) { return  24.91; }
	if ( ch_number == 49 ) { return  31.61; }
	if ( ch_number == 50 ) { return  86.86; }
	if ( ch_number == 51 ) { return  84.89; }
	if ( ch_number == 52 ) { return  84.25; }
	if ( ch_number == 53 ) { return  84.92; }
	if ( ch_number == 54 ) { return  86.92; }
	if ( ch_number == 55 ) { return  27.60; }

      }
      if ( vmm_letter.CompareTo("B") == 0 ) {

	if ( ch_number == 8 )  { return  41.26; }
	if ( ch_number == 9 )  { return 124.17; }
	if ( ch_number == 10 ) { return 121.35; }
	if ( ch_number == 11 ) { return 120.43; }
	if ( ch_number == 12 ) { return 121.37; }
	if ( ch_number == 13 ) { return 124.21; }
	if ( ch_number == 14 ) { return  38.71; }
	if ( ch_number == 15 ) { return  52.04; }
	if ( ch_number == 16 ) { return 156.20; }
	if ( ch_number == 17 ) { return 152.65; }
	if ( ch_number == 18 ) { return 151.50; }
	if ( ch_number == 19 ) { return 152.67; }
	if ( ch_number == 20 ) { return 156.25; }
	if ( ch_number == 21 ) { return  48.72; }
	if ( ch_number == 22 ) { return  50.21; }
	if ( ch_number == 23 ) { return 150.23; }
	if ( ch_number == 24 ) { return 146.81; };
	if ( ch_number == 25 ) { return 145.71; }
	if ( ch_number == 26 ) { return 146.84; }
	if ( ch_number == 27 ) { return 150.28; }
	if ( ch_number == 28 ) { return  46.89; }
	if ( ch_number == 29 ) { return  48.38; }
	if ( ch_number == 30 ) { return 144.26; }
	if ( ch_number == 31 ) { return 140.98; }
	if ( ch_number == 32 ) { return 139.92; }
	if ( ch_number == 33 ) { return 141.01; }
	if ( ch_number == 34 ) { return 144.31; }
	if ( ch_number == 35 ) { return  45.05; }
	if ( ch_number == 36 ) { return  46.55; }
	if ( ch_number == 37 ) { return 138.29; }
	if ( ch_number == 38 ) { return 135.15; }
	if ( ch_number == 39 ) { return 134.13; }
	if ( ch_number == 40 ) { return 135.18; }
	if ( ch_number == 41 ) { return 138.34; }
	if ( ch_number == 42 ) { return  43.22; }
	if ( ch_number == 43 ) { return  44.72; }
	if ( ch_number == 44 ) { return 132.33; }
	if ( ch_number == 45 ) { return 129.32; }
	if ( ch_number == 46 ) { return 128.35; }
	if ( ch_number == 47 ) { return 129.34; }
	if ( ch_number == 48 ) { return 132.38; }
	if ( ch_number == 49 ) { return  41.39; }
	if ( ch_number == 50 ) { return  42.88; }
	if ( ch_number == 51 ) { return 126.36; }
	if ( ch_number == 52 ) { return 123.49; }
	if ( ch_number == 53 ) { return 122.56; }
	if ( ch_number == 54 ) { return 123.51; }
	if ( ch_number == 55 ) { return 126.41; }
	if ( ch_number == 56 ) { return  39.56; }
	if ( ch_number == 57 ) { return  41.05; }
	if ( ch_number == 58 ) { return 120.39; }
	if ( ch_number == 59 ) { return 117.66; }
	if ( ch_number == 60 ) { return 116.77; }
	if ( ch_number == 61 ) { return 117.68; }
	if ( ch_number == 62 ) { return 120.44; }
	if ( ch_number == 63 ) { return  37.73; }

      }
    }
  
    if ( layer_number == 4 ) {
    
      if ( vmm_letter.CompareTo("C") == 0 ) {
      
	if ( ch_number == 55 ) { return  41.26; }
	if ( ch_number == 54 ) { return 124.17; }
	if ( ch_number == 53 ) { return 121.35; }
	if ( ch_number == 52 ) { return 120.43; }
	if ( ch_number == 51 ) { return 121.37; }
	if ( ch_number == 50 ) { return 124.21; }
	if ( ch_number == 49 ) { return  38.71; }
	if ( ch_number == 48 ) { return  52.04; }
	if ( ch_number == 47 ) { return 156.20; }
	if ( ch_number == 46 ) { return 152.65; }
	if ( ch_number == 45 ) { return 151.50; }
	if ( ch_number == 44 ) { return 152.67; }
	if ( ch_number == 43 ) { return 156.25; }
	if ( ch_number == 42 ) { return  48.72; }
	if ( ch_number == 41 ) { return  50.21; }
	if ( ch_number == 40 ) { return 150.23; }
	if ( ch_number == 39 ) { return 146.81; }
	if ( ch_number == 38 ) { return 145.71; }
	if ( ch_number == 37 ) { return 146.84; }
	if ( ch_number == 36 ) { return 150.28; }
	if ( ch_number == 35 ) { return  46.89; }
	if ( ch_number == 34 ) { return  48.38; }
	if ( ch_number == 33 ) { return 144.26; }
	if ( ch_number == 32 ) { return 140.98; }
	if ( ch_number == 31 ) { return 139.92; }
	if ( ch_number == 30 ) { return 141.01; }     
	if ( ch_number == 29 ) { return 144.31; }
	if ( ch_number == 28 ) { return  45.05; }
	if ( ch_number == 27 ) { return  46.55; }
	if ( ch_number == 26 ) { return 138.29; }
	if ( ch_number == 25 ) { return 135.15; }
	if ( ch_number == 24 ) { return 134.13; }
	if ( ch_number == 23 ) { return 135.18; }
	if ( ch_number == 22 ) { return 138.34; }
	if ( ch_number == 21 ) { return  43.22; }
	if ( ch_number == 20 ) { return  44.72; }
	if ( ch_number == 19 ) { return 132.33; }
	if ( ch_number == 18 ) { return 129.32; }
	if ( ch_number == 17 ) { return 128.35; }
	if ( ch_number == 16 ) { return 129.34; }
	if ( ch_number == 15 ) { return 132.38; }
	if ( ch_number == 14 ) { return  41.39; }
	if ( ch_number == 13 ) { return  42.88; }
	if ( ch_number == 12 ) { return 126.36; }
	if ( ch_number == 11 ) { return 123.49; }
	if ( ch_number == 10 ) { return 122.56; }
	if ( ch_number == 9 )  { return 123.51; }
	if ( ch_number == 8 )  { return 126.41; }
	if ( ch_number == 7 )  { return  39.56; }
	if ( ch_number == 6 )  { return  41.05; }
	if ( ch_number == 5 )  { return 120.39; }
	if ( ch_number == 4 )  { return 117.66; }
	if ( ch_number == 3 )  { return 116.77; }
	if ( ch_number == 2 )  { return 117.68; }
	if ( ch_number == 1 )  { return 120.44; }
	if ( ch_number == 0 )  { return  37.73; }

      }
      if ( vmm_letter.CompareTo("B") == 0 ) {

	if ( ch_number == 63 ) { return  39.22; }      
	if ( ch_number == 62 ) { return 114.42; }
	if ( ch_number == 61 ) { return 111.82; }
	if ( ch_number == 60 ) { return 110.98; }
	if ( ch_number == 59 ) { return 111.85; }
	if ( ch_number == 58 ) { return 114.47; }
	if ( ch_number == 57 ) { return  35.90; }
	if ( ch_number == 56 ) { return  37.39; }
	if ( ch_number == 55 ) { return 108.46; }
	if ( ch_number == 54 ) { return 105.99; }
	if ( ch_number == 53 ) { return 105.20; }
	if ( ch_number == 52 ) { return 106.02; }
	if ( ch_number == 51 ) { return 108.51; }
	if ( ch_number == 50 ) { return  34.07; }
	if ( ch_number == 49 ) { return  35.56; }
	if ( ch_number == 48 ) { return 102.49; }
	if ( ch_number == 47 ) { return 100.16; }
	if ( ch_number == 46 ) { return  99.41; }
	if ( ch_number == 45 ) { return 100.19; }
	if ( ch_number == 44 ) { return 102.54; }
	if ( ch_number == 43 ) { return  32.23; }
	if ( ch_number == 42 ) { return  33.73; }
	if ( ch_number == 41 ) { return  96.52; }
	if ( ch_number == 40 ) { return  94.33; }
	if ( ch_number == 39 ) { return  93.62; }
	if ( ch_number == 38 ) { return  94.35; }
	if ( ch_number == 37 ) { return  96.57; }
	if ( ch_number == 36 ) { return  30.40; }
	if ( ch_number == 35 ) { return  31.90; }
	if ( ch_number == 34 ) { return  90.55; }
	if ( ch_number == 33 ) { return  88.50; }
	if ( ch_number == 32 ) { return  87.84; }
	if ( ch_number == 31 ) { return  88.52; }
	if ( ch_number == 30 ) { return  90.60; }
	if ( ch_number == 29 ) { return  28.57; }
	if ( ch_number == 28 ) { return  30.06; }
	if ( ch_number == 27 ) { return  84.59; }
	if ( ch_number == 26 ) { return  82.67; }
	if ( ch_number == 25 ) { return  82.05; }
	if ( ch_number == 24 ) { return  82.69; }
	if ( ch_number == 23 ) { return  84.64; }
	if ( ch_number == 22 ) { return  26.74; }
	if ( ch_number == 21 ) { return  28.23; }
	if ( ch_number == 20 ) { return  78.62; }
	if ( ch_number == 19 ) { return  76.83; }
	if ( ch_number == 18 ) { return  76.26; }
	if ( ch_number == 17 ) { return  76.86; }
	if ( ch_number == 16 ) { return  78.67; }
	if ( ch_number == 15 ) { return  24.91; }
	if ( ch_number == 14 ) { return  31.61; }
	if ( ch_number == 13 ) { return  86.86; }
	if ( ch_number == 12 ) { return  84.89; }
	if ( ch_number == 11 ) { return  84.25; }
	if ( ch_number == 10 ) { return  84.92; }
	if ( ch_number == 9 )  { return  86.92; }
	if ( ch_number == 8 )  { return  27.60; }

      }

    }
  }

  if ( detector_type == 2 ) {

    if ( layer_number == 3 ) {

      if ( vmm_letter.CompareTo("C") == 0 ) {

	if ( ch_number == 0 ) { return 295.26; }
	if ( ch_number == 1 ) { return 189.87; }
	if ( ch_number == 2 ) { return 187.70; }
	if ( ch_number == 3 ) { return 286.87; }
	if ( ch_number == 4 ) { return 184.50; }
	if ( ch_number == 5 ) { return 182.33; }
	if ( ch_number == 6 ) { return 278.48; }
	if ( ch_number == 7 ) { return 179.13; }
	if ( ch_number == 8 ) { return 176.96; }
	if ( ch_number == 9 ) { return 270.09; }
	if ( ch_number == 10 ) { return 173.77; }
	if ( ch_number == 11 ) { return 171.59; }
	if ( ch_number == 12 ) { return 261.71; }
	if ( ch_number == 13 ) { return 168.40; }
	if ( ch_number == 14 ) { return 166.23; }
	if ( ch_number == 15 ) { return 253.32; }
	if ( ch_number == 16 ) { return 163.03; }
	if ( ch_number == 17 ) { return 160.86; }
	if ( ch_number == 18 ) { return 244.93; }
	if ( ch_number == 19 ) { return 157.66; }
	if ( ch_number == 20 ) { return 117.39; }
	if ( ch_number == 21 ) { return 178.61; }
	if ( ch_number == 22 ) { return 114.99; }
	
      }

      if ( vmm_letter.CompareTo("B") == 0 ) {

	if ( ch_number == 42 ) { return 181.29; }
	if ( ch_number == 43 ) { return 278.21; }
	if ( ch_number == 44 ) { return 178.77; }
	if ( ch_number == 45 ) { return 225.28; }
	if ( ch_number == 46 ) { return 345.58; }
	if ( ch_number == 47 ) { return 222.08; }
	if ( ch_number == 48 ) { return 219.91; }
	if ( ch_number == 49 ) { return 337.19; }
	if ( ch_number == 50 ) { return 216.72; }
	if ( ch_number == 51 ) { return 214.54; }
	if ( ch_number == 52 ) { return 328.81; }
	if ( ch_number == 53 ) { return 211.35; }
	if ( ch_number == 54 ) { return 209.17; }
	if ( ch_number == 55 ) { return 320.42; }
	if ( ch_number == 56 ) { return 205.98; }
	if ( ch_number == 57 ) { return 203.81; }
	if ( ch_number == 58 ) { return 312.03; }
	if ( ch_number == 59 ) { return 200.61; }
	if ( ch_number == 60 ) { return 198.44; }
	if ( ch_number == 61 ) { return 303.64; }
	if ( ch_number == 62 ) { return 195.24; }
	if ( ch_number == 63 ) { return 193.07; }
	
      }
      
    }

    if ( layer_number == 4 ) {

      if ( vmm_letter.CompareTo("C") == 0 ) {

	if ( ch_number == 0 ) { return 193.07; }
	if ( ch_number == 1 ) { return 195.24; }
	if ( ch_number == 2 ) { return 303.64; }
	if ( ch_number == 3 ) { return 198.44; }
	if ( ch_number == 4 ) { return 200.61; }
	if ( ch_number == 5 ) { return 312.03; }
	if ( ch_number == 6 ) { return 203.81; }
	if ( ch_number == 7 ) { return 205.98; }
	if ( ch_number == 8 ) { return 320.42; }
	if ( ch_number == 9 ) { return 209.17; }
	if ( ch_number == 10 ) { return 211.35; }
	if ( ch_number == 11 ) { return 328.81; }
	if ( ch_number == 12 ) { return 214.54; }
	if ( ch_number == 13 ) { return 216.72; }
	if ( ch_number == 14 ) { return 337.19; }
	if ( ch_number == 15 ) { return 219.91; }
	if ( ch_number == 16 ) { return 222.08; }
	if ( ch_number == 17 ) { return 345.58; }
	if ( ch_number == 18 ) { return 225.28; }
	if ( ch_number == 19 ) { return 178.77; }
	if ( ch_number == 20 ) { return 278.21; }
	if ( ch_number == 21 ) { return 181.29; }
	
      }

      if ( vmm_letter.CompareTo("B") == 0 ) {

	if ( ch_number == 41 ) { return 114.99;}
	if ( ch_number == 42 ) { return 178.61; }
	if ( ch_number == 43 ) { return 117.39; }
	if ( ch_number == 44 ) { return 157.66; }
	if ( ch_number == 45 ) { return 244.93; }
	if ( ch_number == 46 ) { return 160.86; }
	if ( ch_number == 47 ) { return 163.03; }
	if ( ch_number == 48 ) { return 253.32; }
	if ( ch_number == 49 ) { return 166.23; }
	if ( ch_number == 50 ) { return 168.40; }
	if ( ch_number == 51 ) { return 261.71; }
	if ( ch_number == 52 ) { return 171.59; }
	if ( ch_number == 53 ) { return 173.77; }
	if ( ch_number == 54 ) { return 270.09; }
	if ( ch_number == 55 ) { return 176.96; }
	if ( ch_number == 56 ) { return 179.13; }
	if ( ch_number == 57 ) { return 278.48; }
	if ( ch_number == 58 ) { return 182.33; }
	if ( ch_number == 59 ) { return 184.50; }
	if ( ch_number == 60 ) { return 286.87; }
	if ( ch_number == 61 ) { return 187.70; }
	if ( ch_number == 62 ) { return 189.87; }
	if ( ch_number == 63 ) { return 295.25; }
	
      }
      
    }
    
  }

  return -1.0;

}

double chan_properties::getStripArea( int layer_number, int ivmm, int ch_number ) {

  if ( layer_number == 3 ) {

  }

  if ( layer_number == 4 ) {

  }

  return 100.0;
  
}

bool chan_properties::isGood( int ilayer, int ivmm, int ich, bool ispad, int data_type, int detector_type ) {

  
  bool good = true;

  if ( detector_type == 1 ) { 

    if ( data_type == type_6bit ) {

      if ( ispad ) {
	if ( ilayer == 3 ) {
	  if ( ivmm == 1 ) {
	    if ( ich >= 0 && ich <= 7 ) good = false; // unconnected channels
	    if ( ich == 29 )            good = false; // only one point bad (at=2.2)
	    if ( ich == 52 )            good = false; // missing at 1.5 and at 1.0 points
	    if ( ich == 56 )            good = false; // missing many points
	  }
	  else if ( ivmm == 2 ) {
	    if ( ich == 0 )             good = false; //wrong efficiency in low KEK rate < 2.0 kHz/cm^2 region
	    if ( ich == 29 )            good = false; // dead channel
	    if ( ich >= 56 )            good = false; // unconnected channels
	  }
	}
	else if ( ilayer == 4 ) {
	  if ( ivmm == 1 ) {
	  
	    good = false; // layer 4 bottom has low detector gain
	  
	    /*
	      if ( ich >= 0 && ich <= 7 ) good = false; // unconnected channels
	      if ( ich == 8 )             good = false; // weird shape plus inconsistent pattern (threshold too low?)
	      if ( ich == 9 )             good = false; // also inconsistent pattern
	      if ( ich == 10 )            good = false; // also inconsistent pattern
	      if ( ich == 11 )            good = false; // also inconsistent pattern
	      if ( ich == 12 )            good = true; // questionable at low KEK rate...
	      if ( ich == 13 )            good = true;
	      if ( ich == 14 )            good = false; // also inconsistent pattern
	      if ( ich == 15 )            good = false; // low gain
	      if ( ich == 16 )            good = false; // inconsistent pattern
	      if ( ich == 21 )            good = true;
	      if ( ich == 33 )            good = true;
	      if ( ich == 37 )            good = false; // good shape but low gain
	    */
	  }
	  else if ( ivmm == 2 ) {
	  
	    good = false;// can't use data
	  
	    /*
	      if ( ich == 1 ) good = true;
	      if ( ich == 7 ) good = true;
	      if ( ich == 9 ) good = true;
	      if ( ich == 21 ) good = true;
	    */
	  
	  }
	}

      }
      else {
	if ( ilayer == 3 ) {
	  if ( ivmm == 4 ) {
	    if ( ich == 0 ) good = false;
	    if ( ich == 4 ) good = false;  // threshold too low
	    if ( ich == 15 ) good = false; // threshold too low 
	    if ( ich == 24 ) good = false; // threshold too low 
	    if ( ich == 39 ) good = false; // threshold too low 
	    if ( ich == 42 ) good = false; // threshold too low 
	    if ( ich == 46 ) good = false; // threshold too low 
	  
	    if ( ich >= 47 && ich <= 53) good = false; // spacer
	  }
	}
	else if ( ilayer == 4 ) {
	  if ( ivmm == 3 ) {
	    good = false;

	    if ( ich == 7 )  good = false;
	    if ( ich == 20 ) good = false;
	    if ( ich == 23 ) good = false;
	  
	  }
	}

      }
    
    }
    else if ( data_type == type_10bit ) {

    }
    else {
      good = false;
    }

  }

  //For QS2
  
  else if ( detector_type == 2 ) {
    
    if ( data_type == type_6bit ) {

      if ( ispad ) {
	if ( ilayer == 3 ) {
	  if ( ivmm == 1 ) {
	    if ( ich <= 41 )            good = false; // unconnected channels
	  }
	  else if ( ivmm == 2 ) {
	    if ( ich == 0 )             good = false;
	    if ( ich == 2 )             good = false; // one bad point
	    if ( ich == 20 )            good = false; // lower rates disorderly
	    if ( ich == 22 )            good = false; // all rates disorderly 
	    if ( ich >= 23 )            good = false; // unconnected channels
	  }
	}
	else if ( ilayer == 4 ) {
	  if ( ivmm == 1 ) {
	    if ( ich <= 40 )            good = false; // unconnected channels
	    if ( ich == 41 )            good = false; // no pattern
	  }
	  else if ( ivmm == 2 ) {
	    if ( ich == 0 )             good = false;
	    if ( ich >= 22 )            good = false; // unconnected channels
	  }
	}

      }

      //For QS2 strips;
      else {
	if ( ilayer == 3 ) {
	  good = false; // not using this layer
	  /*
	  if ( ivmm == 3 ) {
	    if ( ich == 0 ) good = false;
	    if ( ich == 4 ) good = false;  // threshold too low
	    if ( ich == 15 ) good = false; // threshold too low 
	    if ( ich == 24 ) good = false; // threshold too low 
	    if ( ich == 39 ) good = false; // threshold too low 
	    if ( ich == 42 ) good = false; // threshold too low 
	    if ( ich == 46 ) good = false; // threshold too low 
	  
	    if ( ich >= 47 && ich <= 53) good = false; // spacer
	  }
	  */
	}
	else if ( ilayer == 4 ) {
	  if ( ivmm == 4 ) {
	    if ( ich == 0 )  good = false; // default
	    if ( ich == 60 ) good = false; // poorly fit efficiency
	    }
	}

      }
    
    }
    else if ( data_type == type_10bit ) {

    }
    else {
      good = false;
    }
    
  }

  return good; 
}
