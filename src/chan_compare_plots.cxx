#include "chan_compare_plots.h"

chan_compare_plots::chan_compare_plots() {
  layer = -1;
  SCA_ID = -1;
  elink = -1;
  vmm   = -1;
  chan  = -1;

  npoints = 0;

  capacitance = -1;

  // y2 = ( a2*y1 ) / ( a1 + (a2*t2*area2 - a1*t1*area1) * y1 )
  // a1 = [1]    // a2 = [2]
  // t1 = [3]    // t2 = [4]
  // area1 = [5] // area2 = [6]

  f_compare = new TF1("f_compare", "[0]+[2]*x/([1]+([2]*[4]*[6]-[1]*[3]*[5])*x)", 0, 2000);

  Tl = new TLatex();
  Tl->SetTextSize(0.04);

  hits_ch1_vs_hits_ch2 = TGraph(1);
  hits_ch1_vs_hits_ch2.SetMarkerStyle(kStar);
  hits_ch1_vs_hits_ch2.SetMarkerSize(0.5);
}

chan_compare_plots::~chan_compare_plots() {

  delete f_compare;

  delete Tl;

  delete ch1;
  delete ch2;

}

void chan_compare_plots::reset() {

  npoints=0;

  hits_ch1_vs_hits_ch2.Clear();

}

bool chan_compare_plots::SetParams( chan_plots * ich1,
				    chan_plots * ich2 ) {

  ch1 = ich1;
  ch2 = ich2;

  max_rate_ch1     = 0;
  max_rate_ch2     = 0;
  max_tot_rate_ch1 = 0;
  max_tot_rate_ch2 = 0;

  // y2 = ( a2*y1 ) / ( a1 + (a2*t2*area2 - a1*t1*area1) * y1 )
  // a1 = [1]    // a2 = [2]                                   
  // t1 = [3]    // t2 = [4]                                   
  // area1 = [5] // area2 = [6]                                

  //f_compare->FixParameter( 5, ch1->area );
  //f_compare->FixParameter( 6, ch2->area );

  f_compare->FixParameter( 5, 1.0 );   
  f_compare->FixParameter( 6, 1.0 );

}

bool chan_compare_plots::MakePlots() {

  int npoints_1 = ch1->hits_vs_KEK_rate_vec.size();
  int npoints_2 = ch2->hits_vs_KEK_rate_vec.size();

  if ( npoints_1 != npoints_2 ) {
    std::cout << "FATAL: ch1 and ch2 not same number of points!!! " << std::endl;
    return false;
  }

  for ( int ip=0; ip < npoints_1; ip++ ) {
    float KEK_rate_ch1 = ch1->hits_vs_KEK_rate_vec.at(ip).first;
    float hit_rate_ch1 = ch1->hits_vs_KEK_rate_vec.at(ip).second;
    float area_ch1     = ch1->area;

    float KEK_rate_ch2 = ch2->hits_vs_KEK_rate_vec.at(ip).first;
    float hit_rate_ch2 = ch2->hits_vs_KEK_rate_vec.at(ip).second;
    float area_ch2     = ch2->area;

    if ( hit_rate_ch1 > 0.00001 && 
	 hit_rate_ch2 > 0.00001 ) { 

      std::cout << "filling points " << npoints << " " << hit_rate_ch1*area_ch1 << " " << hit_rate_ch2*area_ch2 << std::endl;

      hits_ch1_vs_hits_ch2.SetPoint( npoints, 
				     hit_rate_ch1*area_ch1, 
				     hit_rate_ch2*area_ch2);
      npoints++;

      if ( max_rate_ch1 < hit_rate_ch1 )     max_rate_ch1     = hit_rate_ch1;
      if ( max_rate_ch2 < hit_rate_ch2 )     max_rate_ch2     = hit_rate_ch2; 
      if ( max_tot_rate_ch1 < hit_rate_ch1*area_ch1 ) max_tot_rate_ch1 = hit_rate_ch1 * area_ch1;
      if ( max_tot_rate_ch2 < hit_rate_ch2*area_ch2 ) max_tot_rate_ch2 = hit_rate_ch2 * area_ch2;
    }
    
  }

}


void chan_compare_plots::FitAll( float max_kek_rate ){

  f_compare->FixParameter(1, ch1->f_linear_tot->GetParameter(1));
  f_compare->FixParameter(2, ch2->f_linear_tot->GetParameter(1));
  f_compare->SetParameter(3, ch1->f_1overX_tot->GetParameter(2));
  f_compare->SetParameter(4, ch2->f_1overX_tot->GetParameter(2));


  hits_ch1_vs_hits_ch2.Fit(f_compare, "N", "", 0, max_tot_rate_ch1*1.1);

}

void chan_compare_plots::DrawAll( float max_kek_rate, TPostScript *ps, TCanvas * c_ch, TH1F * hist ){

  if ( npoints == 0 ) return;

  c_ch->cd();

  //-----------------------------//
  
  int layer1 = ch1->layer;
  int vmm1   = ch1->vmm;
  int chan1  = ch1->chan;

  float area1 = ch1->area;
  float cap1  = ch1->capacitance;

  int layer2 = ch2->layer;
  int vmm2   = ch2->vmm;
  int chan2  = ch2->chan;

  float area2 =ch2->area;
  float cap2  = ch2->capacitance;

  //-----------------------------//

  if ( ch1->npoints_eff > 0 && ch2->npoints_eff > 0 ) {

    hist->SetBins(1, 0, TMath::Max(max_kek_rate*area1,max_kek_rate*area2)*1.1  );
    hist->SetMaximum(TMath::Max(ch1->max_tot_rate, ch2->max_tot_rate)*2.0);

    m_sx.str("");
    m_sx << "Layer " << layer1 << " VMM " << vmm1 << " ch "<< chan1 << " vs "
         << "Layer " << layer2 << " VMM " << vmm2 << " ch "<< chan2 << " Hit Rate vs KEK Hit Rate";

    hist->SetTitle(m_sx.str().c_str());
    hist->SetXTitle("KEK Photon Rate (kHz)");
    hist->SetYTitle("Total Hit Rate (KHz)");

    hist->Draw();

    ch1->f_linear_eff_kek_tot->Draw("same");
    ch2->f_linear_eff_kek_tot->Draw("same");

    m_sx.str("");
    m_sx << "Slope ch1 " << ch1->f_linear_eff_kek_tot->GetParameter(1) 
	 << " slope ch2 " << ch2->f_linear_eff_kek_tot->GetParameter(1);
    Tl->DrawLatex(max_kek_rate*0.15*area1, hist->GetMaximum()*0.8, m_sx.str().c_str());

    m_sx.str("");
    m_sx << "Capcitance ch1 " << cap1 << " Dead Time ch1 " << ch1->f_expo_eff_tot->GetParameter(2) * 1e6 << "ns ";
    Tl->DrawLatex(max_kek_rate*0.15*area1, hist->GetMaximum()*0.7, m_sx.str().c_str());

    m_sx.str("");
    m_sx << "Capcitance ch2 " << cap2 << " Dead Time ch2 " << ch2->f_expo_eff_tot->GetParameter(2) * 1e6 << "ns ";
    Tl->DrawLatex(max_kek_rate*0.15*area1, hist->GetMaximum()*0.6, m_sx.str().c_str());


    ch1->hits_vs_KEK_rate_tot         .Draw("p");
    ch1->hits_over_eff_vs_KEK_rate_tot.Draw("p");
    ch2->hits_vs_KEK_rate_tot         .Draw("p");
    ch2->hits_over_eff_vs_KEK_rate_tot.Draw("p");

    c_ch->Update();
    ps->NewPage();

    //-----------------------------------------------------------//

    /*
    hist->SetBins(1, 0, max_kek_rate*1.1  );
    hist->SetMaximum(2.0);

    m_sx.str("");
    m_sx << "Layer " << layer1 << " VMM " << vmm1 << " ch "<< chan1 << " vs "
         << "Layer " << layer2 << " VMM " << vmm2 << " ch "<< chan2 << " Hit Efficiency vs KEK Hit Rate";

    hist->SetTitle(m_sx.str().c_str());
    hist->SetXTitle("KEK Hit Rate (kHz/cm^2)");
    hist->SetYTitle("Channel Efficiency");

    hist->Draw();

    ch1->f_expo_eff_kek->Draw("same");
    ch2->f_expo_eff_kek->Draw("same");

    m_sx.str("");
    m_sx << "Slope ch1 " << ch1->f_linear_eff_kek_tot->GetParameter(1)
         << " slope ch2 " << ch2->f_linear_eff_kek_tot->GetParameter(1);
    Tl->DrawLatex(max_kek_rate*0.15, hist->GetMaximum()*0.8, m_sx.str().c_str());

    m_sx.str("");
    m_sx << "Capcitance ch1 " << cap1 << " Dead Time ch1 " << ch1->f_expo_eff_tot->GetParameter(2) * 1e6 << "ns ";
    Tl->DrawLatex(max_kek_rate*0.15, hist->GetMaximum()*0.7, m_sx.str().c_str());

    m_sx.str("");
    m_sx << "Capcitance ch2 " << cap2 << " Dead Time ch2 " << ch2->f_expo_eff_tot->GetParameter(2) * 1e6 << "ns ";
    Tl->DrawLatex(max_kek_rate*0.15, hist->GetMaximum()*0.6, m_sx.str().c_str());

    ch1->eff_vs_KEK_rate.Draw("p");
    ch2->eff_vs_KEK_rate.Draw("p");

    c_ch->Update();
    ps->NewPage();

    //-----------------------------------------------------------//

    hist->SetBins(1, 0, TMath::Max(max_kek_rate*area1,max_kek_rate*area2)*1.1  );
    hist->SetMaximum(2.0);

    m_sx.str("");
    m_sx << "Layer " << layer1 << " VMM " << vmm1 << " ch "<< chan1 << " vs "
         << "Layer " << layer2 << " VMM " << vmm2 << " ch "<< chan2 << " Hit Efficiency vs KEK Hit Rate";

    hist->SetTitle(m_sx.str().c_str());
    hist->SetXTitle("KEK Hit Rate (kHz)");
    hist->SetYTitle("Channel Efficiency");

    hist->Draw();

    ch1->f_expo_eff_kek_tot->Draw("same");
    ch2->f_expo_eff_kek_tot->Draw("same");

    m_sx.str("");
    m_sx << "Slope ch1 " << ch1->f_linear_eff_kek_tot->GetParameter(1)
         << " slope ch2 " << ch2->f_linear_eff_kek_tot->GetParameter(1);
    Tl->DrawLatex(max_kek_rate*0.15*area1, hist->GetMaximum()*0.8, m_sx.str().c_str());

    m_sx.str("");
    m_sx << "Capcitance ch1 " << cap1 << " Dead Time ch1 " << ch1->f_expo_eff_tot->GetParameter(2) * 1e6 << "ns ";
    Tl->DrawLatex(max_kek_rate*0.15*area1, hist->GetMaximum()*0.7, m_sx.str().c_str());

    m_sx.str("");
    m_sx << "Capcitance ch2 " << cap2 << " Dead Time ch2 " << ch2->f_expo_eff_tot->GetParameter(2) * 1e6 << "ns ";
    Tl->DrawLatex(max_kek_rate*0.15*area1, hist->GetMaximum()*0.6, m_sx.str().c_str());

    ch1->eff_vs_KEK_rate_tot         .Draw("p");
    ch2->eff_vs_KEK_rate_tot.Draw("p");

    c_ch->Update();
    ps->NewPage();
    */

    //------------------------------------------------------------------------------//

    hist->SetBins(1, 0, TMath::Max(ch1->max_tot_rate, ch2->max_tot_rate)*1.1  );
    hist->SetMaximum(2.0);

    m_sx.str("");
    m_sx << "Layer " << layer1 << " VMM " << vmm1 << " ch "<< chan1 << " vs "
         << "Layer " << layer2 << " VMM " << vmm2 << " ch "<< chan2 << " Hit Efficiency vs Total Hit Rate";

    hist->SetTitle(m_sx.str().c_str());
    hist->SetXTitle("Total Hit Rate (kHz)");
    hist->SetYTitle("Channel Efficiency");

    hist->Draw();

    ch1->f_expo_eff_tot->Draw("same");
    ch2->f_expo_eff_tot->Draw("same");

    m_sx.str("");
    m_sx << "Slope ch1 " << ch1->f_linear_eff_kek_tot->GetParameter(1)
         << " slope ch2 " << ch2->f_linear_eff_kek_tot->GetParameter(1);
    Tl->DrawLatex(TMath::Max(ch1->max_tot_rate, ch2->max_tot_rate)*0.15, 
		  hist->GetMaximum()*0.8, m_sx.str().c_str());

    m_sx.str("");
    m_sx << "Capcitance ch1 " << cap1 << " Dead Time ch1 " << ch1->f_expo_eff_tot->GetParameter(2) * 1e6 << "ns ";
    Tl->DrawLatex(TMath::Max(ch1->max_tot_rate, ch2->max_tot_rate)*0.15, 
		  hist->GetMaximum()*0.7, m_sx.str().c_str());

    m_sx.str("");
    m_sx << "Capcitance ch2 " << cap2 << " Dead Time ch2 " << ch2->f_expo_eff_tot->GetParameter(2) * 1e6 << "ns ";
    Tl->DrawLatex(TMath::Max(ch1->max_tot_rate, ch2->max_tot_rate)*0.15, 
		  hist->GetMaximum()*0.6, m_sx.str().c_str());

    ch1->eff_vs_photon_rate_tot         .Draw("p");
    ch2->eff_vs_photon_rate_tot.Draw("p");

    c_ch->Update();
    ps->NewPage();


    return;


  }

  bool plot_norm = true;
  bool plot_expo = false;

  if ( plot_norm ) {

    //----------------------------------------------------//

    hist->SetBins(1, 0, TMath::Max(max_kek_rate*area1,max_kek_rate*area2)*1.1  );
    hist->SetMaximum(TMath::Max(ch1->max_tot_rate, ch2->max_tot_rate)*2.0);

    m_sx.str("");
    m_sx << "Layer " << layer1 << " VMM " << vmm1 << " ch "<< chan1 << " vs "
         << "Layer " << layer2 << " VMM " << vmm2 << " ch "<< chan2 << " Hit Rate vs KEK Hit Rate";

    hist->SetTitle(m_sx.str().c_str());
    hist->SetXTitle("KEK Photon Rate (kHz)");
    hist->SetYTitle("Total Hit Rate (KHz)");

    hist->Draw();

    ch1->f_linear_tot->Draw("same");
    ch2->f_linear_tot->Draw("same");
    ch1->f_x1overX_tot->Draw("same");
    ch2->f_x1overX_tot->Draw("same");

    //------------------------------------------------------//

    m_sx.str("");
    m_sx << "Noise ch1 " << ch1->f_linear_tot->GetParameter(0) << " Cross-Talk Factor ch1 " << ch1->f_linear_tot->GetParameter(1);
    Tl->DrawLatex(max_kek_rate*0.15*area1, hist->GetMaximum()*0.8, m_sx.str().c_str());

    m_sx.str("");
    m_sx << "Capcitance ch1 " << cap1 << " Dead Time ch1 " << ch1->f_1overX_tot->GetParameter(1) * 1e6 << "ns ";
    Tl->DrawLatex(max_kek_rate*0.15*area1, hist->GetMaximum()*0.7, m_sx.str().c_str());

    ch1->hits_vs_KEK_rate_tot.Draw("p");

    //-----------------------------------------------------//

    m_sx.str("");
    m_sx << "Noise ch2 " << ch2->f_linear_tot->GetParameter(0) << " Cross-Talk Factor ch2 " << ch2->f_linear_tot->GetParameter(1);
    Tl->DrawLatex(max_kek_rate*0.15*area1, hist->GetMaximum()*0.6, m_sx.str().c_str());

    m_sx.str("");
    m_sx << "Capcitance ch2 " << cap2 << " Dead Time ch2 " << ch2->f_1overX_tot->GetParameter(1) * 1e6 << "ns ";
    Tl->DrawLatex(max_kek_rate*0.15*area1, hist->GetMaximum()*0.5, m_sx.str().c_str());

    ch2->hits_vs_KEK_rate_tot.Draw("p");

    //-----------------------------------------------------//

    c_ch->Update();
    ps->NewPage();


    //----------------------------------------------------//
    //             Channel Fit to 1 over 1+X
    //----------------------------------------------------//

    hist->SetBins(1, 0, max_kek_rate*2.0*area1  );
    hist->SetMaximum(1.5);

    m_sx.str("");
    m_sx << "Layer " << layer1 << " VMM " << vmm1 << " ch "<< chan1 << " vs "
         << "Layer " << layer2 << " VMM " << vmm2 << " ch "<< chan2 << " Hit Efficiency vs Total Hit Rate";

    hist->SetTitle(m_sx.str().c_str());
    hist->SetXTitle("Total Hit Rate (kHz)");
    hist->SetYTitle("Channel Efficiency");

    hist->Draw();

    ch1->f_1overX_tot->Draw("same");
    ch2->f_1overX_tot->Draw("same");

    //------------------------------------------------------//

    m_sx.str("");
    m_sx << "Noise ch1 " << ch1->f_linear_tot->GetParameter(0) << " Cross-Talk Factor ch1 " << ch1->f_linear_tot->GetParameter(1);
    Tl->DrawLatex(max_kek_rate*0.15*area1, hist->GetMaximum()*0.8, m_sx.str().c_str());

    m_sx.str("");
    m_sx << "Capcitance ch1 " << cap1 << " Dead Time ch1 " << ch1->f_1overX_tot->GetParameter(1) * 1e6 << "ns ";
    Tl->DrawLatex(max_kek_rate*0.15*area1, hist->GetMaximum()*0.7, m_sx.str().c_str());

    ch1->hits_over_linear_vs_tot_rate.Draw("p");

    //-----------------------------------------------------//

    m_sx.str("");
    m_sx << "Noise ch2 " << ch2->f_linear_tot->GetParameter(0) << " Cross-Talk Factor ch2 " << ch2->f_linear_tot->GetParameter(1);
    Tl->DrawLatex(max_kek_rate*0.15*area1, hist->GetMaximum()*0.6, m_sx.str().c_str());

    m_sx.str("");
    m_sx << "Capcitance ch2 " << cap2 << " Dead Time ch2 " << ch2->f_1overX_tot->GetParameter(1) * 1e6 << "ns ";
    Tl->DrawLatex(max_kek_rate*0.15*area1, hist->GetMaximum()*0.5, m_sx.str().c_str());

    ch2->hits_over_linear_vs_tot_rate.Draw("p");

    //-----------------------------------------------------//
    
    c_ch->Update();
    ps->NewPage();

    //----------------------------------------------------//
    //   Channel Efficiency Fit to Exponential
    //----------------------------------------------------//

    if ( plot_expo ) {
      
      hist->SetBins(1, 0, max_kek_rate*2.0*area1  );
      hist->SetMaximum(1.5);
      
      m_sx.str("");
      m_sx << "Layer " << layer1 << " VMM " << vmm1 << " ch "<< chan1 << " vs "
	   << "Layer " << layer2 << " VMM " << vmm2 << " ch "<< chan2 << " Hit Efficiency vs Total Hit Rate";
      
      hist->SetTitle(m_sx.str().c_str());
      hist->SetXTitle("Total Hit Rate (kHz)");
      hist->SetYTitle("Channel Efficiency");
      
      hist->Draw();
      
      ch1->f_expo_tot->Draw("same");
      ch2->f_expo_tot->Draw("same");
      
      //------------------------------------------------------//
      
      m_sx.str("");
      m_sx << "Noise ch1 " << ch1->f_linear_tot->GetParameter(0) << " Cross-Talk Factor ch1 " << ch1->f_linear_tot->GetParameter(1);
      Tl->DrawLatex(max_kek_rate*0.15*area1, hist->GetMaximum()*0.8, m_sx.str().c_str());
      
      m_sx.str("");
      m_sx << "Capcitance ch1 " << cap1 << " Dead Time ch1 " << ch1->f_expo_tot->GetParameter(2) * 1e6 << "ns ";
      Tl->DrawLatex(max_kek_rate*0.15*area1, hist->GetMaximum()*0.7, m_sx.str().c_str());
      
      ch1->hits_over_linear_vs_tot_rate.Draw("p");
      
      //-----------------------------------------------------//
      
      m_sx.str("");
      m_sx << "Noise ch2 " << ch2->f_linear_tot->GetParameter(0) << " Cross-Talk Factor ch2 " << ch2->f_linear_tot->GetParameter(1);
      Tl->DrawLatex(max_kek_rate*0.15*area1, hist->GetMaximum()*0.6, m_sx.str().c_str());
      
      m_sx.str("");
      m_sx << "Capcitance ch2 " << cap2 << " Dead Time ch2 " << ch2->f_1overX_tot->GetParameter(1) * 1e6 << "ns ";
      Tl->DrawLatex(max_kek_rate*0.15*area1, hist->GetMaximum()*0.5, m_sx.str().c_str());
      
      ch2->hits_over_linear_vs_tot_rate.Draw("p");
      
      //-----------------------------------------------------//
      
      c_ch->Update();
      ps->NewPage();
    }

    //----------------------------------------------------//
    //    Channel Digital Compare
    //----------------------------------------------------//
    
    hist->SetBins(1, 0, TMath::Max(max_tot_rate_ch1,max_tot_rate_ch2)*2.0  );
    hist->SetMaximum(   TMath::Max(max_tot_rate_ch1,max_tot_rate_ch2)*2.0  );
    
    m_sx.str("");
    m_sx << "Layer " << layer1 << " VMM " << vmm1 << " ch "<< chan1 << " vs " 
	 << "Layer " << layer2 << " VMM " << vmm2 << " ch "<< chan2 << " Total Hit Rate";
    
    hist->SetTitle(m_sx.str().c_str());

    m_sx.str("");
    m_sx << "Layer " << layer1 << " VMM " << vmm1 << " ch "<< chan1 << " Hit Rate ( kHz )";
    hist->SetXTitle(m_sx.str().c_str());

    m_sx.str("");
    m_sx << "Layer " << layer2 << " VMM " << vmm2 << " ch "<< chan2 << " Hit Rate ( kHz )";
    hist->SetYTitle(m_sx.str().c_str());
    
    hist->Draw();
    
    m_sx.str("");
    m_sx << "Area ch1 " << area1 << "cm^2 Area " << area2 << "cm^2";
    Tl->DrawLatex(max_tot_rate_ch1*0.15, hist->GetMaximum()*0.9, m_sx.str().c_str());
    m_sx.str("");
    m_sx << "Capcitance ch1 " << cap1 << "pF Capacitance " << cap2 << "pF";
    Tl->DrawLatex(max_tot_rate_ch1*0.15, hist->GetMaximum()*0.83, m_sx.str().c_str());

    m_sx.str("");
    m_sx << "Fit Param a1 " << f_compare->GetParameter(1) << " a2 " << f_compare->GetParameter(2) ;
    Tl->DrawLatex(max_tot_rate_ch1*0.15, hist->GetMaximum()*0.75, m_sx.str().c_str());

    m_sx.str("");
    m_sx << " t1 " << f_compare->GetParameter(3)*1e6 << " t2 " << f_compare->GetParameter(4)*1e6;
    Tl->DrawLatex(max_tot_rate_ch1*0.15, hist->GetMaximum()*0.68, m_sx.str().c_str());

    f_compare->Draw("same");

    hits_ch1_vs_hits_ch2.Draw("p");

    c_ch->Update();
    ps->NewPage();

    //reset();

  }

}
