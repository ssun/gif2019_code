#include "chan_plots.h"

chan_plots::chan_plots() {
  layer = -1;
  SCA_ID = -1;
  elink = -1;
  vmm   = -1;
  chan  = -1;

  npoints = 0;
  npoints_eff = 0;
  npoints_baseline = 0;

  is_pad = true;
  is_good = true;

  capacitance = -1;
  resistance  = -1;

  //-------------------------------------------------//

  hits_vs_KEK_rate_vec.resize(0);

  hits_vs_KEK_rate = TGraph(1);
  hits_vs_KEK_rate_tot = TGraph(1);

  hits_over_linear_vs_KEK_rate = TGraph(1);
  hits_over_linear_vs_tot_rate = TGraph(1);

  baseline_vs_KEK_rate = TGraph(1);
  baseline_vs_KEK_rate_tot = TGraph(1);

  //------------------------------------------------//

  hits_over_eff_vs_KEK_rate = TGraph(1);
  hits_over_eff_vs_KEK_rate_tot = TGraph(1);

  eff_vs_KEK_rate = TGraph(1);
  eff_vs_KEK_rate_tot = TGraph(1);

  eff_vs_photon_rate_tot = TGraph(1);

  //------------------------------------------------//

  f_linear = new TF1("f_linear", "[0]+[1]*x", 0, 20);
  f_expo   = new TF1("f_expo",   "[0]+exp(-x*[1]*[2]*[3])", 0, 20);
  f_xexpo  = new TF1("f_xexpo",  "[0]+[1]*x*exp(-x*[1]*[2]*[3])", 0, 20);

  f_x1overX  = new TF1("f_x1overX",  "[0]+[1]*x/(1.0+[1]*[2]*[3]*x)", 0, 20);

  f_linear_tot = new TF1("f_linear_tot", "[0]+[1]*x", 0, 5000);
  f_expo_tot   = new TF1("f_expo_tot",   "[0]+0*[1]+exp(-x*[2])", 0, 5000);
  f_xexpo_tot  = new TF1("f_xexpo_tot",  "[0]+[1]*x*exp(-x*[1]*[2])", 0, 5000);

  f_x1overX_tot = new TF1("f_x1overX_tot", "[0]+[1]*x/(1.0+[1]*[2]*x)", 0, 5000);
  f_1overX_tot  = new TF1("f_1overX_tot",  "[0]+1.0/(1.0+[1]*x)", 0, 5000);

  //----------------------------------------------//

  f_linear_eff_kek     = new TF1("f_linear_eff_kek",     "[0]+[1]*x", 0, 20);
  f_linear_eff_kek_tot = new TF1("f_linear_eff_kek_tot", "[0]+[1]*x", 0, 5000);

  f_expo_eff_kek       = new TF1("f_expo_eff_kek",      "[0]+exp(-x*[1]*[2]*[3])", 0, 20);
  f_expo_eff_kek_tot   = new TF1("f_expo_eff_kek_tot",   "[0]+exp(-x*[1]*[2])", 0, 5000);
  //f_expo_eff_tot       = new TF1("f_expo_eff_tot",       "[0]+0*[1]+exp(-x*[2])", 0, 5000);
 
  f_expo_eff_tot       = new TF1("f_expo_eff_tot",       "[0]+[1]/(1+[2]*x)", 0, 5000);
  
  //---------------------------------------------//

  f_expo->SetParameter(0, 0.0);
  f_expo->FixParameter(1, 1.0);
  f_expo->SetParameter(2, 400e-6);
  f_expo_tot->SetParameter(0, 0.0);
  f_expo_tot->FixParameter(1, 1.0);
  f_expo_tot->SetParameter(2, 400e-6);
  f_xexpo->SetParameter(2, 400e-6);
  f_xexpo_tot->SetParameter(2, 400e-6);
  
  f_x1overX_tot->SetParameter(1, 1.0);
  f_x1overX_tot->SetParameter(2, 800e-6);
  f_1overX_tot->SetParameter(1, 800e-6);

  //-------------------------------------------//

  f_linear_eff_kek->SetParameter(0, 0.0);
  f_linear_eff_kek->SetParameter(1, 1.0);

  f_linear_eff_kek_tot->SetParameter(0, 0.0);
  f_linear_eff_kek_tot->SetParameter(1, 1.0);

  f_expo_eff_kek->SetParameter(0, 1.0);
  f_expo_eff_kek->FixParameter(1, 1.0);
  f_expo_eff_kek->SetParameter(2, 250e-6);

  f_expo_eff_kek_tot->SetParameter(0, 1.0);
  f_expo_eff_kek_tot->FixParameter(1, 1.0);
  f_expo_eff_kek_tot->SetParameter(2, 250e-6);

  f_expo_eff_tot->SetParameter(0, 0.0);
  f_expo_eff_tot->FixParameter(1, 1.0);
  f_expo_eff_tot->SetParameter(2, 250e-6);
  
  //-------------------------------------------//

  Tl = new TLatex();
  Tl->SetTextSize(0.04);

}

chan_plots::~chan_plots() {

  delete f_linear;
  delete f_expo;
  delete f_xexpo;

  delete f_x1overX;

  delete f_linear_tot;
  delete f_expo_tot;
  delete f_xexpo_tot;

  delete f_1overX_tot;
  delete f_x1overX_tot;

  delete f_linear_eff_kek    ;
  delete f_expo_eff_kek      ;
  delete f_linear_eff_kek_tot;
  delete f_expo_eff_kek_tot  ;

  delete f_expo_eff_tot      ;

  delete Tl;

}

void chan_plots::reset() {

  npoints=0;
  npoints_eff=0;

  hits_vs_KEK_rate_vec.resize(0);

  hits_vs_KEK_rate.Clear();
  hits_vs_KEK_rate_tot.Clear();

  hits_over_linear_vs_KEK_rate.Clear();
  hits_over_linear_vs_tot_rate.Clear();
  
}

void chan_plots::SetParams( int ilayer, int ielink, int ivmm, int ich, int iSCA_ID, float iarea, int icap, bool ispad ) {

  is_pad = ispad;

  layer = ilayer;
  elink = ielink;
  SCA_ID = iSCA_ID;
  area = iarea;
  chan = ich;
  vmm = ivmm;

  if   ( is_pad ) capacitance = icap;
  else {
    f_x1overX_tot->SetParameter(1, 300.0);
    resistance  = icap;
  }

  if ( layer == 3 ) {
    hits_vs_KEK_rate.SetMarkerStyle(kStar);
    hits_vs_KEK_rate_tot.SetMarkerStyle(kStar);
    hits_over_linear_vs_KEK_rate.SetMarkerStyle(kStar);
    hits_over_linear_vs_tot_rate.SetMarkerStyle(kStar);
    
    hits_over_eff_vs_KEK_rate.SetMarkerStyle(kFullStar);;
    hits_over_eff_vs_KEK_rate_tot.SetMarkerStyle(kFullStar);;
    eff_vs_KEK_rate.SetMarkerStyle(kFullStar);;
    eff_vs_KEK_rate_tot.SetMarkerStyle(kFullStar);;
    eff_vs_photon_rate_tot.SetMarkerStyle(kFullStar);;
  }
  else {
    hits_vs_KEK_rate.SetMarkerStyle(kCircle);
    hits_vs_KEK_rate_tot.SetMarkerStyle(kCircle);
    hits_over_linear_vs_KEK_rate.SetMarkerStyle(kCircle);
    hits_over_linear_vs_tot_rate.SetMarkerStyle(kCircle);

    hits_over_eff_vs_KEK_rate.SetMarkerStyle(kFullCircle);;
    hits_over_eff_vs_KEK_rate_tot.SetMarkerStyle(kFullCircle);;
    eff_vs_KEK_rate.SetMarkerStyle(kFullCircle);;
    eff_vs_KEK_rate_tot.SetMarkerStyle(kFullCircle);;
    eff_vs_photon_rate_tot.SetMarkerStyle(kFullCircle);;
  }

  hits_vs_KEK_rate.SetMarkerSize(0.5);
  hits_vs_KEK_rate_tot.SetMarkerSize(0.5);
  hits_over_linear_vs_KEK_rate.SetMarkerSize(0.5);
  hits_over_linear_vs_tot_rate.SetMarkerSize(0.5);

  hits_over_eff_vs_KEK_rate.SetMarkerSize(0.5);
  hits_over_eff_vs_KEK_rate_tot.SetMarkerSize(0.5);
  eff_vs_KEK_rate.SetMarkerSize(0.5);
  eff_vs_KEK_rate_tot.SetMarkerSize(0.5);
  eff_vs_photon_rate_tot.SetMarkerSize(0.5);


  if ( is_pad ) {

    if ( capacitance == 100 ) {
      //hits_vs_KEK_rate.SetMarkerColor(kTeal);
      //hits_vs_KEK_rate_tot.SetMarkerColor(kTeal);
      //hits_over_linear_vs_KEK_rate.SetMarkerColor(kTeal);
      //hits_over_linear_vs_tot_rate.SetMarkerColor(kTeal);
      
      f_linear     ->SetLineColor(kTeal);
      f_expo       ->SetLineColor(kTeal);
      f_xexpo      ->SetLineColor(kTeal);
      f_x1overX    ->SetLineColor(kTeal);
      f_linear_tot ->SetLineColor(kTeal);
      f_expo_tot   ->SetLineColor(kTeal);
      f_xexpo_tot  ->SetLineColor(kTeal);
      f_x1overX_tot->SetLineColor(kTeal);
      f_1overX_tot ->SetLineColor(kTeal);

      f_linear_eff_kek    ->SetLineColor(kTeal);
      f_expo_eff_kek      ->SetLineColor(kTeal);
      f_linear_eff_kek_tot->SetLineColor(kTeal);
      f_expo_eff_kek_tot  ->SetLineColor(kTeal);

      f_expo_eff_tot      ->SetLineColor(kTeal);

    }
    else if ( capacitance == 150 ) {
      //hits_vs_KEK_rate.SetMarkerColor(kGreen+2);
      //hits_vs_KEK_rate_tot.SetMarkerColor(kGreen+2);
      //hits_over_linear_vs_KEK_rate.SetMarkerColor(kGreen+2);
      //hits_over_linear_vs_tot_rate.SetMarkerColor(kGreen+2);
      
      f_linear     ->SetLineColor(kGreen+2);
      f_expo       ->SetLineColor(kGreen+2);
      f_xexpo      ->SetLineColor(kGreen+2);
      f_x1overX    ->SetLineColor(kGreen+2);
      f_linear_tot ->SetLineColor(kGreen+2);
      f_expo_tot   ->SetLineColor(kGreen+2);
      f_xexpo_tot  ->SetLineColor(kGreen+2);
      f_x1overX_tot->SetLineColor(kGreen+2);
      f_1overX_tot ->SetLineColor(kGreen+2);

      f_linear_eff_kek    ->SetLineColor(kGreen+2);
      f_expo_eff_kek      ->SetLineColor(kGreen+2);
      f_linear_eff_kek_tot->SetLineColor(kGreen+2);
      f_expo_eff_kek_tot  ->SetLineColor(kGreen+2);

      f_expo_eff_tot      ->SetLineColor(kGreen+2);


    }
    else if ( capacitance == 200 ) {
      //hits_vs_KEK_rate.SetMarkerColor(kBlue);
      //hits_vs_KEK_rate_tot.SetMarkerColor(kBlue);
      //hits_over_linear_vs_KEK_rate.SetMarkerColor(kBlue);
      //hits_over_linear_vs_tot_rate.SetMarkerColor(kBlue);
      
      f_linear     ->SetLineColor(kBlue);
      f_expo       ->SetLineColor(kBlue);
      f_xexpo      ->SetLineColor(kBlue);
      f_x1overX    ->SetLineColor(kBlue);
      f_linear_tot ->SetLineColor(kBlue);
      f_expo_tot   ->SetLineColor(kBlue);
      f_xexpo_tot  ->SetLineColor(kBlue);
      f_x1overX_tot->SetLineColor(kBlue);
      f_1overX_tot ->SetLineColor(kBlue);

      f_linear_eff_kek    ->SetLineColor(kBlue);
      f_expo_eff_kek      ->SetLineColor(kBlue);
      f_linear_eff_kek_tot->SetLineColor(kBlue);
      f_expo_eff_kek_tot  ->SetLineColor(kBlue);

      f_expo_eff_tot      ->SetLineColor(kBlue);

    }
    else if ( capacitance == 300 ) {
      //hits_vs_KEK_rate.SetMarkerColor(kOrange+2);
      //hits_vs_KEK_rate_tot.SetMarkerColor(kOrange+2);
      //hits_over_linear_vs_KEK_rate.SetMarkerColor(kOrange+2);
      //hits_over_linear_vs_tot_rate.SetMarkerColor(kOrange+2);
      
      f_linear     ->SetLineColor(kOrange+2);
      f_expo       ->SetLineColor(kOrange+2);
      f_xexpo      ->SetLineColor(kOrange+2);
      f_x1overX    ->SetLineColor(kOrange+2);
      f_linear_tot ->SetLineColor(kOrange+2);
      f_expo_tot   ->SetLineColor(kOrange+2);
      f_xexpo_tot  ->SetLineColor(kOrange+2);
      f_x1overX_tot->SetLineColor(kOrange+2);
      f_1overX_tot ->SetLineColor(kOrange+2);

      f_linear_eff_kek    ->SetLineColor(kOrange+2);
      f_expo_eff_kek      ->SetLineColor(kOrange+2);
      f_linear_eff_kek_tot->SetLineColor(kOrange+2);
      f_expo_eff_kek_tot  ->SetLineColor(kOrange+2);

      f_expo_eff_tot      ->SetLineColor(kOrange+2);

    }
    else if ( capacitance == 470 ) {
      //hits_vs_KEK_rate.SetMarkerColor(kRed);
      //hits_vs_KEK_rate_tot.SetMarkerColor(kRed);
      //hits_over_linear_vs_KEK_rate.SetMarkerColor(kRed);
      //hits_over_linear_vs_tot_rate.SetMarkerColor(kRed);
      
      f_linear     ->SetLineColor(kRed);
      f_expo       ->SetLineColor(kRed);
      f_xexpo      ->SetLineColor(kRed);
      f_x1overX    ->SetLineColor(kRed);
      f_linear_tot ->SetLineColor(kRed);
      f_expo_tot   ->SetLineColor(kRed);
      f_xexpo_tot  ->SetLineColor(kRed);
      f_x1overX_tot->SetLineColor(kRed);
      f_1overX_tot ->SetLineColor(kRed);

      f_linear_eff_kek    ->SetLineColor(kRed);
      f_expo_eff_kek      ->SetLineColor(kRed);
      f_linear_eff_kek_tot->SetLineColor(kRed);
      f_expo_eff_kek_tot  ->SetLineColor(kRed);

      f_expo_eff_tot      ->SetLineColor(kRed);

    }

  } // end if( is_pad )   
  else { // if its strips

    if ( resistance == 200 ) {
      //hits_vs_KEK_rate.SetMarkerColor(kTeal);
      //hits_vs_KEK_rate_tot.SetMarkerColor(kTeal);
      //hits_over_linear_vs_KEK_rate.SetMarkerColor(kTeal);
      //hits_over_linear_vs_tot_rate.SetMarkerColor(kTeal);

      f_linear     ->SetLineColor(kTeal);
      f_expo       ->SetLineColor(kTeal);
      f_xexpo      ->SetLineColor(kTeal);
      f_x1overX    ->SetLineColor(kTeal);
      f_linear_tot ->SetLineColor(kTeal);
      f_expo_tot   ->SetLineColor(kTeal);
      f_xexpo_tot  ->SetLineColor(kTeal);
      f_x1overX_tot->SetLineColor(kTeal);
      f_1overX_tot ->SetLineColor(kTeal);

      f_linear_eff_kek    ->SetLineColor(kTeal);
      f_expo_eff_kek      ->SetLineColor(kTeal);
      f_linear_eff_kek_tot->SetLineColor(kTeal);
      f_expo_eff_kek_tot  ->SetLineColor(kTeal);

      f_expo_eff_tot      ->SetLineColor(kTeal);

    }
    else if ( resistance == 300 ) {
      //hits_vs_KEK_rate.SetMarkerColor(kGreen+2);
      //hits_vs_KEK_rate_tot.SetMarkerColor(kGreen+2);
      //hits_over_linear_vs_KEK_rate.SetMarkerColor(kGreen+2);
      //hits_over_linear_vs_tot_rate.SetMarkerColor(kGreen+2);

      f_linear     ->SetLineColor(kGreen+2);
      f_expo       ->SetLineColor(kGreen+2);
      f_xexpo      ->SetLineColor(kGreen+2);
      f_x1overX    ->SetLineColor(kGreen+2);
      f_linear_tot ->SetLineColor(kGreen+2);
      f_expo_tot   ->SetLineColor(kGreen+2);
      f_xexpo_tot  ->SetLineColor(kGreen+2);
      f_x1overX_tot->SetLineColor(kGreen+2);
      f_1overX_tot ->SetLineColor(kGreen+2);

      f_linear_eff_kek    ->SetLineColor(kGreen+2);
      f_expo_eff_kek      ->SetLineColor(kGreen+2);
      f_linear_eff_kek_tot->SetLineColor(kGreen+2);
      f_expo_eff_kek_tot  ->SetLineColor(kGreen+2);

      f_expo_eff_tot      ->SetLineColor(kGreen+2);

    }
    else if ( resistance == 500 ) {
      //hits_vs_KEK_rate.SetMarkerColor(kBlue);
      //hits_vs_KEK_rate_tot.SetMarkerColor(kBlue);
      //hits_over_linear_vs_KEK_rate.SetMarkerColor(kBlue);
      //hits_over_linear_vs_tot_rate.SetMarkerColor(kBlue);

      f_linear     ->SetLineColor(kBlue);
      f_expo       ->SetLineColor(kBlue);
      f_xexpo      ->SetLineColor(kBlue);
      f_x1overX    ->SetLineColor(kBlue);
      f_linear_tot ->SetLineColor(kBlue);
      f_expo_tot   ->SetLineColor(kBlue);
      f_xexpo_tot  ->SetLineColor(kBlue);
      f_x1overX_tot->SetLineColor(kBlue);
      f_1overX_tot ->SetLineColor(kBlue);

      f_linear_eff_kek    ->SetLineColor(kBlue);
      f_expo_eff_kek      ->SetLineColor(kBlue);
      f_linear_eff_kek_tot->SetLineColor(kBlue);
      f_expo_eff_kek_tot  ->SetLineColor(kBlue);

      f_expo_eff_tot      ->SetLineColor(kBlue);

    }
    else if ( resistance == 1000 ) {
      //hits_vs_KEK_rate.SetMarkerColor(kRed);
      //hits_vs_KEK_rate_tot.SetMarkerColor(kRed);
      //hits_over_linear_vs_KEK_rate.SetMarkerColor(kRed);
      //hits_over_linear_vs_tot_rate.SetMarkerColor(kRed);

      f_linear     ->SetLineColor(kRed);
      f_expo       ->SetLineColor(kRed);
      f_xexpo      ->SetLineColor(kRed);
      f_x1overX    ->SetLineColor(kRed);
      f_linear_tot ->SetLineColor(kRed);
      f_expo_tot   ->SetLineColor(kRed);
      f_xexpo_tot  ->SetLineColor(kRed);
      f_x1overX_tot->SetLineColor(kRed);
      f_1overX_tot ->SetLineColor(kRed);

      f_linear_eff_kek    ->SetLineColor(kRed);
      f_expo_eff_kek      ->SetLineColor(kRed);
      f_linear_eff_kek_tot->SetLineColor(kRed);
      f_expo_eff_kek_tot  ->SetLineColor(kRed);

      f_expo_eff_tot      ->SetLineColor(kRed);

    }

  }

  max_tot_rate = 0;
  npoints = 0;

  f_expo->FixParameter(3, area);
  f_xexpo->FixParameter(3, area);
  f_x1overX->FixParameter(3, area);

}


void chan_plots::AddNoisePoint( float noise_rate ) {
  //f_linear->FixParameter(0, noise_rate/area); // normalized to area
  //f_linear_tot->FixParameter( 0, noise_rate ); // total rate
  //f_xexpo->FixParameter(0, noise_rate/area); // normalized to area
  //f_xexpo_tot->FixParameter( 0, noise_rate ); // total rate   

  if ( max_tot_rate < noise_rate ) max_tot_rate = noise_rate;

}

void chan_plots::AddPoint(float kek_photon_rate, float hit_rate){

  AddPoint( kek_photon_rate, hit_rate, -1.0 );

}

void chan_plots::AddPoint(float kek_photon_rate, float photon_hit_rate, float efficiency){

  if ( photon_hit_rate > 0.00001 ) {

    //std::cout << "adding point " << kek_photon_rate << " " << hit_rate << " eff " << efficiency std::endl;

    //-------------------------------------------------------------------------------//
    //                     photon rate vs KEK rate plots
    //-------------------------------------------------------------------------------//

    hits_vs_KEK_rate_tot.SetPoint( npoints, kek_photon_rate*area, photon_hit_rate);
    hits_vs_KEK_rate.SetPoint(     npoints, kek_photon_rate,      photon_hit_rate/area);

    if ( max_tot_rate < photon_hit_rate ) max_tot_rate = photon_hit_rate;

    hits_vs_KEK_rate_vec.push_back( std::make_pair( kek_photon_rate, photon_hit_rate/area ) );

    npoints++;

    //------------------------------------------------------------------------------//

    if ( efficiency >= 0.0 ) {

      hits_over_eff_vs_KEK_rate.SetPoint(     npoints_eff, kek_photon_rate,      photon_hit_rate/area/efficiency);
      hits_over_eff_vs_KEK_rate_tot.SetPoint( npoints_eff, kek_photon_rate*area, photon_hit_rate/efficiency);

      eff_vs_KEK_rate.SetPoint(        npoints_eff, kek_photon_rate,      efficiency);
      eff_vs_KEK_rate_tot.SetPoint(    npoints_eff, kek_photon_rate*area, efficiency);

      eff_vs_photon_rate_tot.SetPoint( npoints_eff, photon_hit_rate/efficiency, efficiency);

      if ( max_tot_rate < photon_hit_rate/efficiency ) max_tot_rate = photon_hit_rate/efficiency;
       
      npoints_eff++;

    }

  }
  else {
    hits_vs_KEK_rate_vec.push_back( std::make_pair( kek_photon_rate, photon_hit_rate/area ) );
  }

}

void chan_plots::AddBaselinePoint(float kek_photon_rate, float baseline){

  baseline_vs_KEK_rate_tot.SetPoint( npoints_baseline, kek_photon_rate*area, baseline);
  baseline_vs_KEK_rate.SetPoint(     npoints_baseline, kek_photon_rate,      baseline);

  npoints_baseline++;

}

void chan_plots::FitAll( float max_kek_rate){

  //-------------------------------------------------//
  //               Fit Efficiencies
  //-------------------------------------------------//

  if ( npoints_eff > 0 ) {
    
    hits_over_eff_vs_KEK_rate.Fit( f_linear_eff_kek, "N", "", 0.0, 4); //max_kek_rate*1.1);
    float offset = f_linear_eff_kek->GetParameter(0); 
    float slope  = f_linear_eff_kek->GetParameter(1); 
    
    hits_over_eff_vs_KEK_rate_tot.Fit( f_linear_eff_kek_tot, "N", "", 0.0, 4*area); //max_kek_rate*1.1*area);
    float offset_tot = f_linear_eff_kek_tot->GetParameter(0);
    float slope_tot  = f_linear_eff_kek_tot->GetParameter(1);

    f_expo_eff_kek->FixParameter(1, slope);
    f_expo_eff_kek->FixParameter(3, area);
    f_expo_eff_kek_tot->FixParameter(1, slope_tot);

    eff_vs_KEK_rate.Fit( f_expo_eff_kek, "N", "", 0, max_kek_rate*1.1);
    eff_vs_KEK_rate_tot.Fit( f_expo_eff_kek_tot, "N", "", 0, max_kek_rate*1.1*area);

    eff_vs_photon_rate_tot.Fit( f_expo_eff_tot, "N", "", 0, max_tot_rate*1.1);

  }

  //-------------------------------------------------//

  if ( npoints > 0 ) {
    
    //hits_vs_KEK_rate.Fit(f_linear, "N", "", 0, 0.3);
    //float offset = f_linear->GetParameter(0);
    //float slope  = f_linear->GetParameter(1);
    
    hits_vs_KEK_rate.Fit(f_x1overX, "N", "", 0, max_kek_rate); //1.0);
    float offset = f_x1overX->GetParameter(0);
    float slope  = f_x1overX->GetParameter(1);
    
    f_linear->FixParameter(0, offset);
    f_linear->FixParameter(1, slope);
    
    f_xexpo->FixParameter(0, offset);
    f_xexpo->FixParameter(1, slope);
    
    f_x1overX->FixParameter(0, offset);
    f_x1overX->FixParameter(1, slope);
    
    //-------------------------------------------------//
    
    //hits_vs_KEK_rate_tot.Fit(f_linear_tot, "N", "", 0, 0.3*area);
    //float offset_tot = f_linear_tot->GetParameter(0);
    //float slope_tot  = f_linear_tot->GetParameter(1);
    
    hits_vs_KEK_rate_tot.Fit(f_x1overX_tot, "N", "", 0, max_kek_rate*area); //1.0*area);
    float offset_tot = f_x1overX_tot->GetParameter(0);
    float slope_tot  = f_x1overX_tot->GetParameter(1);
    
    f_linear_tot->FixParameter(0, offset_tot);
    f_linear_tot->FixParameter(1, slope_tot);
    
    f_xexpo_tot->FixParameter(0, offset_tot);
    f_xexpo_tot->FixParameter(1, slope_tot);
    
    f_x1overX_tot->FixParameter(0, offset_tot);
    f_x1overX_tot->FixParameter(1, slope_tot);
    
    //------------------------------------------------//
    
    hits_vs_KEK_rate.Fit(f_xexpo, "N", "", 0, max_kek_rate*1.1);
    hits_vs_KEK_rate_tot.Fit(f_xexpo_tot, "N", "", 0, max_kek_rate*1.1*area);
    hits_vs_KEK_rate_tot.Fit(f_x1overX_tot, "N", "", 0, max_kek_rate*1.1*area);
    
    //------------------------------------------------------------------//
    //          Plot Efficiency as Rate / Linear Expected Rate
    //------------------------------------------------------------------//
    
    hits_eff_vs_KEK_rate_vec.resize(0);
    hits_eff_vs_tot_rate_vec.resize(0);
    
    hits_over_linear_vs_KEK_rate.Clear();
    hits_over_linear_vs_tot_rate.Clear();
    
    for( uint ipoint=0; ipoint < hits_vs_KEK_rate_vec.size(); ipoint++ ) {
      
      float KEK_rate = hits_vs_KEK_rate_vec.at(ipoint).first;
      float hit_rate = hits_vs_KEK_rate_vec.at(ipoint).second;
      
      float hit_eff  = hit_rate / f_linear->Eval( KEK_rate );
      float tot_rate = f_linear_tot->Eval( KEK_rate*area );
      
      hits_over_linear_vs_KEK_rate.SetPoint( hits_eff_vs_KEK_rate_vec.size(), 
					     KEK_rate, hit_eff);
      hits_over_linear_vs_tot_rate.SetPoint( hits_eff_vs_tot_rate_vec.size(),
					     tot_rate, hit_eff);
      
      hits_eff_vs_KEK_rate_vec.push_back( std::make_pair( KEK_rate, hit_eff ) );
      hits_eff_vs_tot_rate_vec.push_back( std::make_pair( tot_rate, hit_eff ) );
      
    }
    
    //------------------------------------------------------------//
    //              Fit Efficiency with Exponential Decay
    //------------------------------------------------------------//
    
    hits_over_linear_vs_KEK_rate.Fit(f_expo, "N", "", 0, max_kek_rate*1.1);
    
    hits_over_linear_vs_tot_rate.Fit(f_expo_tot, "N", "", 0, f_linear_tot->Eval(max_kek_rate*area*1.1));
    hits_over_linear_vs_tot_rate.Fit(f_1overX_tot, "N", "", 0, f_linear_tot->Eval(max_kek_rate*area*1.1));
        
  }

}

void chan_plots::DrawAll( float max_kek_rate, TPostScript *ps, TCanvas * c_ch, TH1F * hist ){

  c_ch->cd();

  bool plot_norm = false;
  bool plot_total_rate = true;
  bool plot_expo = false;

  if ( npoints_eff > 0 ) {

    hist->SetBins(1,0, max_kek_rate*1.1 ); 
    hist->SetMaximum(max_tot_rate*1.1/area);

    m_sx.str("");
    m_sx << "Layer " << layer << " VMM " << vmm << " ch " << chan << " Original Hit Rate vs KEK Rate";

    hist->SetTitle(m_sx.str().c_str());
    hist->SetXTitle("KEK Rate (kHz/cm^{2})");
    hist->SetYTitle("Original Photon Rate (kHz/cm^{2})");

    hist->Draw();

    hits_vs_KEK_rate.Draw("p");
    hits_over_eff_vs_KEK_rate.Draw("p");

    m_sx.str("");
    m_sx << "intercept " << f_linear_eff_kek->GetParameter(0) 
	 << " slope "    << f_linear_eff_kek->GetParameter(1);
    Tl->DrawLatex(max_kek_rate*0.15, hist->GetMaximum()*0.8, m_sx.str().c_str());

    m_sx.str("");
    if ( capacitance > 1 ) m_sx << "Capcitance " << capacitance << "pF";
    if ( resistance  > 1 ) m_sx << "Resistance " << resistance  << "kOhm";
    m_sx << " Dead Time " << f_expo_eff_tot->GetParameter(2)*1e6 << "ns";
    Tl->DrawLatex(max_kek_rate*0.15, hist->GetMaximum()*0.7, m_sx.str().c_str());

    f_linear_eff_kek->Draw("same");

    c_ch->Update();
    ps->NewPage();

    //---------------------------------------------------------//
    
    hist->SetBins(1,0, max_kek_rate*1.1*area );
    hist->SetMaximum(max_tot_rate*1.1);

    m_sx.str("");
    m_sx << "Layer " << layer << " VMM " << vmm << " ch " << chan << " Original Hit Rate vs KEK Rate";

    hist->SetTitle(m_sx.str().c_str());
    hist->SetXTitle("KEK Rate (kHz)");
    hist->SetYTitle("Original Photon Rate (kHz)");

    hist->Draw();

    hits_vs_KEK_rate_tot.Draw("p");
    hits_over_eff_vs_KEK_rate_tot.Draw("p");

    m_sx.str("");
    m_sx << "intercept " << f_linear_eff_kek_tot->GetParameter(0) 
	 << " slope "    << f_linear_eff_kek_tot->GetParameter(1);
    Tl->DrawLatex(max_kek_rate*0.15, hist->GetMaximum()*0.8, m_sx.str().c_str());

    m_sx.str("");
    m_sx << "Capcitance " << capacitance << " Dead Time " << f_expo_eff_tot->GetParameter(2)*1e6 << "ns";
    Tl->DrawLatex(max_kek_rate*0.15, hist->GetMaximum()*0.7, m_sx.str().c_str());

    f_linear_eff_kek_tot->Draw("same");

    c_ch->Update();
    ps->NewPage();


    //---------------------------------------------------------//

    /*
    hist->SetBins(1,0, max_kek_rate*1.1 );
    hist->SetMaximum(1.5);

    m_sx.str("");
    m_sx << "Layer " << layer << " VMM " << vmm << " ch " << chan << " Efficiency vs KEK Rate";

    hist->SetTitle(m_sx.str().c_str());
    hist->SetXTitle("KEK Rate (kHz/cm^{2})");
    hist->SetYTitle("Efficiency");

    hist->Draw();

    eff_vs_KEK_rate.Draw("p");

    m_sx.str("");
    m_sx << " Cross-Talk Factor " << f_linear_eff_kek->GetParameter(1);
    Tl->DrawLatex(max_kek_rate*0.15, hist->GetMaximum()*0.8, m_sx.str().c_str());

    m_sx.str("");
    m_sx << "Capcitance " << capacitance << " Dead Time " << f_expo_eff_kek->GetParameter(2)*1e6 << "ns";
    Tl->DrawLatex(max_kek_rate*0.15, hist->GetMaximum()*0.7, m_sx.str().c_str());

    f_expo_eff_kek->Draw("same");

    c_ch->Update();
    ps->NewPage();

    //-------------------------------------------------------------------//

    hist->SetBins(1,0, max_kek_rate*1.1*area );
    hist->SetMaximum(1.5);

    m_sx.str("");
    m_sx << "Layer " << layer << " VMM " << vmm << " ch " << chan << " Efficiency vs KEK Rate";

    hist->SetTitle(m_sx.str().c_str());
    hist->SetXTitle("KEK Rate (kHz)");
    hist->SetYTitle("Efficiency");

    hist->Draw();

    eff_vs_KEK_rate_tot.Draw("p");

    m_sx.str("");
    m_sx << " Cross-Talk Factor " << f_linear_eff_kek->GetParameter(1);
    Tl->DrawLatex(max_kek_rate*0.15, hist->GetMaximum()*0.8, m_sx.str().c_str());

    m_sx.str("");
    m_sx << "Capcitance " << capacitance << " Dead Time " << f_expo_eff_kek_tot->GetParameter(2)*1e6 << "ns";
    Tl->DrawLatex(max_kek_rate*0.15, hist->GetMaximum()*0.7, m_sx.str().c_str());

    f_expo_eff_kek_tot->Draw("same");

    c_ch->Update();
    ps->NewPage();
    */

    //----------------------------------------------------------------------//

    hist->SetBins(1,0, max_tot_rate*1.1 );
    hist->SetMaximum(1.5);
    hist->SetMinimum(0.4);

    m_sx.str("");
    m_sx << "Layer " << layer << " VMM " << vmm << " ch " << chan << " Efficiency vs Original Hit Rate";

    hist->SetTitle(m_sx.str().c_str());
    hist->SetXTitle("Original Hit (kHz)");
    hist->SetYTitle("Efficiency");

    hist->Draw();

    eff_vs_photon_rate_tot.Draw("p");

    m_sx.str("");
    m_sx << " Cross-Talk Factor " << f_linear_eff_kek_tot->GetParameter(1);
    Tl->DrawLatex(max_kek_rate*0.15, hist->GetMaximum()*0.8, m_sx.str().c_str());

    m_sx.str("");
    m_sx << "Capcitance " << capacitance << " Dead Time " << f_expo_eff_tot->GetParameter(2)*1e6 << "ns";
    Tl->DrawLatex(max_kek_rate*0.15, hist->GetMaximum()*0.7, m_sx.str().c_str());

    f_expo_eff_tot->Draw("same");
    
    c_ch->Update();
    ps->NewPage();

    //---------------------------------------------------------------------//

    return;

  }

  if ( plot_norm ) {
    
    if ( plot_expo ) {
      
      hist->SetBins(1,0, 0.8 ); // Linear Region
      hist->SetMaximum(0.8*2.0);
      
      m_sx.str("");
      m_sx << "Layer " << layer << " VMM " << vmm << " ch " << chan << " Digital Hit Rate vs KEK Rate";
      
      hist->SetTitle(m_sx.str().c_str());
      hist->SetXTitle("KEK Rate (kHz/cm^{2})");
      hist->SetYTitle("Photon Rate (kHz/cm^{2})");
      
      hist->Draw();
      
      m_sx.str("");
      m_sx << "Noise " << f_linear->GetParameter(0) << " Cross-Talk Factor " << f_linear->GetParameter(1);
      Tl->DrawLatex(max_kek_rate*0.15, hist->GetMaximum()*0.8, m_sx.str().c_str());
      
      m_sx.str("");
      m_sx << "Capcitance " << capacitance << " Dead Time " << f_xexpo->GetParameter(2)/area;
      Tl->DrawLatex(max_kek_rate*0.15, hist->GetMaximum()*0.7, m_sx.str().c_str());
      
      f_xexpo->Draw("same");
      f_linear->Draw("same");
      
      hits_vs_KEK_rate.Draw("p");
      c_ch->Update();
      ps->NewPage();
    
      //------------------------------------------------------------------//
      
      hist->SetBins(1,0, max_kek_rate*1.1 );
      hist->SetMaximum(max_kek_rate*2.0);
      
      m_sx.str("");
      m_sx << "Layer " << layer << " VMM " << vmm << " ch " << chan << " Digital Hit Rate vs KEK Rate";
      
      hist->SetTitle(m_sx.str().c_str());
      hist->SetXTitle("KEK Rate (kHz/cm^{2})");
      hist->SetYTitle("Photon Rate (kHz/cm^{2})");
      
      hist->Draw();
    
      m_sx.str("");
      m_sx << "Noise " << f_linear->GetParameter(0) << " Cross-Talk Factor " << f_linear->GetParameter(1);
      Tl->DrawLatex(max_kek_rate*0.15, hist->GetMaximum()*0.8, m_sx.str().c_str());
      
      m_sx.str("");
      m_sx << "Capcitance " << capacitance << " Dead Time " << f_xexpo->GetParameter(2)/area;
      Tl->DrawLatex(max_kek_rate*0.15, hist->GetMaximum()*0.7, m_sx.str().c_str());
      
      f_xexpo->Draw("same");
      f_linear->Draw("same");
    
      hits_vs_KEK_rate.Draw("p");
      c_ch->Update();
      ps->NewPage();
          
      //----------------------------------------------------------//
      
      hist->SetBins(1,0, max_kek_rate*1.1 );
      hist->SetMaximum(1.5);
      
      m_sx.str("");
      m_sx << "Layer " << layer << " VMM " << vmm << " ch "<< chan<< " Hit Efficiency vs KEK Rate";
      
      hist->SetTitle(m_sx.str().c_str());
      hist->SetXTitle("KEK Photon Rate (kHz/cm^{2})");
      hist->SetYTitle("Channel Efficiency");
      
      hist->Draw();
      
      f_expo->Draw("same");
      
      m_sx.str("");
      m_sx << "Noise " << f_linear->GetParameter(0) << " Cross-Talk Factor " << f_linear->GetParameter(1);
      Tl->DrawLatex(max_kek_rate*0.15, hist->GetMaximum()*0.8, m_sx.str().c_str());
      
      m_sx.str("");
      m_sx << "Capcitance " << capacitance << " Dead Time " << f_expo->GetParameter(2);
      Tl->DrawLatex(max_kek_rate*0.15, hist->GetMaximum()*0.7, m_sx.str().c_str());
      
      hits_over_linear_vs_KEK_rate.Draw("p");
      c_ch->Update();
      ps->NewPage();
    }

  }

  //----------------------------------------------------------//

  if ( plot_total_rate ) {

    if ( plot_expo ) {
      hist->SetBins(1,0, 0.8*area ); // linear region
      hist->SetMaximum(0.8*area*2.0);
      
      m_sx.str("");
      m_sx << "Layer " << layer << " VMM " << vmm << " ch " << chan << " Digital Hit Rate vs KEK Rate";
      
      hist->SetTitle(m_sx.str().c_str());
      hist->SetXTitle("KEK Rate (kHz)");
      hist->SetYTitle("Photon Rate (kHz)");

      hist->Draw();
      
      m_sx.str("");
      m_sx << "Noise " << f_linear_tot->GetParameter(0) << " Cross-Talk Factor " << f_linear_tot->GetParameter(1);
      Tl->DrawLatex(max_kek_rate*0.15, hist->GetMaximum()*0.8, m_sx.str().c_str());
      
      m_sx.str("");
      m_sx << "Capcitance " << capacitance << " Dead Time " << f_xexpo_tot->GetParameter(2) * 1e6 << "ns ";;
      Tl->DrawLatex(max_kek_rate*0.15, hist->GetMaximum()*0.7, m_sx.str().c_str());
      
      f_xexpo_tot->Draw("same");
      f_linear_tot->Draw("same");
      
      hits_vs_KEK_rate_tot.Draw("p");
      c_ch->Update();
      ps->NewPage();

      //-------------------------------------------------------//
      
      hist->SetBins(1,0, max_kek_rate*1.1*area );
      hist->SetMaximum(max_tot_rate*1.5);
      
      m_sx.str("");
      m_sx << "Layer " << layer << " VMM " << vmm << " ch " << chan << " Digital Hit Rate vs KEK Rate";

      hist->SetTitle(m_sx.str().c_str());
      hist->SetXTitle("KEK Rate (kHz)");
      hist->SetYTitle("Photon Rate (kHz)");
      
      hist->Draw();

      m_sx.str("");
      m_sx << "Noise " << f_linear_tot->GetParameter(0) << " Cross-Talk Factor " << f_linear_tot->GetParameter(1);
      Tl->DrawLatex(max_kek_rate*0.15, hist->GetMaximum()*0.8, m_sx.str().c_str());
      
      m_sx.str("");
      m_sx << "Capcitance " << capacitance << " Dead Time " << f_xexpo_tot->GetParameter(2) * 1e6 << "ns ";;
      Tl->DrawLatex(max_kek_rate*0.15, hist->GetMaximum()*0.7, m_sx.str().c_str());
      
      f_xexpo_tot->Draw("same");
      f_linear_tot->Draw("same");
      
      hits_vs_KEK_rate_tot.Draw("p");
      c_ch->Update();
      ps->NewPage();
      
    }
    //------------------------------------------------------//
    
    hist->SetBins(1,0, max_kek_rate*1.1*area );
    hist->SetMaximum(max_tot_rate*1.5);
    
    m_sx.str("");
    m_sx << "Layer " << layer << " VMM " << vmm << " ch " << chan << " Digital Hit Rate vs KEK Rate";
    
    hist->SetTitle(m_sx.str().c_str());
    hist->SetXTitle("KEK Rate (kHz)");
    hist->SetYTitle("Photon Rate (kHz)");
    
    hist->Draw();
    
    m_sx.str("");
    m_sx << "Noise " << f_linear_tot->GetParameter(0) << " Cross-Talk Factor " << f_linear_tot->GetParameter(1);
    Tl->DrawLatex(max_kek_rate*0.15, hist->GetMaximum()*0.8, m_sx.str().c_str());
    
    m_sx.str("");
    m_sx << "Capcitance " << capacitance << " Dead Time " << f_x1overX_tot->GetParameter(2) * 1e6 << "ns ";;
    Tl->DrawLatex(max_kek_rate*0.15, hist->GetMaximum()*0.7, m_sx.str().c_str());
    
    f_x1overX_tot->Draw("same");
    f_linear_tot->Draw("same");

    hits_vs_KEK_rate_tot.Draw("p");
    c_ch->Update();
    ps->NewPage();
    
    //-------------------------------------------------------//
    
    if ( plot_expo ) {
      
      hist->SetBins(1, 0, max_kek_rate*2.0*area  );
      hist->SetMaximum(1.5);
      
      m_sx.str("");
      m_sx << "Layer " << layer << " VMM " << vmm << " ch "<< chan<< " Hit Efficiency vs Total Hit Rate";
      
      hist->SetTitle(m_sx.str().c_str());
      hist->SetXTitle("Total Photon Rate (kHz)");
      hist->SetYTitle("Channel Efficiency");
      
      hist->Draw();
      
      f_expo_tot->Draw("same");
      
      m_sx.str("");
      m_sx << "Noise " << f_linear_tot->GetParameter(0) << " Cross-Talk Factor " << f_linear_tot->GetParameter(1);
      Tl->DrawLatex(max_tot_rate*0.15, hist->GetMaximum()*0.8, m_sx.str().c_str());
      
      m_sx.str("");
      m_sx << "Capcitance " << capacitance << " Dead Time " << f_expo_tot->GetParameter(2) * 1e6 << "ns ";;
      Tl->DrawLatex(max_tot_rate*0.15, hist->GetMaximum()*0.7, m_sx.str().c_str());
      
      hits_over_linear_vs_tot_rate.Draw("p");
      c_ch->Update();
      ps->NewPage();
      
    }
    //----------------------------------------------------//
    
    hist->SetBins(1, 0, max_kek_rate*2.0*area  );
    hist->SetMaximum(1.5);
    
    m_sx.str("");
    m_sx << "Layer " << layer << " VMM " << vmm << " ch "<< chan<< " Hit Efficiency vs Total Hit Rate";
    
    hist->SetTitle(m_sx.str().c_str());
    hist->SetXTitle("Total Photon Rate (kHz)");
    hist->SetYTitle("Channel Efficiency");
    
    hist->Draw();
    
    f_1overX_tot->Draw("same");
    
    m_sx.str("");
    m_sx << "Noise " << f_linear_tot->GetParameter(0) << " Cross-Talk Factor " << f_linear_tot->GetParameter(1);
    Tl->DrawLatex(max_tot_rate*0.15, hist->GetMaximum()*0.8, m_sx.str().c_str());
  
    m_sx.str("");
    m_sx << "Capcitance " << capacitance << " Dead Time " << f_1overX_tot->GetParameter(1) * 1e6 << "ns ";
    Tl->DrawLatex(max_tot_rate*0.15, hist->GetMaximum()*0.7, m_sx.str().c_str());

    hits_over_linear_vs_tot_rate.Draw("p");
    c_ch->Update();
    ps->NewPage();

  }

}
